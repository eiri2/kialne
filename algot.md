## Algot

When thinking about trying this, the name _Algot_ quite soon appeared in my mind.

Testing this is more or less dependent on there being some good word list or similar of Gothic, and there might be it seems, first we have at least some Swadesh words from Wiktionary (165, if I got it right).

The script might be nice, but not suitable for a simple (to learn, etc) langauge, so I remove that. the /hw/ sound might be written hw as in OE. I also see that /k^w/ seems to be written q, that's an option, but simplifying this to /kw/ and kw might be good.

-a seems to be quite a common noun ending in Gothic, in certain noun cases, according to airushimmadaga. Hmm.

At any rate, infinitive verbs seems to end in -an, so that could change into Mundezes -i (basing this on Mundeze (MZ) grammar and POS markers, generally).

At and ~~to~~ in are kind of surprising.

Not sure if pronouns should follow the MZ system, maybe not (because you have to mangle some, and their characteristicness is a bit fun). Anyhow, _is_ and _si_ for he and she is ingeniuus! Quite good. (Are they too close to each other? Seems to have worked in Gothic.) _Ita_ is also good.

They is /'i:s/, but maybe that should be /'ejs/ for clarity and simplicity.

Maybe ɛ to e will work.

There's a conflict in long/vs short phonemes. Should the language be very simple, it shouldn't have and need that distinction, then it should go. Should it be some kind of germanic interlang, then maybe removing that will make it too weird. Hmm.

If keeping long /i/, ei might be as good as any way of writing it. Ditto with ai and au. Or, otherwise, I guess the simplest way is to reduplicate the vowel, even if I don't like that super much, visually/esthetically.

-s might seem to be an adjective ending, probably will be replaced with MZ counterpart.

I first transscribed /θ/ as dh, erraneously/out of temporary confusion, changed now to th. <gg> is apparently /ŋ/. _fidwor_ is a funny word.

POS markers _maybe_ doesn't go so well into this as into PIE words. _Maybe_ we don't have to have them, either (this necessitates rather fixed word order, though).

/ˈɛː.θiː/ is a strange word for mother, if I ever saw one.

-s is often, in nouns, a case marker (masc. nom. sing.), but there are others, so.. not sure if I can delete them correctly, but I'll probably try that.

The benefit of gothic as a base for an intergermanic is that it's not biased to either north or west germanic. Though, it might be closer to one of them (maybe NG?) (it should also be closer to PG than the modern variants, given that it's older). On the other hand, it also might contain weirdnesses that doesn't occur in the others. go _akrane,_ en _fruit,_ sv _frukt,_ de _Obst_ (hmm), _Frucht_ etc.

It _could_ make sense with -e as verb ending in a Germanic language (I'm not sure whether it looks good in the examples, though). Nouns could go with, difficult. -s is common in many noun cases, but also in adjectives. None could be as valid as changing things into e at the end – so those that has none would have none, and those that end in a vowel would get that vowel cut. Hm. ~~Interestingly, -a- seems common in Gothic verb endings, maybe, which I otherwise only know from Swedish.~~ (Maybe that depends on the verb.)

### Day 2
Well, it hasn't been a night yet, but it's after midnight. Icelandic is another language that seems relatively close to PG (I mean, Icelandic does, I'm not sure about Gothic, but like, it's old, so. And by the looks of it.), so it would make sense also to base a similar project on Icelandic. The name then is given – _Alis_ (hey, even _Alice_ works! Didn't think of that before). (But _Alis_ is better from an Icelandic perspective. And strictly speaking, _Algot_ should maybe be _Allgot,_ and _Alis/Alice Allis/Allice._ But I don't know if it works as well, and I don't know if I care, the one l-versions look good.)

It would also make sense to fill in gaps in Algot or balance "unbalanced" things with Icelandic (and maybe even a third language, potentially even non-germanic, say looking for what word has the most common cognate for the word in question in Latvian – if any of the Germanic ones). If doing so, the name could be _Algis._

It feels, so far, like no-vowel-endings on nouns (except pronouns) would work well, I was going to say that it would only mangle those ending in vowels, but actually I had thought, I remember now, that one could have an h in the end of those. I'm not sure I can pronounce that correctly, I can do something – but if we can't, no panic – it shouldn't be that important of a distinguisher so that it would be mandatory in speech – no worries if we cannot pronounce ending h, then. But it gives some (sort of) marking to the nouns, that feel like it fit with the words.

How do we know, btw, that au (or was it ao?) was pronounced /<insert correct sign>/? Feels a bit weird that Wulfila, as I presume it is, chose two letters for that sound, that usually is written just <o>, right? I guess there might be texts written in other alphabets, simply.

I guess the nominative endings -o and -a should be cut then. And cutting ending vowels, I was gonna say, that wouldn't work so well at least for diu. So at least in that case, an ending h is needed (or less problem-causing, if choosing between cutting and adding h).

Hmm, it would be easy if all nouns ending in -a in the word list were in feminine, and should get their -a removed, but 1) matha sounds better than math, maybe, and 2) I suppose _atta_ is maybe not feminine (maybe neuter)?

-s after vowel could be kept, that would be.. close to one actual form in GOT, and still make the nouns end in consonant. Hm. Either that, or it will be swapped to an h (which would be more regular – but with keeping some s:es there would be 1) less h-at-ends-how-to-pronounce 2) more variation).

It looks like letting "personals", determiners and question words end in whatever could work okay, looks good in the list. Might keep to that.

Looks like au is always long /ɔ/, maybe, and then there are some regular /o/:s too. The former could be marked with ó, maybe. To keep it rather simple. In Swedish, we would use å (we did put a ring on it).

### Day 3
Should /ɛ/ be kept or reduced to e, should /ɔ/ be kept or reduced to o? And should vowel lenght distinction be kept, and how to write it (in a simple way) if so? I'm a friend of simplicity, and think that possibly removing vowel length wouldn't create so many minimal pairs, but that could also be completely wrong.

An ugly hack would be to write long vowels with capital letters. WIs, jUs. Or use the Esperanto X system (courtesy of Mr. Elon Musk), wixs, juxs. Wow. h is more reasonable then: juhs, wihs, but see, it already collides with hw in hwehwa (hweewa), and also obiously so with ending h.

Another way is to not mark it, keeping it close to more Germanic languages (right?), except that double consonant/long consonant is always (?) preceeded by short vowel (it is often thought of as just marking the vowel lenght in Swedish, but according at least to research by Hartmudt Traunmüller, Swedish has long consonants, and so seems Gothic to have. According to Wiktionary Swadesh list, eh.), BUT this is ambigous due to double consonants being reduced before yet another consonant (*/frap:t/ → *frapt) – which can however be avoided (→ *frappt) – and also at the end of words (no following consonant). Hm. I kind of lost track of the paragraph here, what I was talking about. I was going to continue: and not care to much if people (hypothetical people) say it right or not (if there are not too many or too critical minimal pairs, it should work). I kind of like this idea – on paper – especially with no reduction of cononant doubling before another consonant. (Unless Gothic has long vowel-followed-by-long consonant-combinations).

Another option is to mark long vowel with ´. Sounds like it could look good, but it's not the simplest of writing methods. It's readily available on a Swedish keyboard (because we use it in.. like, one word – _idé_ – and some surnames. Hm.).

Potentially, ' could mark short vowel in a word without ending consonants: sa' (two '' might strictly speaking be more logical, but since the ' wouldn't be used for anything else, it wouldn't be necessary, and a bit ugly indeed).

It seems like _þar_ is really /þar/, and should potentially be tharr then. Hmm. It surprised and confused me a bit, at least. (Would _tharr_ actually be right, or confusing? Hm.)

After sleeping:

I somehow feel that tharr could give the wrong impression, like /'θar:/, as it might be in Swedish (even if that written word would often be analysed as /'θar/). It's also a factor that Swedish has a long vowel in the corresponding word, _där,_ so that example might feel extra confusing.

It also doesn't work, quite, if we keep long consonants, or, then there would be no way to see if a consonant is long.

But I have a feeling the long consonants 1) might be rare 2) always occur after short vowels. If so, one could remove that distinction and keep close to original Gothic (for the pan-germanic goal) (I'm not sure what goal or goals will end up with – if any). And au in original GTC can sometimes be short /ɔ/, it seems, so it's not perfect for one spelling per pronounciation, but otherwise it might be quite regular and simple. I just instinctively started working towards simple-simple.

I have a feeling not including long consonants (sort of rare still? Both globally and in G langs?) might be the way to go.

Marking short vowels not only seem to maybe mark loads of words, but technically one would need to mark the -i ending too.

On the 165 rows of words I have (some with more than one word), there are 78 occurences of /ː/, meaning long vowels in just under half of the words. There are 30 occurences of /ɛ/, and 15 of those with /ɛː/ – 16 /ɔ/, and 10 /ɔː/ (ca 1/3). In the IPA column, there are 8 occurences of /e/, of which all (!) are short vowels (okay, maybe not soo surprising, after all), 27 /o/, and 19 /oː/. Overall, I guess more short vowels, but not many more. And maybe /ɛ/ and /ɔ/ could be marked with say é and ó – but also, it would make sense to mark the less common sound, for less work, thus marking /e/ – but I think that would probably be too counterintuitive.

All in all, it's probably wise to start with complexity (in translitteration and phonology, in this case), and then add simplification (I think this is one of those thigs that might generally be true, when simplifying a system, depending on the situation, though). So, I mean, trying to keep as much as possible (phonemes, vowel lengths, maybe not consonant length) (otoh, consonant length is easy to mark if using double vowels for long vowels, or skipping vowel length), and after that see what can be cut.

(Personal) pronouns could relatively simply be left out from vowel lenght marking, explaining them in the material of the language, or just allowing variation (at least if changing iis to something more distinct from is – this would be required then).

There doesn't seem to be many nouns ending in i (four with short, two with long i:s in the sample), and I guess h would only be needed (if at all) where there is risk of confusion with POS markers, of which we use -i so far. Same goes for keeping -s.

It wasn't an explicit goal or thought, but this could become an option for people curious in Gothic to get aquainted with some of it's words (but not orthography, hm). I've had similar ideas before, including for Gothic, I think, and like, Swedish and so on.

Oh, there's an un- prefix, cool! There seems to be also a ga- prefix that I'm not sure what it does.

<div align="center">🙑</div>  

Another option is to only mark é and ó with apostrophy on the first letter when duplicated (maybe not optimal). Or these:

k´órus, kó´rus  
kwíno  
kw´ino, kwi´no

hmm. (Or maybe a straight apostrophe.)

I also wonder what's the difference between /man:na/ and /man.na/? Is there really a pause in the latter?

/ˈhɔːs.jand/ must be a Wiktionary error.

I'm cleaning up the word list a bit, and generally try to make the nouns in the first column, especially, a bit more 'positivistic'/agnostic, without my changes. I'm skipping the verbs, I'm rather content with their form. (Added extra column(s) where it's a bit messy, or where I didn't want to change too much of my earlier draft.) (If you copy-paste it from the source into a spreadsheet, and divide cells by tabs, you'll get a table.)

#### Some testing
Ikk mattji' wórrm  
Ik matji wórm  
(I see a snake)

Thu hosji' wattoh  
Thuu hoosji watooh  
(You hear water)

### Day 4
I think some more testing would be good. First of all, it will show a bit how the langauge/vocabulary works (if it does) or functions, secondly it's fun.

_Ik sehwi sléhts namoo._  
(I see a smooth name.)

(Completely illogical sentence, and do note that it means that the name is literally smooth. I'll leave it to the reader to imagine a context where this makes sense, it's not at all impossible.<!--name signs-->)

If we imagine some extreme simplification:

_Ik sewi sleht namo._  

_Si matji wórt._  
(She eats a root.) (Was looking at flower, and thought that no, this is more realistic – yes, I might have had herb in mind, which is close swe cognate.)

I see here how I spiral into some weird track of how to solve stuff, something that is bugging me personally, potentially ending up with something weird, debatable/contentious? and unintuitive for everyone else.

But I'm considering ' as an option for long vowel marker. (Alternatively it could be a noun-ending-in-vowel marker.) And, frankly, c doesn't seem to work too bad for ó (but x or y feels a bit weird for é), with long vowels marked by ´. (Which one sound to scrap?

Diaresis is also not so hard to type, right? But yes, it doesn't seem to combine with the accent mark.

Maybe è for /e/ and e for é? (Okay, then we can still not have accent mark for vowel lengths – or we can, because seems like /e/ is never long? Sorry, *never short. è would thus be /e:/.) Digraphs would also work, but the native gothic ones are a bit unintuitive. _ao_ or native _au_ and _ae_ (or is this just swe bias?). And then, inspired by the translitteration acc to wp, one would mark the vowel length on the last vowel (or is it clearer the other way around?). Other option: both.

I guess no digraphs is maybe less confusing. And if not that, then marking both on long au and ae.

In the end, either long vs short or ó and é might be too complicated and skipped.

Seems like /o/ only occurs in /o:/, in the wp table? But not according to my calculation above. Hmm. And btw, isn't /o/ and /u/ rather similar?

### Day 5
I'm reading about Gothic phonology and it sheds a lot of light. It is a possibility to uze *use ortographical Gothic as the basis too. Long /ɔ/ seems to only occur in loanwords. And it seems the goths only marked long /i/ orthographically.

Elaborating on ortographical Gothic, some of the <ai> and <au> (even all the long vowel ones?) comes from historical Germanic ~~digraphs~~ sorry, diphtongs, and ~~hypothetically~~, they used those digraphs for words with [ɛ] and [ɔ], because hypothetically Germanic ai and au had developed into that in Gothic.

Relying on this orthographical/historical Gothic would maybe not quite work out, and I'd possibly have to figure out which ai and au that comes from Germanic diphtongs (if it's not simply m/l all long [ɛː] and [ɔː]).

Btw, Qoura says that Gothic is the earliest substantially recorded GL. I guess I knew that implicitly if I had thought about it, but anyhow it suits our purpouses, because the more archaic the form, the closer to the common-for-all traits.

Where was I? Yes, WP says that "In most cases short [ɛ] and [ɔ] are allophones of /i, u/ before /r, h, ʍ/", which could enable a simplification there, at least if one wants to simplify to the extreme (in this case regarding phonology).

Where does this leave us? I still need to figure out the difference between /u/ and /o/ (yes! honestly! I'm Swedish.), to see if there is room for any simplification there. Seems like Esperanto has /o/, which in turn makes me unsure about the difference between that and /ɔ/. I usually use ipachart.com, but I don't quite get it from the samples there in this case.

[u] as in [̯iu̯] seem to represent the end of a falling diphtong, maybe.

### Day 6

Having looked just a bit at determiners/adjectives, it seems like removing the s and changing into preferred POS marker might be the thing there. Or just keep the s as a marker, obviously, but maybe we could use that as plural (if that is somewhat common in Gothic)? I should look into how reasonable that seems, looking at different plural endings (almost wrote genitive).

Thinking just a little bit more straight about it now, I think s might be even more reasonable as just a genitive. Not sure if I'd want to use that, tho, but should look at things first.

Also, yesterday I presumed that there are always good consonant ending forms of nouns. Might not be the case, of course.

_guma_ would turn into _guman_ or _gumin,_ the former leading users to maybe believe that it's an ancestor of "man" (it's not, afaik). That's a bit boring, but bareable.

_walus_ has no other consonant ending in it's forms than s. I guess my thought was not to disallow nouns ending in s.

mann
guman
dius
kwen
aban
éthín
fisk
fugl
hund
waurm
mathan?
thramstín – grashopper, locust
malón – moth
bagm
hrungos?
lof (what to do with the vowel?)
wort (ditto)
blóman
hojis? (ditto)

Is this idea worth it? Like, only keeping "stem s-es" might be a good idea, but making every noun end in a consonant?

I feel like possibly merging the two o-sounds will make sense.

### Day 7

I guess we are closing up on some solutions. Maybe I should try implementing the è and only o spellings.

It's good, è being always long feels logical, with ´ marking the other long vowels.

Maybe we can live with no POS endings on nouns? And a few potential collisions, then. Hm. Or add h to certain vowels (which also means less-work-for-the-author).

_tunthus_ even varies u in different declensions. Is it cleaner then to consider only tunth the root? _fótus_ and _handus_ is the same with that.

Maybe a rule like if you cannot write (or remember) the accent marks, it's okay to write without them, could possibly work (if confusing minimal pairs are plentiful, it won't work). (And yes, unfortunately they might be hard to remember, sometimes even to see. Not great for a language striving for optimal, or well, high simplicity.)

Wait a second. I don't feel much like looking at the declension of every Gothic noun, which speaks against skipping the nominative s just because it's part of a declension. Hmm. (Then again, there is feel-like-it.)

After sleeping:

_isas_ might be good for 3pp, clearer than _ís_ vs _is_ (3psm).

è almost now felt like it would be what I previously wrote as é – maybe I've just gotten used to that one, being the accented one, I mean. Checked if diaresis and accent mark could be combined for say, that previous one, and I know the sign exist, I think, but I cant type it, which is a hinderance (I could find it and copy it). Again, this would render a common sign in a difficult way. I mean, a bit complex and cumbersome (which is not ideal).

For adjectives, I guess it's -a in MZ, and we could use that too. One could also use the s ending, that seems to be prominently prevalent (?). Then one should not use that for plural, or genitive. Hmm. Plural could be (optional) -(e)y, anyhow, or just adjectives and no suffix. Genitive could also be "de", or.. it's something else, at least often, in MZ (maybe "a"). But you get it, a marker. (One could look for a native Gothic one, too.)

### Day 8
Well, what to do now? Maybe I'm starting to feel finished. ;) Okay, seriously: I might be closing up on a good enough phono-orthography – phonography? – so I guess that leaves the adjectives, adverbs, and syntax and various types of sentences.

Ik kwimi, sewhi jah thwahi.
_I came, saw and washed._

**sing	plur**  
ik	wís  
thú	jús  
si, is, isa, ita	isas

	**question word	near	far**  
**place**	hwar	hèr	thar  
**identity**	hwa	sa	jéns  
**person**	hwas, hwo?  
**manner**	hwéwa

**and**	jah  

### Words
I (1sg)	(ik)	/ˈik/	ik  
you (2sg)	(þu)	/ˈθuː/	thu			thú  
he, she, it (3sg)	(is) (masculine), (si) (feminine), (ita) (neuter)	/ˈis/, /ˈsi/, /ˈi.ta/	is, si, ita (sis common animate? isi? isa?)			is, si, ita	isa (com)?  
we (1pl)	(weis)	/ˈwiːs/	weis, wiis			wís  
you (2pl)	(jus)	/ˈjuːs/	jus, juus			jús  
they (3pl)	(eis)	/ˈiːs/	ejs, ihs, iis (itas? sisas, isis? isas?)			ís	isas?  
this	(sa)	/ˈsa/	sa  
that	(jains)	/ˈjɛːns/	jéns  
here	(hēr)	/ˈheːr/	hèr  
there	(þar)	/ˈθar/	thar  
who	(ƕas) (masculine), (ƕō) (feminine)	/ˈʍas/, /ˈʍoː/	hwas, hwó  
what	(ƕa)	/ˈʍa/	hwa  
where	(ƕar)	/ˈʍar/	hwar  
when	(ƕan)	/ˈʍan/	hwan  
how	(ƕaiwa)	/ˈʍɛː.wa/	hwéwa  
not	(ni)	/ˈni/	ni  
all	(alls)	/ˈalːs/	alls  
many	(manags)	/ˈma.naɡs/	manags  
some	(sums)	/ˈsums/	sums  
few	(faus)	/ˈfɔːs/	fós  
other	(anþar)	/ˈan.θar/	anthar  
one	(ains)	/ˈɛːns/	ens, eens, ehns			éns  
two	(twai)	/ˈtwɛː/	twé  
three	(þreis)	/ˈθriːs/	thrís  
four	(fidwōr)	/ˈfid.woːr/	fidwór  
five	(fimf)	/ˈfimf/	fimf  
big	(mikils)	/ˈmi.kils/	mikils  
long	(laggs)	/ˈlanɡs/	langs  
wide	(braiþs)	/ˈbrɛːθs/	bréths  
heavy	(kaurus)	/ˈkɔ.rus/	kórus  
small	(leitils)	/ˈliː.tils/	lítils  
narrow	(aggwus)	/ˈan.ɡʷus/	angwus  
woman	(qinō)	/ˈkʷi.noː/	kwínó	kwiine  
man (adult male)	(wair), (guma)	/ˈwɛr/, /ˈɡu.ma/	wer, guma	were, gume  
man (human being)	(manna)	/ˈman.na/	manna	manne  
child	(barn)	/ˈbarn/	barn	barne  
wife	(qēns)	/ˈkʷeːns/	kwèn	kweense, kweene  
husband	(aba)	/ˈa.ba/	aba	abe  
mother	(aiþei)	/ˈɛː.θiː/	éthí	ééthe  
father	(atta), (fadar)	/ˈat.ta/, /ˈfa.dar/	atta, fadar	atte, fadare, fade  
animal	(dius)	/ˈdiu̯s/	dius	diuse, diue  
fish	(fisks)	/ˈfisks/	fisk	fiske  
bird	(fugls)	/ˈfuɡls/	fugl	fugle  
dog	(hunds)	/ˈhunds/	hund	hunde  
snake	(waurms)	/ˈwɔrms/	wórm	wórme  
worm	(maþa)	/ˈma.θa/	matha	mathe  
tree	(bagms)	/ˈbaɡms/	bagm	bagme  
stick	(walus), (hrugga)	/ˈwa.lus/, /ˈhrun.ɡa/	walus, hrunga	waluse, walue, hrunge  
fruit	(akran)	/ˈak.ran/	akran	akrane  
seed	(fraiw)	/ˈfrɛːw/	frew	frewe  
leaf	(laufs)	/ˈlɔːfs/	lóf  
root	(waurts)	/ˈwɔrts/	worts  
flower	(blōma)	/ˈbloː.ma/	blóma  
grass	(hawi)	/ˈha.wi/	hawi  
meat	(mimz)	/ˈmims/	mims  
blood	(blōþ)	/ˈbloːθ/	blóth  
horn	(haurn)	/ˈhɔrn/	horn  
hair	(tagl) (singulative), (skufta) (collective)	/ˈtaɡl/, /ˈskuf.ta/	tagl, skufta  
head	(haubiþ)	/ˈhɔː.biθ/	hóbith  
ear	(ausō)	/ˈɔːsoː/	ósó  
eye	(augō)	/ˈɔːɡoː/	ógó  
mouth	(munþs)	/ˈmunθs/	munth  
tooth	(tunþus)	/ˈtun.θus/	tunthu/tunth  
tongue (organ)	(tuggō)	/ˈtun.ɡoː/	tungó  
foot	(fōtus)	/ˈfoːtus/	fótu/fót  
knee	(kniu)	/ˈkniu̯/	kniu  
hand	(handus)	/ˈhan.dus/	handu/hand  
belly	(wamba)	/ˈwam.ba/	wamba  
guts	(hairþram)	/ˈhɛr.θram/	herthram  
neck	(hals)	/ˈhals/	hals  
breast	(brusts)	/ˈbrusts/	brust  
heart	(hairtō)	/ˈhɛr.toː/	hertó  
to drink	(drigkan)	/ˈdrin.kan/	drinki  
to eat	(matjan)	/ˈmat.jan/	matji  
to bite	(beitan)	/ˈbiː.tan/	bíti  
to spit	(speiwan)	/ˈspiː.wan/	spíwi  
to blow	(waian)	/ˈwɛː.an/	wéi  
to laugh	(hlahjan)	/ˈhlah.jan/	hlahji  
to see	(saiƕan)	/ˈsɛ.ʍan/	sehwi  
to hear	(hausjan)	*/ˈhɔːs.jan/	hósji  
to know	(witan)	/ˈwi.tan/	witi  
to think	(hugjan) (to suppose, to mind), (þagkjan) (to ponder), (þugkjan) (understand, realise), (fraþjan) (to opine, to seem)	/ˈhuɡ.jan/, /ˈθank.jan/, /ˈθunk.jan/, /ˈfraθ.jan/	hugji, thankji, thunkji, frathji	should maybe be simplified into fewer words, or some are already independent (eng think translates to two things in swedish, btw). also, ji to i, maybe?  
to fear	(ōgan), (faurhtjan)	/ˈoː.ɡan/, /ˈfɔrht.jan/	ógi, forthi  
to sleep	(slēpan)	/ˈsleː.pan/	slèpi  
to live	(liban)	/ˈliː.baːn/	líbi  
to die	(gadauþnan), (gaswiltan)	/ɡaˈdɔːθ.nan/, /ɡaˈswil.tan/	gadoothni, gaswilti, doothni, swilti  
to kill	(usqiman), (afdauþjan)	/usˈkʷi.man/, /afˈdɔːθ.jan/	uskwimi, afdóthi  
to fight	(weihan)	/ˈwiː.han/	wíhi  
to hit	(slahan)	/ˈsla.han/	slahi  
to cut	(maitan)	/ˈmɛː.tan/	méti  
to dig	(graban)	/ˈɡra.ban/	grabi  
to walk	(gaggan), (galeiþan)	/ˈɡan.ɡan, /ˈga.liːθan/	gangi, galíthi, líthi  
to come	(qiman)	/ˈkʷi.man/	kwimi  
to lie (as in a bed)	(ligan)	/ˈli.ɡan/	ligi  
to sit	(sitan)	/ˈsi.tan/	siti  
to stand	(standan)	/ˈstan.dan/	standi  
to turn (intransitive)	(wandjan)	/ˈwand.jan/	wandi  
to fall	(driusan)	/ˈdriu̯.san/	driusi  
to give	(giban)	/ˈɡi.ban/	gibi  
to hold	(haban)	/ˈha.baːn/	habi  
to squeeze	(þreihan)	/ˈθriː.han/	thríhi  
to rub	(bnauan)	/ˈbnɔː.an/	bnói  
to wash	(þwahan)	/ˈθwa.han/	thwahi  
to wipe	(biswairban)	/biˈswɛr.ban/	biswerbi  
to pull	(þinsan), (tiuhan)	/ˈθin.san/, /ˈtiu̯.han/	thinsi, thiuhi  
to push	(afskiuban)	/afˈskiu̯.ban/	afskiubi  
to throw	(wairpan)	/ˈwɛr.pan/	werpi  
to tie	(bindan)	/ˈbin.dan/	bindi  
to sew	(siujan)	/ˈsiu̯.jan/	siuji, siui  
to count	(garaþjan)	/ɡaˈraθ.jan/	garathi  
to say	(qiþan)	/ˈkʷi.θan/	kwithi  
to sing	(liuþōn)	/ˈliu̯.θoːn/	liuthoi, liuthi  
to flow	(rinnan)	/ˈrin.nan/	rinni  
sun	(sunnō), (sauil)	/ˈsun.noː/, /ˈsɔː.il/	sunnó, sunn, sóil	sunne, sooile  
moon	(mēna)	/ˈmeː.na/	mèna	meene  
star	(stairnō)	/ˈstɛr.noː/	sternó	sterne  
water	(watō)	/ˈwa.toː/	wató	wate  
rain	(rign)	/ˈriɡn/	rign	rigne  
river	(aƕa)	/ˈa.ʍa/	ahwa, ahwah	ahwe  
lake	(saiws), (marisaiws)	/ˈsɛːws/, /ˈma.ri.sɛːws/	séws, mariséws	seewe, mariseewe  
sea	(marei)	/ˈma.riː/	marí	mare  
salt	(salt)	/ˈsalt/	salt	salte  
stone	(stains)	/ˈstɛːns/	sténs	steene  
sand	(malma)	/ˈmal.ma/	malma	malme  
dust	(stubjus) (mulda)	/ˈstub.jus/, /ˈmul.da/	stubjus, mulda	stubje, mulde  
earth	(airþa)	/ˈɛr.θa/	értha  
cloud	(milhma)	/ˈmilh.ma/	milhma  
sky	(himins)	/ˈhi.mins/	himins  
wind	(winds)	/ˈwinds/	winds  
snow	(snaiws)	/ˈsnɛːws/	snéws  
fire	(fōn)	/ˈfoːn/	foon  
ash	(azgō)	/ˈaz.ɡoː/	azgoo  
to burn	(brinnan)	/ˈbrin.nan/	brinni  
road	(wigs)	/ˈwiɡs/	wigs  
mountain	(fairguni)	/ˈfɛr.ɡu.ni/	ferguni  
red	(rauþs)	/ˈrauθs/	rauths  
white	(ƕeits)	/ˈʍiːts/	hwíts  
black	(swarts)	/ˈswarts/	swarts  
night	(nahts)	/ˈnahts/	nahts  
day	(dags)	/ˈdaɡs/	dags  
year	(jēr) (aþnam)	/ˈjeːr/, /ˈaθ.nam/	jèr, athnam  
cold	(kalds)	/ˈkalds/	kalds  
full	(fulls)	/ˈfulːs/	fulls  
new	(niujis)	/ˈniu̯.jis/	niujis  
old	(sineigs) (of people), (fairneis) (of things)	/ˈsi.niːɡs/, /ˈfɛr.niːs/	sinígs, fernís  
good	(gōþs)	/ˈɡoːθs/	goths  
bad	(ubils)	/ˈu.bils/	ubils  
rotten	(fuls)	/ˈfuːls/	fúls  
dirty	(unhrains)	/ˈunˌhrɛːns/	unhréns  
straight	(raihts)	/ˈrɛhts/	rehts  
sharp (as a knife)	(*ƕass)	/ˈʍasː/	hwass  
smooth	(slaihts)	/ˈslɛhts/	slehts  
dry	(þaursus)	/ˈθɔr.sus/	thorsus  
correct	(garaihts)	/ɡaˈrɛhts/	garehts  
near	(nēƕa) (nēƕ)	/ˈneː.ʍa/, /ˈneːʍ/	nèhwa, nèhw  
far	(fairra)	/ˈfɛr.ra/	ferra  
right	(taihswa)	/ˈtɛhs.wa/	tehswa  
left	(hleiduma)	/ˈhliː.du.ma/	hlíduma  
at	(at)	/ˈat/	at  
in	(in)	/ˈin/	in  
with	(miþ)	/ˈmiθ/	mith  
and	(jah)	/ˈjah/	jah, jahh  
if	(jabai)	/ˈja.bɛː/	jabéé, jabbé			jabé  
because	(untē)	/ˈun.teː/	unte, unnte			untè  
name	(namō)	/ˈna.moː/	namoh, nammoh, namooh			namó  
