# Kah Kah
_things in Kah_

mai uyo!

lau wa tengi janko aya za Kah.

nenju nuko si, yau.

## fekalo-renya
je en renya wau teri nen la ron jevanpuno.

## Wikipedia-tosko feren
**Ontario tinuno chevau-firungi tos sitale** chevau-firungi tos sitale ya janyole bo Ontario tinuno, Kenadan.

[sihoi. om za (a)yin. eom om basta.]

[tansa – nong tosko dal-tansa – nong. aye reza.]
Jeremiah Atwater kwi umukwan kwandoti.

<!--de som blev förgiftade av konserver
vildhussen

minkoi - foolish/wild/crazy
yuvang - wild/rogue (un-tame)
-->
## Minkoi Huss

## Piter tuzi
Wakita laila naim sin pan, ai<!--
once-upon-a-time exist rabbit small four, and--> senka na yunyo: Flopsih, Mopsih, Pezi-lua ai<!--
name of they: F, M, Cotton-tail and--> Piter. Yunyo dora lu uma na yunyo bo dushum-singin/moilu<!--
Peter. they live in sand-hill--> vu avu na vempe wan.<!--
under root of pine-tree big.-->

'tau/tolo/nen, bembe wa' naim ma naita ka ninku kwa.<!--'well, dear-child my' rabbit Mrs. old say morning one.-->

[cont]

Flopsih, Mopsih ai Peza-lua, yunyo naim sin nen, de/vunu nonambe eno kwara kokosu: esto Piter, yu sife wu, gevu noshi lula na ba McGregor, pombo yulo vu tunum!
<!--F, M and CT, that rabbit small good come-go/go down lain in-order-to collect blackberries: but P, that so naughty, run-to straight garden of Mr. McGregor, squeeze himself below gate! | do adverbs go after verb?, and 'en'-->

Na kwa, yu kiza sosho siwi ai vos Faransa siwi, ai tai (kiza) gechos (siwi), tai, miju/monkiju kungile, de kele zirosho. Esto ze tilum/alum na pepos-*aski/peposla, hayu jobo? ba McGregor lo!<!--
fir-st, he eat lettuce some and beans French some, and then radish, then, feel sick/feel sick rather, go look-for parsley. but after end/edge of cucumber-frame [too literal?] [cucumberplace], who meet? Mr. McGregor FOC!-->

Ba Mc.Gregor (kwi) wera la¿ lele ai vuvu¹, ai (CONT) hiva shoro sita, esto yu pya ai gevu geze<!--after?--> Piter, nonoi vudun ai donka ”mumbas uminza!”

Piter omunyi wamwa¿: yu gevu layo tunu lula, chumun yu monjo ano wainu¿ denu tunum. Yu mole kwa chivu bambosko shoro, ai yin bambosko pem.

Ze mole yanyo, yu gevu za vuvu pan ai denu ge tunti/tunti ge/tumwige, la valon<!--so that--> en wa je en yu kwi tengi<!--could have--> achuru en yu nong gevu minishi go bo nanasu-kwe, ai tom owohiza na¿ kosha wan na<!--on--> chivan na ya.<!--order--><!--AFTER losing them, he ran on four legs and went faster, so that I think he might have got away altogether if he had not unfortunately run into a gooseberry net, and got caught by the large buttons on his jacket. It was a blue jacket with brass buttons, quite new.-->

¹ vupil, esto aye hinka¿ tunti (nen) ai teri nen.

## jomolu na Giza

**jomolu na Giza** miabea naitashi wan wisa la Giza bo Masaran. aye odo de bukim na Masaran na Kwita bo tiro 2600–2500 KH. jomolu awembe na 'azawuyana<!--azawuya na?--> teo na Yula, ai nunayun-aude wike/aude wike <for> nynayun.

vailu – cube

## matanu ayonka
–mai? ukwa lai, ha?

–eo.

–…hayu ka aye?

–wa.

–li itinza ochauka tunti, ba.

–wa, yinga vu zonya na li.

–li yinga, ha?

–nenshi.

–ai haya li jam vu zonya na wa?

–nong jo. sisi monzon, ai la lau/lau la.

–janjang.

–nong itinza munjo wa.

–tolo…<!--(li) -->denu kiza wa, ha?

–haya? nong.

–hamun nong?

–…_keju_ eno wa kiza li, ha?

–nong.

–tolo…wa nong yinga na shimbe aye. yinga mweju.

–laila yinga mweng, ha?

–eo, eo. wu wi.<!--oh yeah. lot's of them. "wu eo".-->

–yunyo dora hala?

–*lakwa<!--somewhere, → ukwa--> eno li nong tengi weyun yunyo. yin <plane, layer> na ilaila.

–nodau ayinza?<!--n., order?-->

–aye en wa ka chauku,<!--order?--> nong?

–ho, eo. yau.

–ayo<!--la?--> nenen.

–tolo haya senka, ba yinga mweju?

–hoha en li tengi loika ya.

–tengi kemun.

–sisi<!--just/simply--> reju (ya).

–nenen. tolo, tinti kaka li haya?<!--haya var?-->

–nong 'sinyi. pau senka <to>[nu?] li.

–Eeh…Albert?

–ALBERT? aye nen tio en tengi 'janjebo/je|en le?

–ya yo en wa tengi je|henjo.

–tolo, li reshi nong sajeyun.

–aye nong yunkule <like> akw~~o~~a en yinga mweju ke ka.

–(wa) nong mweju toyo.

–tengi jebo aye.

–him, (li) tengi kaka…Zordor.

–Zordor?

–eo. ya fafau tio <to> senka ho na wa.

–nenen/zanu. [Nice to meet you, Zordor.] senka na wa _Charlie._

[achu](https://old.reddit.com/r/shortstories/comments/14mj4p0/sp_the_nice_monster)

## Gustav Vasa

_Gustav Vasa_<!-- [unclear:] na kwa _Gustav Eriksson,_--> (12 jomaro na a1490 tiro wisa–29 sastaro na a1560) kwi bukim na Severan chu a1523 deche eta yu tomia (la) a1560, ai ku aye <self-recognised, 'owaijo na/chu yulo> 'uvan na an chu 1521, tabo anyoru Severe na anamil (kura) firu Kristian bukim na sun na Udanan, Noran ai Severan.

Gustav tinu notile anyoru Severe na anamil<!--order?--> ze bansoto na _Stockholm,_ eta<!-- during which--> uba na yu omumia. punzato na Gustav <as/for> bukim la 6 vaimaro 1523 (mwela 'nyoshinyo-anin Severe) ai adaivu na yu <into> Stockholm la 11 anin mwela 'kano amondembe ze tio na Severan chu Ronan na Kalmar.

<!--literally "ten thousand li long wall"
https://en.wikipedia.org/wiki/File:%E8%8F%AF%E5%A4%B7%E5%9C%96%E9%99%84%E6%B3%A8.jpg
-->
## donumna wamwa na Chongan

**donumna wamwa na Chongan** eom **donumna na Chongan**<!--kinesiska namnet kanske--> zenyo na aiwaru-audo, yanyo kwi 'ojando zeno basano na kwita na mano na Chongan na Kwita shi avan ~~chu¿~~ fe wonyo nurang wishi chu sompoyan na Panan-Sunan.

## word order
**car to-be-red** – a red car ('the car is red' in phrase/context – seems to have both meanings, and difference is a philosophical question, maybe? so (translation) depending on context.)

**to-be-red car** – the car is red (!) (emphasized version, in my interpretation – only standalone phrase?) [right way around? these two?]

**I PAST see car to-be-red** – I saw a red car

**the car I PAST see, it to-be-red** – the car that I ~~was~~ saw was red

**I PAST think that I see car** – I thought that I ~~was~~ saw a car [okay, kind of pointless without the Kah words]

**I PAST eat, when I see car** I was eating, when I saw a car [same here]

**car be-red be-big** – a big red car (?)/the car is red and big (? if so reverse order)/the red car is big.

In the case of a construction like "historical site of Germany" (or UK or whatever), or say "historical site of interest of Germany" (or ancient, which is "of old-age" in Kah), my conclusion is that the regular adjective must be the one to be put first after the noun:

**site historical of Germany**

noun adj of noun

**site historical of interest of Germany**

noun adj of noun of noun

etc.

Otherwise, if we put an adjective like that last, it gets ambigous:

~~**site of Germany historical**~~

in a way, a site of historical Germany is very similar to (and can sometimes be identical (in identity (at least)) to) a historical site of Germany, but it's not exactly the same thing (consider for example that a site of historical Germany might no longer even be located _in_ Germany, while a historical site of Germany is in Germany, but might not neccessarily have so much to do with Germany, even – say a former foreign military base). The phrasing seems to indicate that we're talking about something relating to historical Germany, or possibly it could be about a historical site (not sure if there is any rule in Kah for interpretation of such ~~sentences~~ constructions, that clears it up). [But I guess there could possibly be cases when this order also works well, maybe?]

I've also been thinking about adjective order.

My hunch is that.. the most important adjective comes last before the noun in a head-last langauge, so in a head-first like Kah, it would be logical if it's the other way around – the first after the noun. (I think I might have put it the other way around in my head before, though.) I'm not quite sure, though? But if so:

**boat be-Turkish be-small**

a small Turkish boat

though, in that case I think turkish is "of Turkey", which with my previous reasoning would give "boat be-small of Turkey". another hypothetical example:

**bird nesting small**

a small nesting bird

addition: I've been thinking more about this now, and it feels like putting the most important adjective/adverb/modifier first would also make sense.

[if-sentences]
[wheather? eller något annat.]
[questions?]

## A summary
So, to sum things up for myself (and others) in case I take a break and forget how things work in Kah:

* Translating is generally a breeze, somehow. Definate/inde–, nah, tense, nah, plural/si–, nah. Maybe it's those things. Also, most things can be found in the dictionary, it's rather thorough, at least. Otoh, I don't know how to form a verb from a noun (or was it the other way around? Both, in any case.) – if that's at all a thing. (But how then to express certain things?) Word derivation (term?), or forming new words from existing roots, is objectively at least more complex than in Esperanto (there are a bunch of prefixes for verbs, should be said, and some for nouns, and conjunctions – iirc – generally start in e-) (existing combinations/compounds are often associative or 'poetic' rather than semantically logical, combining roots triggers phonological changes with certain combinations)

* No copula. "N is N" is just "N N" (if I'm not messing it up now). Single-phrase "the car is red" and "red car", well it's confusing in the way of "making the general basis a bit counterintuitive (head first – (difficulty of) that part depends on native language – but also stative verbs instead of adjectives, I guess that is a bit more odd), and then turning it around from that", please refer to the original grammar (official, one could say, but that sounds like we're talkig about a company, government or institution imo).

* _tos_ means about/for (not all senses of for). it's a great word.

* _esto, en, ai_ means but, that (as in eo:ke, sv:att, de:dass¿) and and. _eom_ means or.

* "the boy that I saw wore a red hat", well, that might be "~~the~~ boy, I saw he" or maybe "~~the~~ boy, TOP he I saw, (he) wear red hat", I'm a bit confused about the second pronoun ("I" – and also not sure if you could skip the second "he"). But – "the boy that wore a red hat" is at least "boy, he (PAST) wear hat red".

* You can skip pronouns, and afaik, other words as long as you get the meaning across (by the context – which can both be what's been said, but also the situation or environment), if I got it right. This might be called 'elison'? ~~Let's make an elision of a liason.~~

* Verbs put one after the other generally seem to get the meaning "(to) A in order to B in order to C in order to D", afaiu (not very certain about it, though).

## Hintal na Yerevan-ahau
ahau na tanyo na Savete:

–ha ho en laila inamil na achuka la Ronan na Savete?

–'ojevu¿ eo. la Ronan na Jomam, li tengi vuti ku Yudo Lam ai donka "mompun Ronald Reagan!", ai li nong tom 'ofon. ayu kwashi ho la Ronan na Savete: al tengi vuti la<!--tila--> Hula Yam ai donka "mompun Ronald Reagan!", ai li nong tom 'ofon.

## Asinyi na Hubbard Tifa

La zekita na Maumaro la London. Nuno ototol<!--invirade. och täckta.--> bo vushina lon tio. Hubbard Tifa, yu dora la Holland Park Avenue 104, yu<!--behövs?--> hura la zonya na yu ai koyun. Yu<!--kwi--> le kwangi 50 faushi ai le 'kiski-kuzi yam-rai pim. (Yu) Chauku la vu bo zubea ai jankuri seo bole chai sham, wanshu<!--tasty-->. Kwi Panta, echu Johanna ai Betty la milto<!--i och we funkar nog inte som verb, i ger substantiv, och we transitativt verb skapt från annat verb.-->. Yu kwasi la yudo lu luana ai lubo na yu, Mary ai Dick. Ayo sus ai vera. Tifa nuda kosho na kaiko – tonko ukele nyocho tatau, ya ninku “Amimia¿ bo chaideo”. Yu soza si na chai, ai jara¹/timoi vuvu<!--utan prep i ordl--> (chemoi) zonya-shantil shushi.

Tai, yu wehim¹ auhim¹ la naibea. Yu pauvu¿ kaiko ai him. Tau ya hinka wai. Hubbard Tifa je en tim¿<!--must be--> Mary eom Dick, yu<!--kiuj fel i pdf?--> <!--nubo-->nuchu zonya. Yu jewai en al kiza somwansu mosau la¿ zekisa. < Natural > yobo, ya je, esto wa sayun tim¿<!--esploru-->, hashi yunyo < are doing/faring >.

Yu jango mantango (na yu), za pisau chu/la nunya sin¹ weluza zonya, ai nuchu/nuchi cheke//nubo naibea. Pisau obas/omompi¿, ai tau nong auhim<!--hinka-->. Yu nubo cheke/(tos) bea na ben. Yunyo hura ai zon, < ŝnufante >, shile¿ en yunyo chisole lon.

Janjang, yu je ai wainu <!--nubola opedagogiskt-->naibea. Yu mumpi <!--dinya-->pisau ai apomoi tunu luvan na vangi:

  –Hea, ukwa la, ha?

Yuyung waika. Tolo, reshi laila < some > amunjebo na tokwe, yu je.

La anin¹ zenu¹, yu wera la kobea (na yu/yudo) ai zazia < unpaid > poko<!--siwi-->. Yu huncho ai je, en ewe¿ tinza¹ janko cheke¿<!-- luma na yu,--> Victoria luma. Nishi¿ tinza densi/zauza yu, esto shi¹ wito¹, nong weyun zashi yin. Vabu na Victoria Smith tabo ata na yu, ojo tos fauvu<!--briefs, also shorts--> na yu – wauku, yu mula fauvu, yu munku talado ai jampo fauvu 'wiwi.

¹ Kaya kenole

<!--
lele-dinya – thought poŝlampo meant flashlight, but seemingnly candle-light. works still? just "candle" or "small lamp" clearer?
(si is not also small, corrected to sin.)
how to form a verb from a noun?
(cheche→cheke)
shile – in such a way, that way, that manner?
mumpi/mompi >_>
benyo, collective noun, how/when is it used?
compounds not built in logical way, but freely/associatively, which makes them a bit unintuitive, at least to form, and then some have distinctions, as a result of semantics I guess, which might be too fine-grained/unneccessary. related words often completely different.
siwi can be used as plural, but specifically more than two.
"ka en a. to say that is, to call" – I don't get this?

### errata
wirthless, i ordl.
"havin to"
"that is nong of your business"
"manufacterer"
"wiwizi s." – no def?
-->

## Kimeo-anumia omumbas – <!--ojeho? al-->jeho en kwi wil yuvang

Al mumbas anumia cheke <!--wana-->kimeo la Berlin, Michael Grubert dolati yoka la akara na chova. Al kwi jeho/yunkuza en wanakimeo churu, esto al jeho tau en unga, yu mun denjuto yo, yu wil yuvang.<!--the chase for the lioness that was feared to have escaped in Berlin is halted, the mayor announced at a press conference. – is there any sane way of making a close translation of this? I simply rephrased it.-->

[achu](https://nitter.net/ttnyhetsbyran/status/1682364462857158658#m)

## 'ganchunyo na Severan zembo (na) abombai

chunyo na [strömming] zembo abombai, ujono zenka. la jafim-talado na gan opishom na Jan Söderström, al tonis [jale] sisi 1/5 na gan, ya okeju.

(achu TT)

## tiso wi ⛆💦 
nintau, tiso ~~la~~ lau. tiso wi lau. noza akuka, tiso wi denu la Sunta, esto denu tiso sin. tiso wi denu la anin zenu, ai anin zenu awau – zemanta.

## kuta
<!--in the beginning, there was the land/realm of fire and the land/realm of ice, and between them, the great Nothing.-->

kuta la, laila ala na api ai ala na aso, ai bambo yunyo – memeng. chu ala na aso, sonai (wisa) asonu. yuyung de fafau memeng, sonai sayom revo. revo tomwan, ai gela, ambe na mano/mano na/ memeng revole – esto nino na memeng, weluza ala na api, tunsa. bambo yunya ala vera – mweju shi tiska leleng tihum/tiska tihun?leng.

eta revo ai api kwanu, sinso omore wisa tonsa' wanyu/wamwa uhai/ula. uhai chisole eta zon, ai chu lele na yu 'werim wanyu wana ai wanyu bu, vuvu sun na yu pauza bembu, ai sen yin waisun uyin.

## Pomperiposah lu' fuku nai

Kwikuta, ata tauku tiro pol wi wi, laila mimpas<!--trollpacka--> (ming), naita temwanyi'/wamwa'/'wiwi<!--terribly old, not old terrible, even if that is also true-->, ai senka na yu Pomperiposah. Ya nong chau¹ senka nenyun, esto eye ya nenyun tunti wi'<!--bra mycket vackrare--> yulo. Linyo tengi jeyun yunku na yu, ha? Yu le ayun(yo) yam sin sun ai kiki wan (sisi) lu' dun yem sisi. Ai yu le lele yobo na chikos wisa ai zeo wan tila' zeze'. Esto fuku na yu lo ming tio, chumun ya nai wengi vulembe kwambe<!--ambig?-->. Linyo tengi jeyun hawi jenshum yu za eta yu fufu ya! Wambio yobo (kwa) (la) atoshi yo!

<details><summary>Translation</summary>
||Once, time ago/before_now year thousand many many, exist witch (evil), old terrible/huge/much-much (terribly old), and name of her Pomperipossa.||
||It not exactly name beautiful, but yet it beatutiful surpass much (a lot more beautiful) herself.||
||You can imagine appearance of her, question_marker?||

||She have eye(s) red small two and mouth big (only) with (?) tooth three only.||
||And she have hand full' of wart plural and hump big on_top_of backside.||
||But nose of her EMPH bad surpass_all, because it long equal lower_arm whole.||
||You can imagine how_much tobacco she use when she sniff it!||
||Kilo (actually more like pound) complete/full (one) time every!||
</details>

Esto aye be Pomperiposah zaten, chumun yu pale temwanyi. Yu dora kwasi bo yudo sin bo penyo, ai yudo ye odo chu sepe ai jenze, ai (ya) le <!--ambe-->wambe wan na zezun yompo duzu. Pomperiposah pale wi ye lo<!--eo:tiom-->.

<details><summary>Translation</summary>
||But that (moved-for-emphasis marker) Pomperipossa can_afford, because she rich terrible [terribly rich].||
||She live alone in forest, and house that from sausage and ham, and (it) have chunk(s) big of caramel instead of brick.||
||Pomperipossa rich much that [so rich] EMPH.||
</details>

Esto nong yunga tunyike de<!--yunde--> yu ai<!--yungasa-->yinga<!--troll--> kakwa fawang eom, chumun yu mweng temwanyi wi ye ('ye-we/'we-ye). Tonto ukwa de yu, yu paska-<!--'mun-->sayom' geta uyu de' nunya eom wan-zudeo<!--wam? wanz* in dict--> eom champe eom akwa yin. Yu mimpas<!--packa--> zen wi. Minyai wan tio na yu (kwi) en fuku na yu tom¹ nai wi tomwi la tato yo eta yu paska-sayom ukwa. Aye afon na yu!

<details><summary>Translation</summary>
But no human dare come her and<!--humanoid-->monster any barely either [barely any (Scandinavian) troll either – word order?], because she cruel terribly [adverb form?] much that. If someone come her, she enchant-change immediate person to [?] table or big-saucepan [kettle – root order?] or pencil or something other. She [is] witch dangerous very. Sorrow big most of her (was) that nose of her become long more and-more [became longer] at time/occasion every when she enchant-change anyone. That punishment of her!

that-state – ye-we/we-ye

</details>



Mau laila bukim bo/la an ye, (an) bo' Pomperiposah dora, ai bukim ye le kimbu sin, (senka na yu) Pipi, ai kimwana sin, (senka na yu) Fifi. Wikuta, yunyo nuvu ai amwevu bo/la noan na rewendo, 'olalu na punkan kim. Punkan chausa na itenai wau, en yu tengi sisi¹ awaika ”A!” eom ”Ha!” tos 'ayayo eno al' ka yu'.

<details><summary>Translation</summary>
||also exist king in country that, (country) in Pomperipossa live/reside, and king have prince little, (name of he) Pipi, and princess little, (name of she) Fifi.||
||Once, they walk and stroll in park of castle, companied of officer/marshal royal.||
||Marshal delicate of extent that, that he can only reply A! or Ha! about everything that they/one? say he.||
</details>

— Wa keju 'chinu/nu penyo! Pipi kimbu ka.

— A! punkan ka ai zeno yunyo.

Ze fauta yunyo de la chuman, ai punan tonso rora' vuvu, chumun yu za chivunyo chu kwengu.

— Ha! yu ka ai nuda geta eno jango kenos-'vunosnyo.

Esto ku' yu de, 'munla|vula yol la fuku na yu ai ka:

— Ba!

<details><summary>Translation</summary>
||-I want into|outto forest! Pipi prince say.||
||-A! marshal say and follow they.||
||After while they arrive at swamp, and marshal get_wet around' foot, because he use shoes of silk.||
||-Ha! he say and turn_around immediately to put_on 'pair_of_collar-boots||
||But before he go, cause-at/lay_down finger at (on) his (own) nose and say: -Ba!||
</details>

Aye tento kano: Tara lau, Pipi kimbu ai Fifi kimwana, deche wa awainu 'ojango [la?] kenos-vunosnyo wan na wa<!--la wa-->!

Esto benyo kim nong jebo aye, esto yunyo (de) mumweku bo penyo.

<details><summary>Translation</summary>
||that supposed' mean:||
||Wait here, Pipi prince and Fifi princess, until I return dress-ed collar-boot-s big of me at me!||
||But children royal not understand that, but they (go) further in forest.||
</details>

Aye kwashi chau penyo en Pomperiposah dora bo. Une yo nuku¹ geta¹ chinchi:

—Nong de lai!

Esto Pipi kimbu nong jebo ajika na une esto ka de'¿ mea na yu:

—Tonto/Sinje wanyo de la' dento? (La) vado munjuleng/sihoi wi<!--so boring-->. Wa yunkule' le simpio vai. Li le hawi, kimwana?

—Wa le sisi simpio kwa, Fifi awaika, esto wa tengi sawa bol/'bolnyo.

—Tolo¹/(echu) li tengi fafau¹ janzu. Li om/(tus) tom 'kizuwana na wa, Pipi kimbu ka.

<details><summary>Translation</summary>
||That be_same exactly forest that Pomperipossa live/reside in. Bird all begin immediate chirp:||
||Not go there! But Pipi prince not understand song of birds but say to' sister of him:||
||-If/Suppose we go at adventure? CHAPTER_POSSIBILITY (At) home boring much (so boring). I apparently have (order?) cent six (six öre). You have how_much, princess?||
||I have only cent one, Fifi reply, but I can fry apple/apples.||
||So_thus_well/(thus_therefore) you can almost cook_food. You vocative_marker/may become female_food-something (housekeeper) of me, Pipi prince say.||
</details>

<!--check for N is Y, if Y is adjective, should come first, right?-->
Tolo/echu yunyo mwevu/mwenu? dento bo penyo sol, deche (yobo) de yudo sin na Pomperiposah, yudo odo na sepe ai jenze ai laila wambe/ambe wan na zezun yompo duzu.

—Om kiza kito na zekita! Pipi kimbu ka, an ju kike, ai sunu sepe wan chu yudo.

Tai Pomperiposah 'muchiku fuku nai na yu ai yun¹' yunyo.

— De bo lau¹! yu ka geta¹. Wa ananso-uma mweju na yunyo. Yunyo denu/lum pauza gumbus-binchil chu wa.

Pipi kimbu ai Fifi sin jeho/reju yu ai nubo, eye yunyo (kwi) yika temwanyi.

— Hm! Pomperiposah ka ai yun yu. Wa nong kiza setu na gaune/gaune-setu tabo ata nai...

Ai yu 'jampas la hum, ai gela/ge kimbu Pipi ai Fifi kimwana tom gaune sin sun, yunyo jehea/yun wanyi wi noyom.

<details><summary>In English, please</summary>
||-Hm! Pomperipossa say and look_at/see they. I not eat steak of goose during [temporal in?] time long...||
||And she make_magic in air, and soon/fast Pipi prince and Fifi princess become goose small two, they [who/which] regard?/look terrified much each_other [can one put wanyi wi last?]||
</details>

— Punkan na rewendo! Pipi kimbu keju donka bo ikuji na yu/yulo. Esto ya hinka' sisi kwashi:

— Kakaka. Yu nong amau¹ tengi kaza, 'gauneka/kwoka sisi.

— Ayy! Pomperiposah donka ai leche fuku na yulo.

Yu tom nai na vulembe kwa amau geta, eta yu paska Pipi ai Fifi INTO/vosa'/de gaune.
<details><summary>Em inglês, por favor</summary>
||-Marshal of court! Pipi prince want scream in anxiety of he/himself. But it sound only same_as [better word for 'as'?]:||
||-Kakaka. He not longer/more can speak, goose-speak/cluck only.||
||–Oww! Pomperipossa scream and touch nose of her.||
||It become long lower_arm one more immediate(ly) [?], when she enchant Pipi and Fifi into/turn_into/arrive goose.||
</details>

<!--(na) yu→(na) yulo, kanske, på vissa ställen.-->

–Aye nempu!/Yu timpai aye! une yo bo penyo donka.
||-That fair/just/right!/She deserve that! bird all in forest shout.||

—Yunyo tara sisi!¹ Pomperipossa donka ai <!--hile-->kai kayol na yulo nole yunyo. Wa denu munchu/munu pas-meo na wa cheke yunyo.
||-You wait only [orig. lit. "Wait you!"]! Pomperipossa shout and close fist of she directed towards [to/for] them. I will send magic-cat of me target you.||

— Wanyo (denu) jale aleo, une yo jika ai nenu eno haka dezane de ai (lai) pau anoka nen.
||-We (will) get help, bird all sing and fly for/to ask stork come (there) and give advice good.||

Dezane wanjo¹ tio na une yo, chumun yu nunai de Masaran la venta yo, ai kakwa tukotan la 'jomola.
||Stork wise most of bird all, because he travel Egypt at vinter every, and study hieroglyphs at 'pyramid(s).||

— Tau linyo denu (tus') ganu, gaune sin na wa! Pomperiposah ka ai pono/muno Pipi ai Fifi (ku yulo) lu/za pesape wan (na yu). Ya nen wi de yu, ai linyo denu loi tunti', eta wa sawa/pizu linyo (la) mantau.
||Now you will (be_allowed [get to]) swim, goose small of me! Ph say and drive Pi and F (in_front_of herself) with_near/using cane big (of her). It good much for/to you [does you so good], and you will taste better', when I fry/roast [fry in oven, I imagine] you (at) tonight.||

Ai echu' yu pono kimbu ai kimwana tenyo vu de/vude/devu boso.
||And thus she drive prince and princess poor_pityful down to lake.||

— Kakaka! yunyo donyoi, esto tim eye de bo aso ven.¹
||– Kakaka! they wailed, but (they) must yet/still to in [out into] water cold.||

— (Linyo) ka kakaka, ha? Pomperiposah haka. (Linyo) kano tente¹ _binchil/something-fitting?_ yu kahau (yunyo).
||–(You) say kakaka? P-a ask. (You) mean maybe pankake_KONSEKVENSKOLL? she mock (them).||

Opaska gaune tim kiza goilia sol sin ai poi oso — ye reshi yinshi|ayin reshi tunti' binchil ai gumbus!
||Enchanted goose must eat tadpoles black small and grass wet – that certainly different|another_thing certainly surpass/more_than [compared to] pancake and jam!||



Pomperiposah vuti la soku ai kaivu yulo za fuku nai (na yu), ya tau nai wengi vuvu na yu. Chauta/tai yu wehim asiris ze yu, bo penyo, ai nuda. Achu punkan, yu kwi la vado ai deza kenos-vunosnyo na yu, ai tachu (tai) (kwi) kele kuyi benyo kim nojong.'
||P stand at shore and support herself use nose long (of she), it now long equal leg/foot [leg] of her. Right that moment she hear rustle behind her, in forest, and turn_around. Source officer, he PAST at home and fetch collar-bookts of he, and since (then) (PAST) look_for anxiously [anxiously look for] children royal lost.||

— Ha! punkan ka, eta yu weyun mimpas. — Pa! yu ka ai kumil naitu na yu eno tuchu fuku na mimpas.
||-Ha! marshal say, when he saw witch. -Pa! he say and draw sword of he in_order_to cut_off nose of witch [her].||

Men Pomperipossa förtrollade honom genast till en gammal kråka, som ängsligt hoppade omkring på stranden och sade:

— Kra, kra! 



Chumun ranyam wan vu' bo boso, yu nong kiza la/tabo anin yem.
||Because lobster [crayfish] big down in [down in?] lake, it not eat at/during day three.||
För nere i sjön var det en stor kräfta, som inte hade fått middag på tre dagar. Och den kräftan knep sig fast med sina klor i Pomperipossas långa nos. Och Pomperipossa skrek så förskräckligt, att hon blev alldeles blå som ett plommon i hela ansiktet. Men kräftan släppte ändå icke sitt tag.

Då skrek trollpackan ändå värre, så att det hördes ända långt ner i Afrika.

— Det var det förskräckligaste skrik i världen, man kan tänka sig! sade storken.

Och han sade sanning, ty ingen fågel kan ljuga.

Fluks ai kruks! Tai apaska na Pipi ai Fifi tomongova geta', ai gaune lam sin (wisa) tom /wai/ kimbu ai kimwana /wai/. Ai kal naita, yu papya la soku, tom punkan kim/na kindonyo/kindo la chauta/sinsita kwashi, rento' 'tongunyo ai konzi ai naitu ai kenos-vunosnyo.

— Ha? A! Nen! punkan ka ai za Pipi kimbu ai Fifi kimwana, kwa la lele yo.
||Flux and krux! Then enchantment of Pipi and Fifi dissolve immediate[ly? – adverbs how?], and goose white small (plural) become /again/ prince and princess /again/.||
||and crow old, it [which was] skip at shore, become officer royal/of court/palace KONSEKVENSÄNDRA_EVT at [temporal?] right_that_moment/instant same, carrying [with] order_medals and wig and collar_boots.||
||-Ha? A! Good! officer say and take Pipi prince and Fifi princess, one at [?] hand every.||

Ai (tolo) yu gevu gele tio na yu WITH yunyo tunde penyo wainu rewendo, la yu bukim rati' tara yunyo, yu/bukim leza simpe-zanyo/bombe kim la lele yino ai bol kim la lele zano.<!--l kwa (l) yin--> Yu wonka wi, chumun yumun lajong tabo ata nai ye'.

— Na! punkan kim ka eno javera.
||And (then) he run fast most/superior of he with them through forest return castle, at it king sit_up wait them, he_it/king hold twig-bundle royal [or is this rather "that hold a twig-bundle royal"? is there a difference] at hand left and apple royal² at hand right. ['(of the) nation', 'crown', 'realm'→'royal' in lack of better translation.]||
||He be_angry_with' much, because they be_missing during time long that' [such a long time – thos 'that' work?].||
||-Na! officer royal say in_order_to calm_down [reassuringly, soothingly].||

Ai bukim jebo geta¹, en punkan kano, en yumun kwi opaska 'omun' chu' Pomperiposah, ai en yumun nong tengi kubas aye.

Tai bukim tom¹ oponju|Aye ponju bukim, ai yu jumbo simpe-zanyo/bombe bo koshango na punkan – ya leke tongu na 'imweti/'itumangi tumwi – esto pau sumbe na bol kim wan de Pipi kimbu ai Fifi kimwana yo.
||And king understand immediate, that officer mean, that they was enchanted caused from [? – by] Pomperipossa, and that they not could avoid it [they couldn't help it].||
||That move/touch the king, and he insert rice-bundle in shirt of officer – it function as medal/decoration [distinction] of exquisiteness/fancyness ['distinguished-ness'] extra – but give half of apple royal big to P prince and F princess all [one each] ['but' a bit strange there, but it's in the original].||

Esto Pomperiposah sayom geta adu wan. Li lo tengi weyun yu mau¹ tau, tonto¹ de boso ye, eye yu tau¹ ('otomwan) otingo bo zipea ai hupea wi ye, en yunkule gin sin/(singin sin).
||But Pomperipossa transform/turn_into immedetely [to?] stone big.||
||You focus_marker [vi mem] can see her also now [still, even today – order/position in phrase?], if come/go/arrive lake that, even_if [?] she now [pos?] (grown-ed) covered in moss and bush much that [so (covered)], that look_like mountain small/(hill small) [small mountain in original].||  

² bol na an la' senka na roya kim (roya ai pano) la Severeka – lau "bol kim" eno itunyun /na atoka/.
||¹ apple of country [realm-apple] (is) name of sphere royal (sphere and cross) [orb and cross, globus cruciger, stavroforos sphaira] in Swedish – here (translated as) "apple royal" for clarity /of the story/.||

### legendary legend
WORDS IN OTHER LANGUAGE  
translation' that I'm uncertain about  
'new construction by me, some are uncertain  
different|root|options  
→word →order →uncertainty

### useful words
eye – yet, though  
wi – much, more, very  
? tila – on  
eta – when (temporal conjunctor)  
de – a. – to come, go, arrive at  
chumun – because  
fawang – barely  
wan – big  
wamwa – huge, enormous  
en – that, if, whether, basic conjunctor
eno – in order to

<!--two flaws of Kah: making compounds triggers sound changes that are a bit complex and not fully clearly documented, and related words such as hammer and to hammer are usually different (built by hinting components, albeit/but dissimilar ones).

tanke: Mundeze semantiskt striktare. Kan på sätt och vis vara lättare.

Roots with a certain meaning are not always used in their bare/freestanding form as one root words, in the dictionary. si/sin, wan/wana.

A (possibly) common root is **nu,** having to do with '(to) go', as in nubo (go+in, enter) and denu (come/go/arrive/leave+go) 'to go (?), will, future marker'. Another one is **je** – 'think, mind, brain', as in **je** (to think, think about) and **jebo** (understand, realise), **jebonu** (realise, come to understand).

mushroom – meat flower
kwashi – to be the same as, almost as quasi, 'almost' in Portuguese.

jam- – också stative verb, tillståndsverb, från substantiv, enligt exemplen.

mumbon – to drop
sinso – drop (n.)
sisinso – to trickle, drop, leak

tilembe, vulembe – upper arm, lower arm
lembe – ion
lele – hand, arm – IN GRAMMAR, MISSING IN DICT
"attibutive"
duzu – brick? MISSING ENTRY
"become something ales"
wanzai – patriot = big dear/loved? dear big?
wikuta→kwikuta!
amwevu – ska beskrivas som noun
mwevu → mwenu?
se – meat
sehum – air, breath
grammar: jam-, basic transitive verb→(basic) active verb?



tonai – get long



ana     n.      the one of: ana wa - mine, ana li - yours; ana senji - dancing, ana gevu - running

kimbu – bukim (prince – king)
kimwana – wanakim (princess – queen)

ruling queen vs. wife of king?
a ruler of any sort vs. a king?

kintiki n. pineapple – crown-something – crown-mouth? is that parsed then as mouth-crown?

(Coincidental similarity to the Kon-Tiki expedition.)

yin – other
yino – left
(yi – ?)

zano – right
za – uze
no – ?

### errata
thunderbolt lighting → ball lightning?
uyo – everybody, everyone


### användbart
tonai   a.      to get long, elongate, lengthen



wiye, yewi [?] – so much, so (good etc)
_de_ for go/arrive, confusing. _gå_ in Swedish at least has a strong connotation to leave (by foot, n.b.), maybe? and so does english go, maybe?
singin – compounds have reverese order of words (as sen-mik said something about too)?



criterias for auxlangs: shouldn't have mandatory tense or number.



oi – i. – adhortative interjection: [oi] do! come on – eller?
is ku only temporal?



Mai!

[One of] the simplest form of sentences that you can form in Kah is declarative (?) [but those are a bit tricky, so hm.]

* genitive is just marked with **na,** meaning "of": **vatil na li** – your phone.

* the phone is red – be-red phone – **yam vatil**

* white phone – phone be-white – **vatil lam**

https://www.youtube.com/watch?v=RcOeosN7WYA-->

## Inyeka na gan
Zhuangzi ai Huizi kwi mwevu (ti)la vano ti sonai Hao, eta 'ukucheva na kwa boyun, ”(lo) weyun ashi na gan eta yunyo genu bambo adu/adunyo! Shishis'/aye inyeka na gan.”
||Zhuangzi and Huizi was stroll on bridge over river Hao, when aforementioned_person of first observe ”(FOC) see manner of fish [minnow] when they dash/rush [dart] between rock/rocks! Such [?]/that is happiness of fish.” | netu – arrow||
||Zhuangzi and Huizi were strolling on a bridge over the River Hao, when the former observed, “See how the minnows dart between the rocks! Such is the happiness of fishes.”||

”Li nong gan,” Huizi ka, ”chu haya li jo en gan wau nyeka?”
||”You not fish,” Huizi say ”from what you know that fish-that happy?”||
||“You not being a fish,” said Huizi, “how can you possibly know what makes fish happy?”|‘Whence do you know that the fish are happy?’||

”Ai li nong wa,” Zhuangzi ka, ”chu haya li jo en wa nong jo haya jam' gan nyeka?”
||”And you not me,” Zhuangzi ka, ”from where you know that I not know what make fish happy?”||
||“And you not being I,” said Zhuangzi, “how can you know that I don’t know what makes fish happy?”||

”Tonto wa, eta/tabo/wa nong li, nong tengi/'tenging jo haya li jo,” Huizi waika, ”deva chu rejo ye en li, li nong gan, nong tengi jo haya jam gan nyeka, nong ha?”
||”If I, when/during/I not you, not can know what you know [order?],” Huizi reply, ”follow from fact that that you, you not fish, not can know what make fish happy [order and construction? should it be "tengi jo haya, haya jam gan nyeka" (both places)?], not QUEST?”||
||“If I, not being you, cannot know what you know,” replied Huizi, “does it not follow from that very fact that you, not being a fish, cannot know what makes fish happy?”||

<!--”Om wainu,” Zhuangzi ka ”de ahaka na kwa/kuyo na li. Li haka (wa)”-->”Om wainu de (ha)la wanyo nuku. Eta li ka ’chu haya li jo en gan wau nyeka?’, li haka wa tabo kwi jo en wa jo. Wa kwi jo ya chu ti Hao.”
||“Let us go back,” said Zhuangzi, “to your original question. You asked me how I knew what makes fish happy. The very fact you asked shows that you knew I knew—as I did know, from my own feelings on this bridge.”|“Let’s go back to where we started. When you said ‘Whence do you know that the fish are happy?’, you asked me the question already knowing that knowing that I knew. I knew it from up above the Hao.”||

## What I know in Kah

### ai, esto, nong

Some of the most important and common words in Kah are **ai, esto** and **nong** – ’and’, ’but’ and ’not’.

* **awau ai aye**  
this and that  
* **awau esto nong aye**  
this but not that

As you can see, **awau** means ’this’ (’this thing’ – free standing – as a noun) and **aye** means ’that’ (as a noun).

### en
Another important word is **en,** which is a general conjunctor as in ’I think _that_’, ’I've heard _that_’ or ’I'm not sure _whether_’.

* **li tengi weyun en awau OSV**  
you can see that awau means ’this’  
* **wa ye**
I think that

As you can see, **li** means ’you’ and **wa** means ’I’.

nen, ming
’good, well, fine’, ’bad, evil’

good day
bad day
I think that this is good

eom, eo
or, yes
sisi
eno
eta