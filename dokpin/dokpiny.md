**kendo** - also - seems to be the way to say and when it starts a subphrase, according to Google, such as "and their names were". to keep it simple and easy to use, I presume that it's more expected by most users (could be English/European bias) that the regular word [and] be used there.

This is also all based on Google translate (and **chiemo** from https://lughayangu.com/post/luo-love-wordsgreetingsdirections-and-basic-conversations so far), which has it's limits - it might contain errors and weirdnesses, and often if I try to translate back a word, it get's another meaning (homonyms, or maybe just Google translate shortcomings), so I can't always verify or check the meaning of a component word in a compound expression, for instance. However, this will be far from being any sort of Luo language, other than that the word roots are taken from there (and sometimes distorted by phonological changes).

ny, d, t, ch, and ng´ from Luo can't be kept as is, in pronunciation for instance, so d and t need to be merged with dh and th (/d/ and /t/ - oh, no, it IS the other way around, I got it wrong) - anyway, the dh and th can't be kept phonetically, and therefore should be merged with d and t, in spelling and pronunciation. ny could be turned into nj, but there is already other words with the spelling nj and a rare sound - I'm keeping the spelling of those and changing the pronunciation to the less rare and thus 'simpler' /nj/. Likewise (as with dh and th), I'm merging ng' with ng, which might become either ng of sing, or just /ng/. the spelling ch is kept, but pronounced /tS/.

dok mar piny, might be possible as name. or dokpiny for short?



a/the house is both translated in gtr as ot, so there seems to be no definate and indefinate forms, good.

clean it with water
he came with us
ler gi pi
ne obiro kodwa



the name of the project is even affected by ny, I realised. one could change that digraph to ni, simply, as another option, or even keep the spelling and say that y is pronounced i (but that's a bit messy). it's either of those, or just turn it into n as I said before. I'll consider.

root - tiend yien - "means medicine".

ny could be ny/ni at least when that's needed to disambiguate from homonyms that would otherwise occur. I guess the language can have homonyms that doesn't compete in the same semantical space (?) - that is, that isn't used in too similar places/contexts, so they will still be easy to keep apart.

muot translates back as mother. hm.

Rabbits → Rabuche
rabbits → winyo

Kinde moro ne nitie winyo matindo ang’wen, kendo nyingegi ne gin Pamba-yiel, kod Petro. → turkeys, wth?

small bird
winy matin
small birds
winy matindo

verkar som att adjektiven har pluralformer.

hare - dipuo, big hare - dipuodo maduong'.