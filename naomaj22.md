# Pandunia survival kit

## Pronouns and some structure

* **mi, mimon**
* **tu, tumon**
* **ye, yemon**

* **e ~ o ~ ama ~ a max**

  and ~ or ~ but ~ also, [too]

* **ni e go**

  this and that

* this and that or that, but also that

  **ni e go o go, ama a max go**

## Basic phrase structure
the woman speaks
the cat eats

the woman will sleep
the cat slept
the woman spoke

### With pronouns
With pronouns, the **ya** can be, and is usually, skipped

I speak
I speak Pandunia
I want to eat
I ate
I will sleep
mi WILL some

I have eaten
I had eaten

I am sleeping|eating
I was sleeping

do you understand?
[ca] tu aha?

what did you eat?
sorry, what did you say?

I said that I've never heard of Pandunia
I think that this is very good
ki

jo

# Pandunia
Pandunia pronounciation is quite straightforward. Keep in mind that **x** is pronounced as _sh_ in shall, and **c** as _ch_ in chitchat. **v** is pronounced as English w, and **g** is always hard. Also, the vowels are not diphtongs as in English, but simple pure vowels as in German, Spanish(, Finnish)or Japanese(?). Please refer to the [pronunciation section](https://pandunia.info/english/003_baze.html#letters-and-sounds) in the grammar for further details.

In less time than it takes to learn the basics of expression in Toki pona, you should be able to express basic things and understand the general grammar of Pandunia.

Let's start!

## Basic phrases

<!--~The word for _person_ in Pandunia is **jen** and _speak_ is **basha.** Between the doer and what is done, the word **ya** is used to keep them apart. _the_ is **la,** but it's not mandatory in simple phrases.~-->

The word for I_ in Pandunia is **mi,** and _speak_ is **~baxa~loge.** 

Between the doer and what is done, some kind of marker is used to keep the doer and the doing apart. The standard marker is **ya,** in itself meaning _yes._

**you speak**

  mi ya loge

~We will get back later to other markers that can be used,~ There are also other markers that can be used, but we'll stick to just **ya** for now. _to eat_ is **[yam/yame]** in Pandunia 

**I eat**

  mi ya [yam]

_you_ about one person is **tu**

**you speak**

  tu ya loge

_and_ is called **e**

**you speak and I eat**

  tu ya loge e mi ya [yam/yame]

For the sake of people who are not used to the alphabet (Chinese people, for example), Pandunia doesn't normally use capital letters

**I speak Pandunia**
  mi ya loge /la?/ pandunia
<!-- (you/I speak nonsense) -->

_to understand_ is *aha*

I understand

you eat food

you and I eat food and drink water

To negate a verb, you use **no**

I don't understand
you do not listen

do you understand?
do you see this?

The marker between the doer (the subject) and the doing (the verb) is actually not always needed, but it is (probably) better to first get in the habit of using it, so we will still stick to using it /for now/.

~**the animal eats**~
~[ZOE] ya yam~
~_woman_ is **[fem].**~
~_cat_ is **[mau]**~
~_man_ is **[man].**~

* mi
* marker – ya
* loge
* [yam/yame]
* tu
* e
* aha
* questions – ca
* ya as a basic marker
* ca for forming questions

## Some politeness

hello, hi
salam

salam is the general go-to greeting in Pandunia. You can also say **halo** for hello. salam is also used in other types of greetings.

+welcome
+good night
goodbye
see you

sorry
thank you

please

## Basic tense/time

pas
xa

[le i pan.pdf men ej ordlista]

## ~Other subjects~ / When the verb marker isn't needed / Pronoun subjects
The reason why there is need for markers such as ya, pas, xa and le between the subject (generally the doer of an action) and the verb, is that many words can serve both as subjects and as verbs. Consider the following example.

**mau yam yo pexe**

It can be interpreted both as "a cat eats some food", and "cat food (is) some fish" [a bit unclear since declarative? sentences hasn't been explained yet]. **mau ya yam yo pexe** is therefore clearer.

<!--
~With personal pronouns (I, you, he, she, it, we, you (all), they) the **ya** can be omitted. There is no ambiguity from doing so, because pronouns never form compound expressions with other words/are never combined with other words to form compunds.

Because pronouns /never form compounds with other nouns ["you all" could be said?], and because they/ ~-->
Pronouns can not be used as verbs, so phrases with pronouns as the subject doesn't need markers. It is usually skipped in those phrases, but I chose to include it in the first sentences still, for you to getting accustomed to using it.

**mi yam yo pexe**
**mi loga yo pandunia**
**tu aha**

/I'm thinking/ it's probably easier to learn to skip something later on, than to add something. It's also better to over-include markers, then to miss too many.

The marker can also be skipped if the phrase is still clear without it, for example with a verb that cannot be a noun.

**pexe si mau yam**

yesterday I met a cat. the cat ate some fish.

## More questions
As you have seen, Pandunia uses the same basic word order as in English. Unlike in English, though, the word order is kept the same when asking questions with question words, and the question word is put in the same place that the corresponding word would be in a normal/regular phrase/in a statement.

I think that this is an apple

you say that you think this is good
I understand that you think so

I understand why you think so

the woman that/who/which I saw was bigger

<!--
https://github.com/barumau/pandunia/blob/master/english/pandunia-english.md

Hansa Group Ltd // Hansa Trade Group Gsmbh/Gmbh (Hansa Trading Denmark A/S)

The Hansa Group is a multinational trading institution/company with focus on trade in Northern Europe. Together with local entrepreneurs, we deal with large scale  international trade of locally sourced in-demand products such as fish, skins, and cheese (?), but also cloths, bricks and clay for buildings and castle and fort construction.

With Lübeck Consulting /AG/, we closely cooperate/work in close cooperation with local courts and rulers, towards our vision of an economically stronger/sustainable Northern European trading zone. We always puts the consumers and the produceds/[omvänt] first/The producers and the consumers are always our first priority/interest/concern. We are proud to have been working successfully in several cities towards our goal of less excrements in the streets by 1429.
bad/contageous airs.

UPP The Hansa Group has trading rights in many Northern European cities and kaupings, and our home domain is trading via the Baltic Sea. We have established successful local offices in cities such as Bergen, Copenhagen, and well-fortified Visby. Our main offices are located in beautiful and well fortified Lŭbeck (where we also have a very big number of armored knights at our disposal). We also have a good cooperation with the Teutonic Order of Knights in the East.

We believe in an economically strong Northern European area, with free trade between cities and kaupings|free flow of goods through the rapid waterways bwtween the (Baltic) cities and caupings/other trade areas/areas of trade.

If you are a local entrepreneur interested in joining Hansa Trading Group, please contact a sheriff/plaintiff?? in your nearest area of trade.

-->
# Nao

This ~is~ was initially a shameless ripoff of manmino.github.io/learn. Credit and kudos to the creators of that course!

## Lesson 1 – Basic phrases

The word for _I_ is **me**

The word for understand is **hom**

<details>
<summary>

**I understand**

</summary>

  me hom

</details>

The word for _you_ is **te**

<details>
<summary>

**you understand**

</summary>

  te hom

</details>

To negate a verb, you use **no** ("not")

<details>
<summary>

**I don't understand**

</summary>

  me no hom

</details>

<details>
<summary>

**you don't understand**

</summary>

  te no hom

</details>

The word for _this, that, it_ is **ta**<!--_This, that_ and _it?_ We use ta for that. (And this, and it.)-->
<details>
<summary>

**you understand this**

</summary>

  te hom ta

</details>

<details>
<summary>

**I don't understand this**

</summary>

  me no hom ta

</details>

**jo** means to have

<details>
<summary>

**I don't have that**

</summary>

  me no jo ta

</details>

<details>
<summary>

**you have it**

</summary>

  te jo ta

</details>

The way to say _say_ is **ba**

<details>
<summary>

**I say that**

</summary>

  me ba ta

</details>

<details>
<summary>

**you say this**

</summary>

  te ba ta

</details>

The way to say _want_ is **moi**<!--Want to say _want_? Use **moi.**-->

<details>
<summary>

**I don't want (to)**

</summary>

  me no moi

</details>

<details>
<summary>

**you don't want that**

</summary>

  te no moi ta

</details>

Now you have learned the following words:
* me
* te
* hom
* ba
* jo
* moi

## Lesson 2 – Questions

To turn a statement into a question in Nao, you use the word **ca** before the statement. The word order, however, is the same as in a regular statement.
<!--
As we saw earlier, _you understand_ is **te hom.**-->

<details>
<summary>

**do you understand?**

</summary>

  ca te hom

</details>

<!--**ta** basically means _it._

<details>
<summary>

**do you see it?**

</summary>

  ca te kan ta

</details>-->

Since question words such as **ca** always shows if a phrase is a question, the questionmark isn't normally used in Nao.

Do you remember the word for _this, that_ and _it?_

<details>
<summary>

**do you understand it?**

</summary>

  ca te hom ta

</details>

_yes_ is **dei,** _no_ is **deu**

<details>
<summary>

**yes, I understand**

</summary>

  dei, me hom

</details>

<details>
<summary>

**no, I don't understand this**

</summary>

  deu, me no hom ta

</details>

_you want to eat_ is said "you want eat" in Nao, and _eat_ (and _drink_) is called **gin.**

<details>
<summary>

**do you want to eat?**

</summary>

  ca te moi gin

</details>

<details>
<summary>

**yes, I want to eat**

</summary>

  dei, me moi gin

</details>

The word for _can, be able to_ is **mo.**<!--/_can, be able to_ is **mo.**--> Remember to keep the same word order as in a statement/a normal phrase.

<details>
<summary>

**can you understand this?**

</summary>

  ca te mo hom ta

</details>

<details>
<summary>

**yes, I can understand it**

</summary>

  dei, me mo hom ta

</details>


<details>
<summary>

**you can say that**

</summary>

  te mo ba ta

</details>


<details>
<summary>

**can I say this?**

</summary>

  ca me mo ba ta

</details>

<!-- eget kapitel? -->
There is one other way of making questions in Nao, and that is with using question words like the English _which, who, how, when_ and so on. **ke** means _what,_ which also doesn't need any questionmark.

<details>
<summary>

**you say what? / what are you saying?**

</summary>

  te ba ke

</details>


<details>
<summary>

**I eat what?**

</summary>

  me gin ke

</details>

You can put **ca** in front of a question with ke too – **ca me gin ke** – but it isn't necessary and normally skipped/not used in such phrases.

The word for _do_ (something) is **fa**
<!--**fa** means _to do_ (something)-->

<details>
<summary>

**what are you doing?**

</summary>

  te fa ke

</details>

The most common greeting in Nao is **joimit** (which literally means meeting-related being-happy). It is also friendly and nice to ask how others are doing, at least as a politeness. Then you use **najan** for _feel [EXACT MEANING?],_ and literally ask "you feel what/what-ly". <!--The position of li here makes it also possible to read/interpret as _how._-->

<details>
<summary>

**hello, how are you? / hello, how do you feel?**

</summary>

  joimit, te najan ke?

</details>

The word for _good, nice_ is **li**
<details>
<summary>

**I'm good, thanks! / I feel good, thanks!**

</summary>

  me najan li, joikohunfali

</details>

Good work! The other question words are expressed with compounds of **ke** and other words.

Now you have learned a lot!:
* ca
* dei
* deu
* mo
* fa
* joimit
* najan
* joikohunfali
* how to form questions

## Lesson 3 – Basic time / Some time

It is very simple to express the simple past tense in Nao – you just add the particle **ki** before the verb.

**did you understand?**

</summary>

  ca te ki hom

</details>

<details>
<summary>

**yes, I understood**

</summary>

  dei, me ki hom

</details>

_but_ is **keu**
<details>
<summary>

**I wanted to understand, but I couldn't**

</summary>

  me ki moi hom, keu me no ki mo

</details>

<details>
<summary>

**what did you say?**

</summary>

  te ki ba ke

</details>

<details>
<summary>

**what did you eat?**

</summary>

  te ki gin ke

</details>

_to sleep_ is **son**
<details>
<summary>

**I slept**

</summary>

  me ki son

</details>

Future tense is as easy as past tense ~ you just instead add **ku** before the verb.

<details>
<summary>

**I will sleep**

</summary>

  me ku son

</details>

<details>
<summary>

**I didn't understand, but I will (understand)**

</summary>

  me no ki hom, keu me ku hom

</details>

Now you have learned the following:
* ki
* keu
* son
* ku
* simple past and simple future tense

## Lesson 4 – Opposites and modifiers

As you have seen, _yes, true_ is **dei** and _no, false_ is **deu.** Furthermore, **ki** is past tense, while **ku** is future tense. These words demonstrate the pattern of word pairs with opposite meanings in Nao.

Do you see the pattern too? If I tell you that **pi** means _small,_ what do you think **pu** means?

**pu**

  big
  

**lu** means _bad, [badly ej i dict]_

**li**

  good, [well ej i dict]


and is **din**

<details>
<summary>

**good and bad**

</summary>

  li din lu

</details>

Can you guess how to say this?

<details>
<summary>

**good or bad**

</summary>

  li dun lu

</details>

You should also know that words that doesn't have a reasonable opposite are not part of an opposite pair, even if the word has an u or an i in it. Therefore/Thus, **ris** (rice) is not the opposite of **rus** (Russian).

[vad som är en motsats]Some learners struggle with the difference between not something and an opposite. un- is a way to make [hmm, negatives] in English. [ETC] lau, lai, no lau. You can read more about that in the Nao textbook.

a good person
da li

a big person
da pu

a small house
dom pi

a good house
dom li

a big good house
dom li pu

a small good house
dom li pi

a good small house
dom pi li

a bad house
dom pu

you speak well
te ba li

the person speaks quickly
da ba foi

she speaks quickly
mita ba foi

dark, light

## Lesson 5 – More time

ki, ku repetition
niki

## Lesson 6 ~ The conjunction

pe

## drafts
<details>
<summary>

**do you have bananas?**

</summary>

  ca te jo 'banan

</details>

<details>
<summary>

**no, we don't have bananas today**

</summary>

  deu, jume no jo 'banan [today]

</details>

<details>
<summary>

**do you want a banana?**

</summary>

  ca te moi 'banan

</details>



<!-- There are no articles in Nao, so **dom** means both _a building (or house)_ and _the building (or house),_ and **'banan** _banana, the banana, a banana._

You see that apostrophy in the beginning of banan, by the way? That means it is a word that doesn't follow the normal rules for syllables, and it is also always used before names. Capital letters are not used in Nao. _John and Maria_ is therefore **'jon din 'maria** in Nao.

_Big, large_ is **gepun** in Nao, and _is, are, am_ is **bio.** And you also know now how to say _and_ - **din.** Maybe you will now be able to translate these two phrases:

<details>
<summary>

**I and you see a house. The house is big.**

</summary>

  me din te kan dom - dom bio gepun -

</details>

As you see, Nao uses "-" instead of period between sentences. This is because it doesn't use capital letters, to make it visually clearer where a phrase ends. The lack of capital letters makes Nao a bit easier for people not used to Latin letters (since capital letters often differ in shape from the non-capital ones, in ways that might make them hard to be identified without knowledge).

To turn a statement into a question, you use the word **ca** in the beginning of the phrase. This makes a question mark superfluous, and using it is considered informal.

<details>
<summary>

**I see a house. Do you see the house?**

</summary>

  me kan dom - ca te kan dom -

</details>
-->

din

the position of ke in te najan ke is potentially the same as a modifier, and the asking what about a verb is "how, in what way", so "te najan ke" can also be interpreted as "you feel how?"

## Lesson 3
Verbs without marker words are not tied to a tense form, so moi, ba, jo and hom can also be about past or future tense, depending on context. We'll get back to tense marker words later on.

When clearly talking about the past, how could the following be expressed?

<details>
<summary>

**I wanted that**

</summary>

  me moi ta

</details>

<details>
<summary>

**you had that**

</summary>

  te jo ta

</details>

<details>
<summary>

**you didn't understand**

</summary>

  te no hom

</details>

fa
ba
fo
mo
gui
go
tu
sa

me fo pe ta bio li
me fo pe ta li

The word for _see, look at_ is **kan.**

<details>
<summary>

**I see**

</summary>

  me kan

</details>

he

duiba
joimit
nesa - inform

[när ta används för it]
Remember that **ta** also means _it?_
