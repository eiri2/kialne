## notoj
_internaciaj-svedaj notoj_

<!--iu alia antaŭa?-->
### La malgranda venko de Trump

https://en.wikipedia.org/wiki/List_of_United_States_presidential_elections_by_popular_vote_margin – Trump venkis inter la voĉdonantoj per 1,68 % mariginalo – la 16-a plej granda mariginalo ekde la dua mondmilito, de 19 elektoj entute.

Tio do estas **la kvare plej malbona mariginalo ekde la dua mondmilito.** Ĝi ja estas pli bona mariginalo ol sia antaŭa venko en 2016 (mariginalo de –2,09 %, do negativa), la due plej malbona mariginalo (la tri aliaj estas Kennedy 1960 kaj la unuaj).

Laŭ artikolo, la Demokrata partio kaj amaskomunikiloj tute kredis kaj pli-disvastigis la ideon ke la je-voĉdonanta venko estis granda aŭ decida, kaj mi supozas ke Trump kreis tiun ideon.

La principo ke homoj pli emas kredi grandaj mensogoj, ĉu? (Kaj jes, kompreneble ne estas la voĉdonantoj kiu fakte elektas.)

### Kablo

_De la 20a novembro, 2024._

En la Balta Maro, iun ŝajne rompiĝis komunikilajn kablojn inter Finnlando kaj Germanio, kaj Svedio kaj Litovio.

Fontoj en la registaroj de la landoj kredas ke temas pri sabotado, kaj ne akcidento. Movado de ĉina ŝipo, veturante de Rusio, kongruas kun la rompig-tempoj de la kablojn, kaj la germania ministro de defendo, Boris Pestorius, diris ke oni suspektas tiun ŝipon por la sabotado.

Dana milita ŝipo postiras la ĉina ŝipo, kiu ankris en Kategato en la nokto de mardo.

(svede: https://www.svt.se/nyheter/inrikes/minister-carl-oskar-bohlin-m-om-kabelbrotten-fartygsrorelser-vacker-fragetecken – https://www.aftonbladet.se/nyheter/a/JbLQgm/kinas-fartyg-skuggas-av-danmarks-militarfartyg)

### Stato de Ukrainio (kaj Rusio)

_Pro diskuto de la stato de homaj rajtoj, korupcio ktp en Ukrainio._

Ukrainio estas je 0,63 en indekso de homaj rajtoj kun skalo inter 0 kaj 1, en jaro 2023. Ia organizo nomita _Freedom House_ rankas landojn laŭ libero, kaj rankas Ukrainion en 2024 kiel ’parte libera’, kun 49 poentoj de 100.

Pri korupcio, Ukrainio en 2023 havis 36/100 poentojn da perceptita malkorupcio (pli alta estas pli bona), ranko 141 de 180. Ekde 2013 la poentoj da korupt-percepto kreskis per 10 poentoj, aliavorte la Ukrainia korupcio malkreskis – ankaŭ dum la milito (per 4 poentoj, ekde 32 en 2021).

Rusio estas je 0,30 en la indekso de homaj rajtoj, kaj kun 13/100 poentoj de libero en _Freedom House_ oni rankas ĝin kiel ’ne libera’. Pri korupcio, oni havis 26 poentojn da malkorupcio en 2023, ranko 141/180. Inter 2012 kaj 2022, oni ĝenerale havis ĉirkaŭ 28–29 poentojn, nek kreskante, nek malkreskante.

**Progresaj detaloj**

La poentoj de kaj homaj rajtoj, kaj libero falis en ambaŭ landoj dum la milito, ekde 2021. En Ukrainio, homaj rajtoj estis ĉe 0,76 poentoj en 2021, kaj en Rusio ĉe 0,42. En Ukrainio, inter 2001 kaj 2009 tiuj poentoj kreskis el 0,71 ĝis 0,85, falis ĝis 2014 al 0,61, kaj kreskis denove al 0,76 en 2020 kaj 2021 (0,74 poentoj en 1998). En Rusio, ili falis ekde 1998 de tiama 0,72 poentoj, kaj eĉ pli rapide post 2021.

La progreso de libero laŭ Freedom House falis en Ukraino de 60 poentoj en 2021 ĝis nuna 49 (iel interesa progreso 62-60-61-50-49 ekde 2020), kaj en Rusio de 20 ĝis nuna 13 (progreso 20-20-19-16-13 ekde 2020).

<center>*</center>

Ni jam scias, mi pensas, ke Rusio (la ŝtato/sistemo) multe pli malbonas ol Ukrainio, sed estas interese ankaŭ vidi tiujn nombrojn.

<!--~~sed estas interese vidi iajn mezurojn pri Ukrainio pro tio ke ĝi estis pridiskutita.~~

U L (falo de 60 pintoj en 2021 kaj eĉ 62 la jaro antaŭ tio) (61 en 2022, 50 en 2023) 

R HR (falo de 0,42 en 2021)
R L falo de 20 en 2021

(la 40a plej korupt-percepta lando en la indekso)-->

**Fontoj**
* ourworldindata.org, V-Dem: [Human Rights Index, 2023 – Civil liberties index](https://ourworldindata.org/grapher/human-rights-index-vdem), [v-dem.net](https://v-dem.net/data_analysis/CountryGraph)

* Freedom House, Freedom in the World Country Report: [Ukrainio](https://freedomhouse.org/country/ukraine) kaj [Rusio](https://freedomhouse.org/country/russia)

* Transparency International, transparency.org, Corruption Perceptions Index: [Ukrainio](https://www.transparency.org/en/cpi/2023/index/ukr), [Rusio](https://www.transparency.org/en/cpi/2023/index/rus) (pli longaj serioj kun klaraj grafoj: [Ukrainio](https://tradingeconomics.com/ukraine/corruption-index) [Rusio](https://tradingeconomics.com/russia/corruption-index))

<!--https://www.state.gov/reports/2023-country-reports-on-human-rights-practices/ukraine
https://www.amnesty.org/en/location/europe-and-central-asia/eastern-europe-and-central-asia/russia

pluaĵoj: demokratio. feliĉo, cetere.-->