A workspace for translating the Pandunia grammar to Swedish.

**B01_baze_kanun.md**
# 1. Pandunias grundregler

Detta är pandunias grundläggande regler.
De beskrivs i större detalj senare i detta dokument.

### (1) Världsord

Pandunia är ett viktat/jämlikt globalt språk/i jämvikt.HMM
Internationella ord har lånats från alla delar av världen till pandunia, och har anpassats till pandunias uttal och ortografi/stavning.
Ett grundord antas och ytterligare ord skapas från detta utifrån regel 10.


### (2) Stavning och uttal

Stavningen är enkel och regelbunden. Varje ord uttalas exakt som det skrivs. Nästan varje bokstav och bokstavskombination anger samma ljud varje gång.


### (3) Regelbunden betoning

Rotmorfem betonas på stavelsen som kommer före ordets sista konsonant, t ex
**háu** ('trevlig'), **dúnia** ('värld'), **báshe** ('språk'), **amén** ('amen').
Härledda/avledda och sammansatta ord betonas utifrån sina beståndsdelar, så att den viktigaste delen får huvudbetoningen och andra beståndsdelar kan få bibetoning, t ex
**trabáshe** ('översätta'), **dúnialìsme** ('globalism'), **bàshe skóla** ('språkskola').
Huvudbetoning markeras här med akut accent (*á*) och bibetoning med grav accent (*à*).


### (4) Pronomen

De personliga pronomenen är:  
**mi**
'jag',
**tu**
'du',
**da**
'hon, hen, han, den, det',
**vi**
'vi',
**yu**
'ni',
**di**
'de'.

De possessiva pronomenen är:  
**mi se**
'min',
**tu se**
'din',
**da se**
'hennes, hens, hans, dess',
**vi se**
'vår',
**yu se**
'er',
**di se**
'deras'.

Frågepronomen:
**vat**
'vad',
**hu**
'vem',
**hu se**
'vems'.


### (5) Substantiv

Substantiv har endast en form, alltid samma.
Deras form påverkas inte av antal, genus eller kasus.
Antal visas genom tal eller kvantitetsord/ord som visar mängd.
Deras roll visas genom ordföljd eller genom en preposition.

### (6) Tal

Grundtalen är:  
0 **siro**, 1 **un**, 2 **du**, 3 **tri**, 4 **for**, 5 **faif**, 6 **sixe**,
7 **seven**, 8 **eite**, 9 **nain**, 10 **ten**.  
Större än tio:
11 **ten un**, 12 **ten du**, 13 **ten tri**,
osv.  
Tens:
20 **du ten**, 30 **tri ten**, 40 **for ten**,
etc.  
Hundreds:
100 **un hunde**, 200 **du hunde**, 300 **tri hunde**,
etc.  
Thousands:
1000 **un tauzen**, 2000 **du tauzen**, 3000 **tri tauzen**,
etc.

När ett tal kommer efter ett substantiv, så blir det det motsvarande ordningstalet:  
**parte un**
– del ett, första delen  
**parte du**
– del två, andra delen  
**parte tri**
– del tre, tredje delen


### (7) Attribut

Adjektiv och adverb har samma form.
Adjektivattribut kommer före de substantiv som de definierar,
och adverbat§tribut kommer före de substantiv som de definierar.

**un rapid loga**
– Ett snabbt tal.  
**tu rapid loga.**
– Du talar snabbt.


### (8) Verb

The verb does not change in person, number and tense.
Auxiliary verbs indicate time.

- **bi**
  indicates an ongoing event.
- **haf**
  indicates a completed event, which has an effect on the present situation.
- **did**
  indicates a past event.
- **vil**
  indicates a future event.


### (9) Word order

The word order is subject–verb–object.
The same order is used in declarations and questions.

A passive sentence is created with the help of the passive auxiliary verb
**be**.
A similar effect can also be reached with the impersonal pronoun
**von**.

**pandunia be loga.**
– Pandunia is spoken.  
**von loga pandunia.**
– One speaks Pandunia.

In the _pivot structure_, the object of the transitive verb functions as the subject of the following verb.  
**mi plis tu loga pandunia.**
– I ask you to speak Pandunia.

Pronouns can be left out when they are obvious and redundant.  
**_mi_ plis _tu_ loga pandunia.**
→ **plis loga pandunia.**  
– Please speak Pandunia.


### (10) Word building

In Pandunia, words change only when their actual meaning changes.
Words don't ever change only to serve in a different grammatical role.
Compound words are made by combining the elements that form them.
The main word stands at the end.

**poste**
('mail') +
**kase**
('box') =
**poste kase**
('mailbox')



02-ngt

# Pandunias grunder/grundpelare

Pandunia är ett konstruerat språk med minimalistisk grammatik och globalistiskt ordförråd.

### Bokstäver och ljud

Pandunia använder en systematisk stavning där
varje bokstav representerar ett uttalat ljud.

> A B CH D E F G H I J K L M N O P R S SH T U V X Y Z

The vowels **a, e, i, o, u** are pronounced as in "are there three or two".

Konsonanterna är:
**b**,
**ch** (uttalas som _ch_ i engelska _church_),
**d**,
**f**,
**g** (alltid hårt),
**h**,
**j**,
**k**,
**l**,
**m**,
**n**,
**p**,
**r**,
**s**,
**t**,
**v** (som engelska _w_),
**sh** (som engelska _sh_),
**y**,
**z** (som engelska _z_).

### Ord som aldrig ändrar sig

Ord böjs eller ändras aldrig i pandunia
– inte ens när de byter ordklass!
Samma ord, ~~helt utan förändring,~~ kan helt oförändrat användas som substantiv, adjektiv eller verb.

### Personal pronouns

| Person   | Singular          | Plural       |
|:---------|:-----------------:|:------------:|
|          | **mi**            | **vi**       |
| 1st      | _jag, mig_        | _vi, oss_    |
|          | **tu**            | **yu**       |
| 2nd      | _du, dig_         | _ni, er_     |
|          | **da**            | **di**       |
| 3rd      | _han/hon, honom/henne_ | _de, dem_ |

### Grundläggande meningsbyggnad

Standardordföljden är subjekt–verb–objekt
– samma som på svenska!

**mi love tu.**
– Jag älskar dig.

Grammatiska ord hjälper till att urskilja subjekt, verb och objekt.
Subjekt och objekt är substantivfraserKOLLA och de börjar ofta med ett grammatiskt ord,
som ett demonstrativt pronomen, ett tal, eller en annan bestämmare.KOLLA
Verbet utgör verbfras och det börjar ofta med ett grammatiskt ord,
som en jakande eller en negerande partikelTERMER, ett hjälpverb, eller en tids-, aspekt- eller MOOD-markör.

Exempel 1. Strukturellt oklar/mångtydig mening.

    mau yam fish.
    katt äta fisk
    'Katten äter fisk.' (trolig tolkning av innebörden)
    
Exempel 2. Strukturellt tydlig mening med jakande av verbet.

    mau ye  yam fish.
    katt ja äta fisk
    'Katten äter (verkligen) fisk.'

Exempel 3. Structurally clear sentence with the negation of the verb.

    mau no  yam fish.
    katt nej/inte  äta fisk
    'Katten äter inte fisk.'

Exempel 4. En komplex men strukturellt tydlig mening.

    mi se frende  haf    yam un  pai.
    jag -s vän    ASPECT äta en  paj
    'Min vän har ätit en paj.'


### Negation

Lägg till **no** före ett ord för att negera det.

**mi no sona.**
– Jag sover inte.  
**tu yam no von.**
– Du äter ingen.DÅLIGT_EXEMPEL

### Att vara

Verbet
**es**
betyder 'att vara'.
Det kan utelämnas i väldigt enkla meningar.

**mi es home.**
– Jag är en människa.  
**mi – home.**
– Jag (är en)/en/ människa.

Det kan utelämnas när det är i en följd av verb.WHAT?_EXEMPLEN

**mi van es mau.**
– Jag vill vara en katt.  
**mi van mau!**
– Jag vill ha en katt!DÅLIGT_EXEMPEL

### Ord med flera funktioner

Ord i pandunia kan vara tvetydiga, för de har ofta fler betydelser jämfört med svenska ord.

**da ye love mau.**
– Han älskar katter. / Hon älskade en katt. / Hen kommer att älska katten.

Men betydelsen är (egentligen) klar och tydlig i det faktiska sammanhanget:

**pre tri nen, mi have du mau e un vaf. mi have plus un fem ben. da ye love mau!**
– För tre år sedan hade jag två katter och en hund. I also had a daughter. _She loved the cats!_

### Tid

Tenses and aspects are optionally expressed with the help of adverbs and auxiliary verbs,
like the aspect auxiliaries
**bi**
(be in the process of), and
**haf**
(have completed),
and the tense auxiliaries
**did**
(in the past), and
**vil**
(in the future).

**mi bi love tu.**
– I am loving you.  
**mi haf love tu.**
– I have loved you.  
**mi did love tu.**
– I loved you.  
**mi vil love tu.**
– I will love you.

### Passive

The passive voice is formed with the help of the helping verb
**be**.

**tu be love.**
– You are loved.  
**mi be love of tu.**
– I am loved by you.

### Modifying other words

To modify a noun, put adjectives before it.

**un nove love**
– a new love  
**da gud pai**
– the good pie

To modify a verb, put adverbs before it.
The adverb can be tagged with
**li**.

**mi gud (li) vize tu.**
– I see you well.  
**tu vize li love da.**
– You seemingly love him/her.

Modifiers are compared with
**mor**
'more, -er',
**mor... dan hol**
'the most... (of all)',
**les**
'less',
**les... dan hol**
'the least... (of all)', and
**sim**
'equally, as'.
The point of comparison is introduced with
**dan**
'than, as'.

**tu es mor yun dan mi.**
– You are younger than me.  
**tu es mor yun dan hol.**
– You are the youngest of all.

### Questions

To ask _yes–or–no_ questions, replace the verb with "(verb) **no** (verb)" pattern or add
**he**
'eh, huh' to the end of the sentence.

**tu vize no vize mi?**
– Do you see me?  
**tu vize mi, he?**
– You see me, eh?

To ask a content question, write a normal sentence and replace the word in question with
**vat**
'what' or
**hu**
'who'.

**tu love hu?**
– Who do you love?  
**tu love vat man?**
– Which man do you love?  
**vat man ye love tu?**
– Which man loves you?  
**tu love da hau muche?**
– How much do you love him?

### Commands

To state a command, leave out the subject and start the sentence with the verb.

**yam da pai!**
– Eat that pie!  
**vize da mau!**
– Look at that cat!