## miribla turi a Nils Holgersson

_'by' Selma Lagerlöf, nalezifia da SWEDISH_
*miribla turi per' SWEDEN a Nils Holgersson* si bona ni demamisa nare/buke 'by' Selma Lagerlöf (1858–1940), ki si un da masuno mas bona kitcaney a 'Sweden'. lo si un a aney ke ane/'swedes' una temomeni'/meni tem, os loy meni tem 'Swedish' kitcaney, ni lo avesi 'Nobel prize' a kitceyare |som första kvinna? när?|. nare osi en 'Sweden' os prekoso 1900-ey.

### batide

#### 'tomte'

semdi' 20 martsie.

yos si batide. lo si ruo 14' yare, longa ni burusima' ni gorocera. lo ne si bona por/na multa: lo masomo voli gofi ni nyami, ni posos tie lo ami fari deboneye.

nuo si semdia diese, ni pey a batide so pretesi nas iti na prezene.

batide suló sedi in kamice sop runde a meze ni meni tem /ti bonsorta/kiom bonsorta (lo(INAN) si), ke olbi bape ni mape daiti/ekiti, sa lo vo 'fend for himself' os biare a/biara hore. ”nuo me abli cesi/pluki na ube<!--take down--> pawile a bape //da rupe//, sen pyan nudavi mesiti/mesinesi” lo peli na suló.

may nero si, sim bape sumi mene a batide, as teko ti lo pedi sop 'doorstep'/in dore ni preti iti', lo stopi ni roti'/roiti na lo/batide. ”as tu ne voli iti na prezene kon mape ni me,” lo peli, ”me meni, ti tu lumino abli legi 'preaching' domeno/'domo. ki tu voli, ti du vo i tie?”

”ha”, batide peli, ”tie me pote i”. ni lo senmeno meni, ti lo ne vo legi mas dan lo ivi.

batide meni, ti lo neyos preo vidi mape tiom waya. senvato lo si na/'over by the wall shelf', cesi 'postilla' a Luther, ni oti lo sop meze per'/med' glaspanele, kon 'preaching' a die apa'. lo nio api 'book of evangelies' ni oti lo lata 'postilla'. fino lo depuci gua 'armchair' na meze, ke si procesia os 'auction' in 'priest farm' in Vemmenhög (os) ja yare, ni<!--31 jan--> ki-en' alo neyan ale dan/e bape moci sedi.<!--s'/ es/ si/'g, på senaste fyra styckena, fast manuellt | också 31 jan alt. tidigare.-->

batide sedi ni cica meni, ti mape i suló upero turbe kon tia pretife, as balo ne navoli legi mas dan un u poka 'page'. may nuo si reo olomo tio, sim bape abli wi naliti lo. lo iti nas' batide ni peli kon stira vose: ”nuo rementí', ti tu legi reko/luo'! as en noy riiti, me vo kipeli<!--interrogate--> tu tem ola folye', ni sis tu lesi pya, sa tu ne seti bono<!--går det inte väl för dig-->.”<!--


https://piped.kavin.rocks/watch?v=oRIeytEXGhQ swedish fika

https://archive.org/details/1935-dezembro-15-domingo-correio-da-manha/mode/1up random fynd-->

may si nero, sim bape sumi<!--skulle ha gissat--> mene a batide, as reko os lo pedi en/pedeni dore<!--på tröskeln--> ni si preta nas iti, lo stopi ni roditi/roti na batide/lo. ”as/das tu ne voli iti na prezene kun mape ni mi”, lo peli, ”me meni, ti tu lumino abli legi 'sermon' domeno. ki tu (voli) juri, ti tu fari tie?”

<!--
https://piped.kavin.rocks/watch?v=_3eD172mP_I - @ 46 min ca, hon tror att en intelligent entitet är en dator. 🙂 :3


https://www.cfr.org/backgrounder/can-amazon-countries-save-rain-forest
https://rainforests.mongabay.com/amazon/amazon_countries.html-->