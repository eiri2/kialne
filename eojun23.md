## 15 sep
Mi pripensis ke se oni farus simpliĝitan (?) planlingvan version (basita) de la (rekonstruita) sumeria, oni povus diri pri ĝin “Ekparolu/Lernu paroli la sumeran kiel brutulo en nur tri monatoj!”

Mi ankaŭ estis dironta ke foje oni volas skribi sen scii kion skribi. Sed nu.

[Enmarkar kaj la rego de Aratta](https://en.wikipedia.org/wiki/Enmerkar_and_the_Lord_of_Aratta)  (angle)– ĉi tiu estis ege mojose!

## Nerajta defendo de perforto
En Brazilo, oni decidis ke argumento uzata por defendi perfortojn kontraŭ inoj, ‘rajta defendo de honoro’, ne temas pri rajta defendo, estas [“malaminda, malhumana kaj kruela”, kaj kontraŭkonstitucia](https://www.cnnbrasil.com.br/politica/stf-decide-que-uso-da-legitima-defesa-da-honra-em-crimes-de-feminicidio-e-inconstitucional).

Nu, tiu decido ege bonas, kompreneble, kaj mi tute samopinias pri ĝi – neniu nenie kaj neniam estu perfortita pro honoro. Tamen, ĝi ankaŭ ŝajnas malkaŝi/malkovri tristan ŝtaton de aferoj en la regno – ke tia argumento ja estis uzata.

Sed ja ĉiukaze estas paso en la ĝusta direkto. Bone farita!

(Cetere, ofte mi serĉas temojn por traduki al helplingvoj, ktp, sed foje temoj alvenas pri kiu mi volas skribi sen sufiĉe da energio aŭ tempo, ekzemple, por skribi en alia helplingvo. Tio estas kazo en kiu praktikas skribi ĉi tie.)

## M.I.A. kaj fasto kaj dormo

Kial ne iom da M.I.A.?

[mi (eble) aldonu ligilon, "all I wanna do".]

Mi ne certas pri ŝin ideologie (tiuj armiloj, ekze, kaŭzas demandojn) – miascie, ŝia patro estis militisto, kaj ŝi havas opiniojn pri tiu milito, kaj eble aliaj, kiu estas iom kvereligaj, miascie (impreso de intervjuo en kultura programo ĉe sveda televido). Mi ne diras ke mi trovas tion stranĝega aŭ malbona jam, tamen mi ne scias pri la detalojn. (Kaj ĉu mi volas scii? Mi celas, ĉu oni ĉiam volas havi la faktojn – kial? Ĉu scio en ĉio kazo estas pli bona ŝtato, aŭ celo en si mem? Nun ŝi estas malesplorita misterio, kiu mi iom ŝatas, kaj tiu stato estas iom ~~bona~~ agrabla, sentas iom bona.) (Interese kaj malofte, angla Vikipedio ne funkciis kiam mi provis akiri/malfermi ŝian artikolon tie. Eraro 503.) (Rapisto, diras Guglo. Mi neniam pensis tiel, laŭmemore – eble pro tio ke mi ne pensas ke la muziko malbonas.)

Mi ankaŭ provis fasti, propre, kelkaj semajnoj antaŭe. Trinkis akvon, kaj celis manĝi iom da salo (tiel oni evitas kapodoloron kaj kutimaj problemoj, mi aŭdis, io pri rapide forlasi salon), sed mi ne certas ĉu mi vere faris. Iĝis – kiel mi antaŭe spertis dum pli mallongaj provoj – nervoza kaj tro fokuzigita – "MI DEVAS FARI ĈI TIUN ETAN AFERON" – sed mi ne planis ion gravan kaj ĉefe nur sidis lokigita farante kion mi komencis fari (skribi). Nu, ne multe problemis – mi ne devis fari manĝaĵojn ĉiuokaze. (Egspertulo/eglertulo kuirus por la postfasto!) (Ja, la ĝenerala malmovado ne sanegas, kompreneble.) Daŭris 26-27-ian horojn.

Do, mi ne suferis dum la fasto mem, sed poste dum pli ol semajno mi havis malfacilan periodon mense kaj emocie, kaj mi eble ne tute ~~ekiris,~~ eh, eliris tion ankoraŭ. (Okazis komence de julio.)

Mia celo tamen ne estis rakonti pri tiuj aferoj, sed alia sperto mi havis dum la fastado, aŭ pli ĝuste, post ĝi. En kutima tago mi manĝas nur dum fakte 6 aŭ foje 9 horoj – ne la tuta tempo, sed tio estas la nura tempo dum kio mi kutime manĝas (pli pri tio iu alia fojo, eble). La tiel nomita, kaj planita, ‘manĝo-fenestro’, fakte daŭras 9–10 horojn, tamen la afero estas ke post ofta plej granda manĝo post 6–7 horojn, mi ofte ne plu volas manĝi (dum la fenestro, kaj pro kutimigo, kutime ankaŭ ne post la fenestro), [mi ne malsatas] kaj do ne faras. Mi lastatempe iom pripensis ĉu ĉi tiu habito eble havas malbonajn psikiajn kaj emociajn efikojn. (Malsato kredeble ete maltrankviligas vin – la fasto nervozigis min, ekze – kaj pli facilas ekdormi se oni ne multe malsatas, sed iom satas. Ekdormo post granda kvanto de manĝaĵoj ankaŭ ne.. nu, vi komprenas.)

Kaj do ni alvenas al mia celo, finfine. Post la fasto, iam antaŭ enlitigo, mi ete manĝis. Kolbason, sekiĝitan, tiuj kiu oni ĉi tie nomas biero-kolbason – iu tielan kolbason, minimume – kaj ian _acidiĝan lakton, filmjölk._ Guglo traduko – en manko de la malfunkcianta Vikipedio – asertas ke ĝi nomiĝas jogurton, tamen tio estas nur alia speco de la afero (kaj kutime ĉi tie oni konsideras jogurton iel alia afero ol la kutima acidiĝan lakton, kiu tamen inkluidas aliaj fremdkulturaj (dusence) tiajn laktojn). Kio ajn.

Denove, la celo. Post la manĝo de tian bakterioplenan laktoprodukton kaj seka kolbaso post la fasto, mi lacigis. Kaj tio multe pli kaj pli frue ol kutime. (Honeste, mi kutime/ofte ne multe lacigas en tiu senco, dormeme.) Eble estis simple la manĝado, ĉu ne? Ne, mi iom provis (eĉ fermentitan brasikon), kaj ŝajnas ke estas la filmjölk kiu donas la bonan efikon. Mi provos ĝin plu!

Alian fojon, eble: acidaj ĉericoj kaj filmjölk kontraŭ (iaspeca simptomo/vario de) gastrito (post periodo kun Omeprazol, kiu oni ankaŭ ne tromanĝi – kreskiĝas riskon de galŝtonoj kaj fakte de demenco – tute mortigas substancon grava por la ko-, eh, ŝajne ĝi nomas sciiĝon).

¹ Kompreneble neniam fasti sen trinkado!

## Ne uzu Zopiklon – uzu Mirtazapin

Kara leganto! Mi volas averti vin de precipa medikamento por sono (kaj por trankvileco?): _Zopiklon_ (ankaŭ vendita kiel _Zopiclone_ kaj _Imovane,_ ktp). Mi havas spertojn de daŭra uzado kaj konsekvencoj de tio.

Dum jaroj kiam mi ne bone funkciis kaj ege ofte ne ekdormis, mi akiris Atarax, Propavan kaj Zopiklon de mia ~~medikisto~~ kuracisto. Ŝajnis al mi ke tiuj estis la nuraj alternativoj (sen pli danĝeraj), kaj nur Zopiklon funkcies vere bone. Atarax apenaŭ funkciis por ekdormo – foje mi povis uzi (kelkaj piloloj de) nur tio por ekdormo; de Propavan mi ja povis ekdormi, sed mi estis ege, **ege** laca la venonta tago, kaj apenaŭ ne funkciis. Mi sciis ke Zopiklon estis multe pli dangera (neŭrologike), sed ĝi, sen APPARENT SIDE EFFECTS, trankvile, pace kaj subtile ekdormigis min, multe agrabla sento.

Mi ne kredas ke mi iam aŭ multe insistis ĉe Zopiklon – mi demandis por bonaj alternativoj por ekdormig~~ad~~o, nur pli danĝeraj (minimume Stilnoct, eble pluaj¿) estis menciita de la kuracistoj, krom ke ni post iom da tempo povis provi melatonin. La malrapida RELEASE kiu ni provis ne estis la plej bona por mi, sed mi havis kaj havas pozitivan sperton de aliaj provoj de ĝi. Mi tamen ne povas ekdormi nur de ĝi (ĝi fariĝas min dormema, laca kaj trankvila, tamen), aŭ minimume mi tiam ne povis.

[dozo, Mirta, aliaj alternativoj, HOPS de sube]

**Mallonge dirite:** Zopiklon (kaj eĉ pli benzodiazepin¿-oj) manipulas kaj povas detrui vian trankvil-sistemon, farante vin ege ankciema por monatoj, jaroj aŭ eble eĉ pli longe. Mi ĉesis uzi ĝin pli ol 6 jaroj antaŭe nun, kaj mi suspektas ke la problemoj kiuj mi nun havas estas aŭ rekte INDIRECTLY kaŭzita de tiu uzado – biologaj konsekvoencoj de la uzado, aŭ psikologiaj sekundaj konsekvencoj de sufero kaj travivo de periodo de tiuj biologaj konsekvencoj.

Bonaj alternativoj inkludas melatinonon, magnesium¿, HOPS, sed se ne tion fariĝas vin ekdormi (HOPS estas la pli dormdona de tiuj por mi, mi ne scias ĉu mi povus ekdormi kaj funkcii kun nur tio, foje mi suspektis REBOUND/flankoefikoj – la aliaj havas positivajn¿ efikojn, sed ne sufiĉas por ekdormo por min), Mirtazapin estas agrabla kaj bonefika rimedo, miasperto. La unua afero scii pri Mirtazapin estas ke pli altaj dozoj funkcias pli malbone por ekdormiĝo, relative malaltan ranĝon (eble ie 3,5–20 mg) plej efikas por tio.

## Blogo

Mi nur pensis ke estus agrabla iam eble skribi ion esperante. Plendu aŭ ĝoji pri la ŝtato de la mondo. Aŭ skribi pri kio ajn.

Lastatempe, ekzemple, mi provis eviti fluoridon (kaj ankaŭ Florida, sed mi ĉiam evitas tion) (ĉu mi freneziĝis? nu, eble, mi komprenas ke povas ŝajni tiel) en mia dentopasto. Ĉi tio ne estas konsilo al fari la sama, fluoridon¹, kaj jes, mi scias ke oni ŝajnas esti frenezulo se oni evitas fluorido. Vi ja povas akiri problemon se vi tute evitas fluoridon, ĝi ja helpas mendi komencojn de truoj en la dentoj. Do, ĉi tio **ne** estas konsilo al eviti ĝin – mi volis provi, kaj ĉi tio estas raporto de miaj spertoj. [Oni ne aldonas fluoridon al la akvo mialoke, kaj mi kredas ke la kvanto de fluor/id/o en la akvo kiu mi trinkas ne havas altaj naturaj niveloj de fluorido.]

Unue, **ne** uzu dentopaston kun titanio-dioksido¿[kontrolu] – mi vidis iu himalaja dentopastoafero kun tio, sed ĝi estas iel forbarita en la EU (en manĝaĵoj), pro tio ke ĝi kredeble estas iom toksika kaj sane malbona. Ŝajne ekzistas plurajn aŭ kelkajn malfluoridajn dentopastojn kun tiu enhavero.

Mi havas (ankoraŭ) iom da akneon kaj ankaŭ etan ekzemon, kaj ankaŭ ankscio. Samtempe kun la dentopasto, mi ankaŭ provis (iom fuŝante la kontrolo de la eksperimento, kompreneble, sed oni povas plue kontroli postatempe) kuŝado dum etan tempon kun mia (nuda) doloro sur graso, do sur la grundo, por la mikrobaro de la haŭto (kiel en la ventro, la mikrobaro gravas por la sano de la haŭto, kaj kiam oni duŝas oni multe interrompas ~~kelkaj de la~~ plurajn de la mikroboj, la plej sensitivaj specoj).

La mallonga versio estas ke mia akneo kaj mia ekzemo pliboniĝis dum la uzado de nefluorigita dentopasto, kaj ke ni ne povas certi ĉu estis pro la dentopasto aŭ por la graskuŝado.

La pli longa versio mi eble aldonos, sed ĝi ankaŭ enhavas informo pri ke mia ankscieco tuj pliboniĝis kaj poste tion malpliboniĝis, kiu mi finfine noticis, kaj eble iom pli/plu poste komprenis kredeble ne temis pri manko (de fluorido), sed pri kion la dentopasto ja enhavis (iu alia afero, mi suspektas _limonen_-on), kaj ankaŭ informo pri la dentopastoj mi provis kaj la enhavaĵoj.

¹ oni ofte diras fluoro pri ĝin, sed en la dentopasto ne estas fluoro, sed fluorido. Ne ekzakte la samaj aferoj.
