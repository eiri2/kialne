swe i kwebose tuli e oPute, sya a oPute i sibu heyswa kwedoy, sun botegeba-byasogya swehey'. polu taw (i swa) e doy. hapa i lako e tay doy sogya pakapago [crown], sya a kwedoy pasa i hamu e (sun) [appear like] degwa.
> he lightly touch Putte, and Putte immedeatly become small, like blueberry king himself. everyting other is big. flower wear very big royal-headwear [crown], and plant appear (like) bushes.

sya oPute i botilusi/lusiboti'/dakiboti: ”mohu kya mohu a moybay i tay tey hay e haytako [baskets], a moybay i swa e guse kwetasey?” bemyu bya(kin) i pulisimi [whistle] boy' e swi kikaw-luta. moyswe i (lase) seni [swing] e haytako telu moyswe togakwegyu – toy pene dwi lase tapi a moyswe e deki.
> ”and Putte wonder: ”using what method we really can carry baskets [word order!?], we be too small?” but man whistle for[?] two squirrels. they put/swing basket on their backs – with some quick jumps they are gone.”

dyu san a oPute toy byakin i sami byo doy kamu dego mite pana-kikaw' taso i hadu boy keho. lon a moyswe i bani e doy kisa, pene(lon)' sopi teso a oPute i heyswa e buse boy deha, kile teso [even given the fact that] a moyswe i polulon gwimohu bamu.
> after that Putte with the man [by his side] walk across big magic land among hand-trees [palmtrees!] which shake [sway] in wind. when they meet big spiders, to_some_extent/sometimes happen that Putte become white [pale] on cheek, even that [even given the fact that] they always politely greeted.

dyu kwekumye (lon), a moyswe i buli boy' kiwe tiho/kesupla [lush] kikawdin, sya a botegeba-(bya)sogya i solebega: ”gwibuli/bulibamu boy bay sogyadego!” bemyu oPute i heypoke badeludi/doyludi e: ”moy bote pata i gapi boy kikaw, a bay i mye kwepolulon loy e mey sungoy!”
> after short (time), they come to [?] green leaf/healthfull [lush] forest, and blueberry-king/regent smile: ”welcome to my kingdom!” but Putte shocked-ly shout: ”blue apples hang in trees, I have never seen any similar_thing!”

”gwi a oPute, kyu i (gabi)kweteydalu [stupid], kay a kyu i kwe mapi teso a swe e geba! bemyu may<!--'/pinsibu/pindin--> a moybay i gwalun hoy bay!” swe i tosoludi e: ”i buli e hoyboy peke GULL-byamikya!” – sye gye oPute a peke botegeba-byamikya i tagi i kwepay e pay toga [stand, and bow]!

sya a botegeba-sogya i solegyu [smile]: ”(daw) i suke (hey)loy doke kwelaw e hoy kya daw a pin i tey swa. may, (a) oPute, a kya i dya kweday – i swa lase, a hoybay bote mikya [be fast, my blue children]!”
<!--Och blåbärskungen belåtet ler: »man säkert på utanskriften ser, hvems pojkar det här kan vara. Nu, Putte, ska’ du dina blåbär få, er raska nu, mina gossar blå, ni bruka ju vara snara!»-->

sya may a moygoy i tay dwi lase sopi: a moyswe i payplu i syubili i piki, sya dyu kwekumye a haytako i swa tay/plabey pla. moyswe i doyludi e ”i buli i sesey e moybay”, e ”kyu i dyu loy e tay moy tamye goy”. sya a oPute i swa sibu hey'dobu'.
<!--Och nu, ska’ ni tro, att det gick med fart: De klättrade, skakade, ref, och snart stod korgen till randen rågad.
»Kom nu och följ med oss», ropade de. »så ska’ du så mycket lifvadt få se!» Och Putte var genast hågad.-->

sya a oPute i heytewi' tubyo byo doy sugyo. tubyo e kikawtela [bark] sya a kehotiho [sails] e kikawtiho (doke o Acer). benu, kwepulolon a swe i mye heytamye hoy san dalu. sansibu/sandipu a swe i bitye bomi e bay geba: ”bemyu kyadin a swe i dyu seki lake-geba byo pindin? pindin e kwe kupe satobey, kay kwe?” [here is not sunny enough, is it not?]
<!--Och Putte fick segla på stora sjön. Af bark var båten och segel af lönn. Ack, aldrig han förr haft så roligt!
Då kommer han plötsligt ihåg sina bär: »Men hvar skall jag väl hitta lingon här? här är väl inte nog soligt?»-->

”bemyu i meti, a oPute. kyu i dyu loy. may a moybay i temu' (e) lakegeba-mawmya, a swe i dyu suke day e lakegeba u kyu! kyu i dole subi meti boy kwedoy sibu, a moybay i dyu basi e moybay pana e maba, i samyesyu e dita.”
<!--»Jo vänta, Putte, ska’ du få se, till lingonmor vi nu oss bege. hon säkert ska’ lingon dig skänka! Du måste blott vänta en liten stund, tills vi fått oss tvätta om hand och mun och laga att tänderna blänka!»-->

moybay i kiso boy tepyo mila, sya dya san a moybay i (kiso)potu byo bategeba-kikawdin. lon a moybay i kwesadibey mitu e kwesadi hoy kikawdin, a moybay i loy e (hede) pagidin [a plain] (hoy hede MOSS) toy kwetosegwa boy kyo.
<!--På åtta möss de satte sig opp, genom blåbärsskogen i vildt galopp gick sedan den lustiga ridten. När ändtligen skogens slut de nå, där syns en slätt af mossa grå med en liten hydda på midten.-->

kwelaw (gwa) a lakegeba-mawmya toy gwikihu gyu [dignified expression] i kiso toy polu swe mawmikya. moyswe i mye samye e lakegeba boy plahan. moyswe i dyu dobyoi e moyswe boy masoy-liso, bemyu mye i samye e moyswe boy dalu teso [? so that] a moyswe i samye [so that they shine – confusing?].
<!--Därutanför sitter med värdig min en lingonmamma så prydlig och fin med alla döttrarna sina.
De torkat lingon den hela dag, de skola dem koka i honungslag, men gnida dem först så de skina.-->

(sya a) lakegeba-mya i domaybey ludi e: ”suke a mikya-GULLIG, a kyu i dyu kweday e lakegeba. boysesey, pumi[?] i gwibey demi, a (baybey/hoybay) mikyamaw (hoybay) [word order?]!”<!--Och lingonmor säger vänligt så: »Ja visst, kära barn, ska’ du lingon få – så plocka nu snällt, mina flickor!
Men tag dem blott riktigt varligt ned, och akta att ej ni får karten med! Och inga kvistar och stickor!

När korgen står fylld med råge på, ta gossarna lingonflickorna små i hand och gå ut för att gunga.
I spindelväfsgunga af finaste slag nu gunga de Putte tag på tag, medan flickorna skratta och sjunga.

Då ringer det – och nu få de brådt, ty lingonmor bjuder på något godt, på honungslingon så klara; fast Putte aldrig förr smakat på så godt som detta, han kan ej förmå mer än tvänne lingon bara.-->

kikawdin för skog. fresh?
heypoke – surprised, passive.

~läsa om subject i grammar.~ markeras med a om ej kommer först. evt ngn annan korr i chatt.

> If you have multiple subjects, verbs, or objects, use the corresponding particle. “sya” (and) can be used too, but this will generally be the most unambiguous.

## syntax

adjective (noun+)noun i adverb verb e adverb adjective noun

many words can function as verbs and nouns, and even adjectives and adverbs.

## pronouns
	s	p
1 bay – moybay
2 kyu – moykyu
3 swe – moyswe

## function words

or conjunctions and others

sya – and, also, plus etc
bemyu – but, however
lodu – or, either

teso – who, that, which (relative clause marker)
dyu – shall, after (future tense)
mye – already, before (past tense)

baw – more
byu – most

kya – what, which

## nybörjarlektionerna
If pronouns are used as adjectives, before nouns, they act like possessive pronouns:

bay komya = my family

Basically anything can be compounded.

i marks verb phrase, e marks object noun phrase (and a marks subject noun phrase).

swa means "(to) be". **if it's the only verb** in the sentence, that whole verb phrase can be skipped.

bay i swa e domay = bay e domay

----
Being an auxlang nerd I know a lot of options, and it's common psychology that the more options you know, the harder it is to choose. The more you know, in this case, the more you know that could be different or maybe better. One of the strongest reasons (then) for using Dasopya is that there is a friendly community – or, a community, and a friendly one at that.

y-stavningen, och /sj kj pj/ etc.

(4 jan 25): the Dasopya phonology maybe feels less crazy now after using it for a while – uh, maybe I haven't said that. I'm kidding a bit, but it does feel a bit strange and that are things that I stumble upon (y for /j/, keeping diphtongs apart, /j/ after /p b/ and others), but I think it might have started feeling a bit normal now – indicating that it is indeed as one could suspect to one degree or another a matter of – how to put that in English? – habit/habituation/exposure, etc.

Less daunting, I guess it feels, now – after getting used to it a bit.