# Pomperipossa kon la long naze

Un tem, poli, poli kilo pre nun, es un terifik diHMM lau WITCH, ki BE nimiza Pomperipossa.
Det är inte precis något vackert namn, men ändå är det bra mycket vackrare, än vad hon själv var. Kan ni tänka er, hur hon såg ut? Hon hade två små röda ögon och en stor mun med bara tre tänder i. Och hon hade fullt med vårtor på händerna och en stor puckel på ryggen. Men det värsta var hennes näsa, för den var en hel aln lång. Ni kan tänka er, hur mycket snus, som gick åt, när hon snusade! Ett helt skålpund åt gången!

Men det hade Pomperipossa råd till, för hon var rysligt rik. Hon bodde ensam i ett litet hus i skogen, och det huset var byggt utav korv och skinkor, och i stället för tegelstenar var där stora stycken sirapsknäck. Så rik var Pomperipossa.

Men ingen enda människa vågade sig till henne och knappt något troll heller, ty hon var så förskräckligt elak. Om det kom någon till henne, förtrollade hon honom ögonblickligen till ett bord eller en kittel eller en griffel eller någonting annat. Hon var en mycket farlig trollpacka. Hennes största sorg var den, att hennes näsa blev längre och längre för varje gång hon förtrollade någon. Det var hennes straff!

Så var där också en kung i det landet, där Pomperipossa bodde, och han hade en liten prins, som hette Pipi, och en liten prinsessa, som hette Fifi. De gingo en vacker dag och promenerade i parken, ledsagade av en hovmarskalk, som var så fin, att han endast kunde svara »A!» eller »Ha!» till allt, som man sade till honom. 