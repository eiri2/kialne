**logi ja le eureka bio en la kosmo.** a pos multi nen de hem, idea e soki, men le eureka ki la javabu si dai fasil.

-men ha bio juste a yi, a geo. vi trodde aldrig att|vi hade aldrig trott att svaret skulle vara så lätt, men det finns massor med liv här på jorden. säger X Y, forskare i vetenskap och rymd.

efter denna fantastiska upptäckt, tillägger X Y.

avslutar med att är alltid på sista stället man letar.
it's an interesting observation that things always seem to be in the last place that you look.

##yebaxa

in 11 me mes 9, Suen pas ha **vote to suen di kanun feter grupe.** mimen ha vote to vo a pan un nelu me nen, e de 2010 mimen ha ceti parti in la kanun feter grupe, la _riksdag._ vo ceti parti si la leu parti V, la sosi demi krati ja S, la eko ja o luga ja MP, la medi parti C, la huru sim ja L, la huru sim-'hafiza sim' ja <!-- "moderat" o (bil di) 'hui di ja' -->M, la mesi din demi krati ja KD e la suen demi krati ja SD.

la ba parti ki pas na la maxim ba vote si la sosi demi krati ja (S), la suen demi krati ja (SD), e la "moderat" ja (M), a sum di 30,3, 20,5, e 19,1 % in la un me fa nume de vote.

ama a tema de sana de politi, la parti le forma/fete du grupe. un grupe si la rubi-luga cen di politi de S kun suporte de MP, C e V (S e MP medi-leu, C medi-raite e V leu), la alia si la ba raite parti, M e KD ki le sam rai ki yamen du vol fete politi, e SD e L (M, KD e SD raite, L medi-raite).

SD, S e MP na max da vote ka cen di, a 2,99, 2,09 e 0,68 %, e pan alo de la ceti parti xiti vote – la jong parti (C), la luga ja (MP) e la mesi din demi krati ja (KD) xiti maxim, kon 1,89, 1,29 e 0,98 %.

in rai mete a cen in yi nen

https://www.svt.se/nyheter/inrikes/nu-presenteras-svt-s-valu

parliament
moderate
ganya/jai vote – vinna röster?
preliminary
loose, fast det behövde jag inte
mainly
poll

**sol a supra mimen**

_de Dia Psalma (”Sol över oss”, in Gryningstid, 1994)_

```
*sol* foto a supra mimen su siti
tra la kung in (la) sama
_hata di ka_ sol foto no jen suka
a sabu de ya hogo la pil
```

```
e la ais in *norde*
ya yung max rapide zai den
ka ya fa cen
sun la sui si a mimen su mun
```

```
mimen ha *sol* a supra mimen su cati
mimen ha geo a nice mimen su *fute*
mimen ha hava _a ruke_ mimen su *ruke*
mimen ha la dunia _a jin_ (mimen)
sual tumen bil vide ki mimen ha pan (xe)
```

	hai si be masala kon vo auro, ki si dai kala
	e ki mimen le ha loka a vo
	la jangal e la lage, yamen mece in be suan
	a dura de/cana mimen xope barude
	
	e la seme ki tu le poze
	ya a siro sata be dai (ri)
	ka ya fa cen
	sun la dunya le be fin
	
	mimen ha sol [e max de vo]
	
	pan paci ki a cen/cen di fei a pas
	zai (zaman) be kux a geo, gati mate di
	pan pexe ki pas be boi/fa boi a cen
	pul di le daka fase de la mar
	sual yo galte di xe le be kasu/yo xe le galte di be kasu
<!--although, spite
behind
ahead
in front, front
so, that much, eo:tiom
pour, release
let (out)
kira da - rent out (let, lease)
kira - carat
go by, fly past – a pas?
stiff
gå, gå fel. turn out.
happen, occur - *be kasu*
-->

**lau man in Södertälje fa truke to gar xofer karte,** Suen su Radio loga. la man, in la ba 60 nen, pas uze foto grafi gi yen kanca, ore soni gi, e un "be mute di kamisa" to fa truke in teste de teori di sabe xe to gar xofer karte. la radio kitabu ki ya vol jai en la teste a vo polita ki ya truke. la man si be jude to pei 2500 suen di korona (yaki 235 euro).

[(sverigesradio.se, 2022-09-13)](https://sverigesradio.se/artikel/aldre-man-fuskade-sig-till-korkort-med-oronsnacka-och-spionglasogon)

<!--license, permit
spy
(eye)glasses
sweater
fines

## läsa/ämnen (eg. bara det första som tänkt som evt ämne)
moksha language/mordvin
1993 russian constitutional crisis

## källor/nyheter
UI landfakta via bibl
tidning

bbc.com
auxlangnyhetsblogg
anglish times, men svårt att förstå

newsasfacts.com, förstås
-->

**La xefe minister de Antigua e Barbuda,** Gaston Browne, loga ki men xa fa vote a tema de la dexa be si vakil krati. a zai zaman, vo si demi krati di auto krati, kon Charles III ka raja.

[(newsafacts.com, 2022-09-11)](https://newsasfacts.com/news/99271)
<!--announce, declare
be si? - become
the Commonwealth, parlament, "head of state"
-->

**Kiribati** si vakil krati nesia dexa in Oceania. ya ha max ka 119 000 be dom jen, e max ka un fen du de yamen be dom in la nesia Sude Tarawa.

Kiribati pas si fen de Britan, ama be si ya se su dexa in 1979. saide de pexe e exporte de be gan di nama de yezi frute si la max dai fen da la biznes e vende xope de la dexa. ya si un de la minim be darja mute dexa in dunia.

la dexa ha gau di risko de be asar de klima mute. a sabu de hai fase gau, dai fen de la yam xe geo bil xa si a nice la hai in 2100, e gar vo, max bil xa si no uzebil a sabu de yem de hai.<!-- a sabu de vo problem, la xefe minister ja/prezidente de la dexa cing be dom ja to cuti la dexa in 2011/2014. -->

Kiribati zai/be loka in e norde e sude de geo par lin, e a max e veste e dong de la 180 me ging go la. a sabu de vo, ya si la un di dexa ki si be loka in pan nelu geo gola fen.<!-- vulnerable
to urge – cing?
-->

**Du geo sismo** sismo norde Afghan e mata 6 jen. (2022-09-06)

[(newsasfacts.com)](https://newsasfacts.com/news/99104)

<!--
val i Kenya, tror jag, eller överklagan av resultat
firade i hemdistrikt vinnaren, stängt butiker och nedslagen stämning förloraren/oppositionen.-->

**Jongoku loga ki a minim 46 jen le be mata** a sabu de geo sismo in Sichuan topo in sude-veste de la dexa. ya geo sismo pas si 13.00 den 5 de mes 9, a fase tele de 10 km, e pas si 6,6 in meter de Richter.

la geo sismo a max sabu geo glis e avar ba baxa tul line. in _Chengdu,_ 226 km for de la geo sismo jong, jen na ba jui note a tema de la geo sismo, ama a sabu de kanun loga ki jen in _Chengdu_ mus no cuti yamen su dom, multi jen no cuti (yamen) su dom-loka e ga to dom-bani.

[(bbc.com)](https://www.bbc.com/news/world-asia-china-62764223)
<!--dare-->

pan Suen pas ha norde di sama foto in neu di ba noce. mi no pas vide ya, ama di.

* [makala e grafi a svt.se](https://www.svt.se/nyheter/lokalt/vast/norrsken-over-lysekil-under-mandagsnatten)

<!--aurora | though, however-->

## Oh boy!

_gani de Peps Persson_
<!--song from Peps Persson. (Note: a word bro-ken with hyphens marks that it consists of two words in Pandunia.)

we have beautiful wea-ther
sun shin-ing (at) to-day-->

```
oh boy!
mimen ha mei sama hal,
sol zai foto (a) zai den.
oh boy!
mimen no yau veze di fuku,
e mi fili vo.
lipa e jampe, mimen ha sol zai den
e a vo tipe de den, men no bil
dele e kux.
no, laxe vo e huke a mi to vai
dur lai la garma mosim, (e) zai zaman
fin la leng mosim.
(amir) audi la paci-gani,
ya rota (la) ruhu
```

```
oh boy!
a yi a la bagi-rute,
zai la dau to un ventur.
ye, xiu ajabu ki men bil be suka
a vo polita de ba hua (de la geo)
e (de) luga yepa.
e la baca in men lai to dom a ri
to anmemo di axa
e to garma mosim.
oh boy!
```

```
oh boy!
ke tipa de suka di ba ton,
(ya) tire e trate in mi.
oh boy!
kilo mega de ba 'hande xaka karibu'
mi vol da to tu.
ye, xiu ajabu ki ya bil si
yi polita fasil yo sata,
sam fasil ka vai move un hande
e (fa) safa xiu seku de baca-muka
e be gamo garma de (un) garma mosim hava
oh boy!
la tipa de daxe de (a) supra,
ya si (ki) jiva a zai den.
```

```
oh boy!
sam ka daxe de deu, sual
si guai ki mi be suka?
sol ye foto, e tu si a yi
e Geo be rota in ya su sama-gola
e, ver di, dura ya ki men be gamo
in yi mode
la dunia karibu di si un pari dexa.

oh boy! × 5
mimen ha mei sama hal!

oh boy! × 4
boy!
```
[(asal di version)](https://genius.com/Peps-persson-oh-boy-lyrics)

<!--
har varit nominerad till Telia best ringtone of the year 2007

what a/such a – la tipa de, ke tipa de? (so easy– yi polita fasil)
delay. to slack. dele ~ wait, delay
to vai, kan man använda det för "till ut"?
ex-/vai.
dream
dizzy
twitch, jerk, spritz (tire, trate)
hug
tänk, tänk ändå - i förundran
extend, stretch out, reach out hand - vai move un hande
cheek
pure, puerly, not clean?
glad
shine - foto?
actually
eo: farti. well-being salamta.

jag vet inte	mi no sabe
vet du?	sual tu sabe?
hen vet	ya sabe
ja, jag vet	ya, mi sabe
jag vill ha vatten	mi vol sui
jag ser att den är röd	mi vide ki ya si rubi
ser du vattnet?	sual tu vide la sui?

tjärlakrits
peter rabbit

sala silvergruva
fisk med genomskinligt huvud-->


## Monataj tagokvantoj

En la sveda ni havas utilan poemeton<!--vers--> por memori la tagojn de la monatoj (”Trettio dagar har november, april, juni och septemper – alla de övriga trettioen – februari tjugoåtta allen”), kaj mi provis fari esperantan version.

> Tri-dek tagojn  
> havas novembro,  
> aprilo, junio  
> kaj septembro  

> Ĉiuj aliaj tridek-un,  
> krom februario  
> – estas nur un

Mi pripensis pluraj variaĵojn de la lasta parto. La originalo mencias kiom da tagoj februaro havas, sed mi kredas ke oni kredeble jam memorigis tion, kaj ĉar estas nur un precipa monato (kun un precipa numero, aŭ, nu du, fakte), ne vere malfacilas (kaj ankaŭ simple ne ĉiam veras ke estas dudek-okt tagoj februare).<!-- krom dudek-okt, ktp, variaĵoj. -->

Oni ankaŭ povas memorigi ke ĉiun duan monaton havas 30 kaj ĉiun duan 31 tagojn, krom aŭgusto (kaj do septembro)<!--tamen prenita de februario, laŭ https://eo.wikisource.org/wiki/Dek_monatoj_SR_1975, kaj mi kredas ke mi aŭdis antaŭe. sed vere, ĉu? --> ĉar Aŭgustus ne volis havi pli mallongan monaton ol Julio Kajsaro/Cezaro, kaj ke februario origine estis la lasta monato de la jaro, ĝis la malfrua periodo de la imperio (kiam oni anstataŭiĝis marto kun januario kiel jarokomenco), do ĝi havas malpli. Sed tio ne diras kiujn havas kian numeron, do oni ankaŭ memoru ke komencas kun 31 tagojn (komencas kun la plej granda – fakte komencas kaj finas kun la plej granda).

## Konsiletoj
<!--
kosttillskott
hälsa/träning övrigt
just det, idéer om kost
miljötips/hushåll-->

### Zeitgeber-oj

_Zeitgeber_ estas kiu ajn afero – stimulo, mi supozas – kiu ŝanĝigas la biologikan aŭ korpan horloĝon, kiu diras al via korpo kiam vi estu laca kaj dormu, kaj kiam vi estu viglia (aŭ ”vigluma”) kaj vekigu. Eble oni povas nomi ĝin **tempodonaĵojn** aŭ **tempoindikaĵojn** esperante. Jen eta lista de tiuj ĉefaj kiuj mi konas, kaj kiom ili efikas por mi. Avertu ke ne ĉiuj aferoj en la listo estas veraj tempoindikaĵoj, mi iom intermiksis kun aferoj kiuj helpas dormado

* **Varmo aŭ nur varmeto vekiĝas – efikas.** Mi ja iĝas laca kaj dormema de varmo ankaŭ (sed endormo malfaciliĝas tamen multe de varmo), kaj se estas sufiĉe varma, tio helpas min redormi (tro). Sed en dormoĉambro eble 16°/17°–19° celsiusoj, kiu nur ete varmiĝas, tio ja vekiĝas min.

  Oni ofte parolas pri ke lumo vekigas, kaj jes, ŝajne ĝi malfruigas mian korpan vesperon, sed luman vekiĝon ne ŝajnas fruigi mian matenon (kaj sentas multe pli malmilda/<!--malmola/aspra/bruta/raŭka/ronka/-->severa kaj streĉanta ol vekiĝo de varmo). Varmo donas iom trankvila kaj mola vekiĝon, kaj tiel ŝajnas iom fruigi/influi mian korpan matenon. Laŭ usonan neŭrologiisto, homoj vivante kiel la prahomoj en la Savano **ne vekiĝas de la lumo,** sed de la varmiganta aero en la frua/komenca mateno.

* **Malvarmo helpas dormiĝon kaj dormon – efikas.** Mi dormiĝas kaj dormas pli bone se mi estas malvarma aŭ ne varma. Oni ofte diras ke oni dormas multe pli bone en malvarma dormoĉambr, kaj mi vidis aserton ke<!--WIL--> malvarmo esta la plej efika maniero por fruigi la endormado de malendormemuloj.

* **Stimulo (informo, sono, parolo) – iom efikas** – mi ne ĝuste scias, mi supozas ne estas pura tempodonaĵo, sed estas streĉiga kaj aktiviga, tiel kontraŭ dormemo kaj ekdormado, kaj tiel malfruigas minimume tiun tagon/vesperon. Mi ne scias ĉu ĝi vere ŝanĝigas la korpa tagon, tio estas (se ĝi) influas vian ritmon dum plurajn tagojn (fruigas se en la mateno kaj malfruigas se en la vespero).

* **Vidi vizaĝon (interparoli, sociumi) – eble efikas** – laŭmemore, vidi vizaĝon en la mateno fruigas korpan tagon. Ĝi ankaŭ estas uzata en kurado de socia fobio (minimume tiam simple bildoj de vizaĝoj sufiĉas, 30 sekundoj aŭ io je ĉiu bildo, en la mateno). Mi ne scias ĉu vere fruigas mian matenon, tamen vespere mi kredas ke ĝi eble vekigas min (ankaŭ nur telefona parolado).

* **Lumo – iom efikas** – vespere ĝi ja kredeble malfruigas la korpovespero, kaj ja certe vekigas min kaj malfaciligas endormadon, sed mi ne certas kiom da efikon mi havis je fruigi mian korpan matenon. Artefarita lumo en la mateno, kaj iom eĉ natura, tamen facile streĉigas min.

* **Manĝado, konsumo de kalorioj – malcerta, iom efikas** – mi malcertas pri fruigado de mateno, evito iom helpas endormado. Ĉefe pro aliaj kialoj, mi evitas manĝi ĉirkaŭ 4 horoj antaŭ enlitigo. Teorie, frua kaloria manĝo male, iom da sukero (en la hipato, laŭ MJ) aŭ karbonhidratoj eble helpas endormigo<!--enl. flera källor, blogg tipsade om för längesedan med, el liknande sida-->. Laŭ aserto,<!--WIL--> karbohidratoj kaj la kresko de insulkinokvanto (mi kredas) ĝenerale farigas pli laceco (do laŭ tio oni evitu ilin iom matene; tamen ankaŭ ne bonas manĝi tro da kalorioj vespere).

* **Fizika aktiveco** – estis iu multe kontraŭintuitiva kun la aserto pri ĉi tio, kiel frua aktiveco malfruigas (!) la korpa vespero, aŭ iu tiel. Aktiveco kredeble ja vekigas vin momente, sed mi malcertas pri la daŭra efiko. Pro bonan dormon, minimume, oni ja ne povas esti aktiva en la vespero (prefereble sufiĉe trankvila aktiveco ankaŭ kelkaj horoj antaŭ enlitigo) – tio havas influo de mia dormo – kaj ekzerco de forto estas menciita kiel pli bone por la dormo (ol rapideca ekzerco).

(Jes, estus bona se mi enmetus fontojn. Ni vidu, mi eble povos aldoni kelkajn, kaj la baza informo estas de sveda Vikipedio – artikolo pri.. diurnal-eco, ĉu? Mi kredas ke oni povas serĉi tien por _zeitgeber,_ ĉiuokaze.)

## Svedoj kaj obedo

> _Laŭmemora pri la asertoj_

Rilate al alia diskuto, mi pripensis la temon revolucioj (ke mi ne estas multe revoluciema, mi preferas reformojn se eblas ŝanĝi aferojn tiel, ĉar revulcioj kaj popollevadoj/popolribeloj ofte estas violenciaj, kiu kaŭzas suferadon. Ankaŭ, se aperiĝas rolo de regpovo kaj forto, ŝajnas ke ege ofte tiu rolo estas plenumita de la plej psikopata persono.<!--Precipe se temas pri violenciaj revolucioj.--> Tamen, ĝi kompreneble ankaŭ povas esti legitima en (multe) opresa societo, ekz-e en kiu homaj rajtoj estas malrespektita, la homoj ne rajtas multe de la ŝtato, kun manko de libera esprimado de sentoj, pensoj kaj opinioj, kaj manko da demokratio kaj ofte aŭtotiratisma regado), kaj tiu gvidis min al alia temo.

Svedio ne havis multaj sukcesplenaj violencaj revoluciojn (kiu eble formigas mian perspektivon). Nu, ni – aŭ oni – ja havis multaj popollevadoj (en la formo de _bondeuppror_) inter minimume la mezepoko (1066-1521 en Svedio) kaj la 1700-aj jarojn, sed ili aŭ estis forte subpremita de ekz-e Gustavo Vasa (dum periodo konsiderita la "landopatro" de moderna Svedio, regis multajn dekjarojn en la 1500-uma jarcento), aŭ ne estis tiom granda – mi **kredas.** Ni fakte havas ekzemplon de revolucieto en kiu oni reinstalis la reĝo kiel regposedanto (dum la libera epoko, _Frihetstiden,_ komencita en la 1720-umaj jaroj, laŭmemore, Svedio estis regita de la parlamento, kaj la reĝoj kaj reĝinoj estis pmp nur simbola (antaŭ la franca revlucio, notu bone). Ĝi finiĝis kiam la hered~et~a reĝo Gustavo la 3-a instigis ribelon kaj prenis la regpovon al si mem.)

Oni ofte parolas pri kiel ni ofte obeas kaj obeis la registaron, kaj ŝajnas esti minimume iom vera. Kaj ĝi funkciis por ni kaj bone kaj malbone. Dum la 30-jara milito, la pastroj de la sola, ŝtata kaj deviga eklezio predikis la mesaĝojn kaj komandojn de la registaro, kaj la popolo obeis kaj suferis. Oni devis militi. Laŭmemore, 4 de 5 viroj en la tiamaj svedaj generacioj mortis en la milito (ĉefe de malsanoj, kiel ĉiam en militoj en tiu tempo) (aldoni al tiu, ke ne malmultaj virinoj ankaŭ estis bezonata en la armeo kaj akompanis la milituloj).

Pensante pri ĝin, kialo povas esti ke la reĝimoj de Gustav Vasa, de Karlo la 9-a kun liaj Moseaj leĝoj kaj ĝenerale dum la "grandpovaĵa" epoko (stormaktstiden), la lando estis sufiĉe opresa kaj strikta (notu ankaŭ la rolo de la eklezio supre). Ne ekzistis spacon por defii la ŝtato.

Kaj ankaŭ en la 20-a jarcento mi pensas ke oni povas diri ke oni ĉefe estis obeanta, krom en la komenco, precipe rilate al la baraktado/lukto de la laboristaj ligoj. Kaj la ŝtato ĝenerale (sed ne ĉiam!) faris aferojn kiu bonfaris/estis advantaĝa por/ la bonfareco de la popolon.
<!--
wa li yu (eo:ri), ya (it, thing)

wanyo linyo yunyo yanyo

al - eo:oni
noyom - each other
walo, walonyo - reflexiva pron, även sig, hen själv

eo, nong
esto - but

mai
yau - excuse me
pai li - thank you
nen - good, fine etc
nenen - okay, alright

yunyo jo - they know
wa keju - I want
wa jam - I do
wa weyun - I see
wa tengi - I can
li kiza - you eat
yu nenju - he likes
wanyo de - we come
linyo wera - you sit
yunyo de - they go/come

hea - hey! hey there!
shau shai - "fine", "so-so" (literally: "like this like that")
mau - also, too
tatau - recently, lately
reshi - sure, of course
wehim - to hear
nenju wehim - glad to hear that (lit: "I like to hear that")
soi - please, "I politely request"
kamai - to greet, extend one's greetings to (through another person)

Antika nu-evento-note u antika evento e kosa. Ave o mebi ero.

## Tristan da Cunha

_Tristan da Cunha kon Edinburg de seven mare en dereka._

Tristan da Cunha, u o medi solo Tristan, a da ma solo de-i domo ila-junta en mundo. Kon ota name, da e da ma fara de ota loke kon man.

Da e loke /de-i/ ma sama 2400 km de ila Saint Helena, ma sama 2450 km de ila-junta Falkland an ma sama 2750 km de litora de Cape Town en South Africa.

Da ma mega ila en junta e an de-i name Tristan da Cunha, an en da a Edinburg de seven mare, ke a solo siti en ila-junta. Da site i ave rondo 250 domo-man. Pero veta-gare en ila Gough, Tristan da Cunha a solo ila en da ila-junta kon domo-man.

wire fraud, om optisk telegraf

pjusken, örn som fotobonden fotat, konstant mobbad och i dåligt skick. en vinter kom en björn och härjade, från okt/nov? till vet inte när, då följde den björnen och åt av dess byten hela tiden. nu har den fru och barn.


## naotext tristan
vad ska ”ja” i lajamafua vara?
över 2400 och långt över 2700
kallas bara city på ön, missvisande tvetydighet-->

## Tri manieroj ŝpari energio

Mi pardonpetas por la klak-fiŝanteca titolo. Lastatempe, sentis iom kiel mi havas talento pri konstrui tiajn. Jen tri malsamaj manieroj por ŝparienergio<!--Energi-sparantaj etaj aferoj

alt. Kiel spari energio-något. Ĉu ne la titolo sonas kiel clickbait? Mi pardonpetas.

eller kiel vi povas spari energion, nej, tiel.-->

### Aĉeti malpli da vestaĵojn

>_Ĉi tie mi intencas aldoni la fonto – artikolo je esperanta retradio._

Se vi aĉetas eble 0-5 vestaĵojn po jaro, tio estas malgranda takso/elspenco/elspendo de energio. Mi laciĝas je aĉeti aferojn, kaj interalie pro tio mi aĉetas eble 2-5 vestaĵojn po jaro, plej ofte [undergarments].

## Uzi malpli da programoj

Ofte je la komputilo, kaj kredeble je poŝtelefonojn (mi ne ofte uzas ĝin pro la malprivateco de Android kaj ŝajne ankaŭ de Ios), kelkaj programoj kaj ĉefe programetoj ruliĝas kiuj oni ne bezonas kaj neniam uzas. Ekzempleto estas bludent-programetoj, kiu pretas ricevi bludent-ilo, por mi kiu ĝis nun neniam uzis bluetooth-ilon (nun mi fakte uzis un fojo post komenci la artikolon – eblas iam antaŭe ankaŭ, sed mi ne memoras iun precipan), aŭ nur labortabla ĉirkaŭaĵo¹ (LĈ) kiu uzas sufiĉe multe da risurcojn kaj enhavas plurajn programetojn. Imaĝu ke tiuj superfluaj programetoj estas ŝaltitaj/ruliĝas en /miljardoj/ de /devices/. Mi nur pensas ke tio devas esti multe da energio (ĉiu laboro farita de iu programo okazas tra elektro en la ĉefprocezilo,² kaj ankaŭ la varmon kiu la ĉefprozesilo do faras la ventumilo devas forigi) (tamen, se vi ekz-e en vintro bezonas la varmon en la kameron, tio ne multe gravas. La varmo de la CPU do nur malpliigas la kvanto de varmo kiu la kamerovarmigilo devas fari. Restas la elspeco por la ventumilo. La sama veras en vintro, svedie, pri lum-ampoloj, miakomprene). Avertu do, ankaŭ, ke tio nur estas mia propra ideo.

Mi pensas ke kredeble veras ke ankaŭ Linukso kun LĈ-o (kiel Gnome, KDE kaj la malpli granda XFCE) elspecas malpli ol kutima "Ŭindoŭz" Windows instalaĵo. Persone, mi nur uzas fenestra administrilo,³ kiu normale ne startiĝas programetojn, kaj ankaŭ ofte estas pli malgrandaj en si mem.

¹ angle _desktop environment, DE,_ svede _skrivbordsmiljö_
² _central processing unit, CPU – processor_
³ _window manager, WM – fönsterhanterare_

## Manĝu nebovaĵa viando
Ĉiuj viandaĵoj elĉerpas multe da energio, sed bovaĵoj elĉerpas precipe multe. Fiŝaĵoj kaj birdaĵoj (tamen birdoj ĝenerale havas multe malbonajn vivkondiciojn) precipe elspecas multe multe malpli ol bovaĵoj kaj porkaĵoj, sed porkaĵoj ankaŭ elĉerpas multe malpli ol bovaĵoj.

Mi ne scias ĉu oni kalkulis ke oni ankaŭ produktas lakto kun la bovoj, kiu kompreneble dependas de ĉu oni fakte uzas lakto de bovoj kiu oni havas por produkti viando. Mi ne scias kaj mi ne certas. (Post kontrolado, ĝi ŝajnas ke kelkaj rasoj de bovo oni nur uzas por viando kaj aliaj por lakto, kaj kelkaj por ambaŭ, laŭ _Svenskt kött._)

Kompreneble, se vi entute ne manĝas viandon, tio elĉerpas eĉ multe malpli energio.

> _Mi ĉi tie ial eble iom fuŝe kunfandiĝis energi-elspeco kaj ellaso de karbondioksido, mi kredas. Mi iam pripensu._

<!--## Vori de senjore Hubbard

A eve de monato 11 en London. Jalan e de-i kova en da ma dika nebu. Tingi-senjore Hubbard, ke i viva en Holland Park Avenue 104, i pone en si kama an i leji. Si-viro a rondo penta ten ano, an i ave a pende ruja-maron labi-boroda. Jusa, si e unda en kosina an i toma go ego a kopa kon fogo o bon-aroma tea.

A dia 3, an ka da ale Joana an Beti e labora-libera. Si e solo en kasa kon si du sibi-kinde Mari an Dick. Ale e silen an pase. Tingi-senjore i jira a foli de buku – jusa de-i pubili ofensa-saga ke i name "Mori en tea-pote". Si i bibe a mini de tea an i pone a si pedi en favo-kan kama-kalori-uti.

Deja, si i ia a sono en koredora. Si i unda a buku an i ia. Nun si i ia gen a some kosa. Senjore Hubardo i pensa ke i debe a Mari u Dick ke e eki de kama. Si i memo ke di-dia, si-ale i de manja a seko-feruta kon nata sama afa-manja. Fule natura, si i pensa, pero tamen mi i debe mira a si-ale e ke.

Si i kova a ego kon si eve-roba, i toma a jage-lampa de eve-mesa, /an/ i kite go a koredora. Luse e de-i para, an nun no sono i kan e de-i ia. Si i en a kamera de kinde. Si-ale i pone an i resi kon fasa-sono, an da i maka ke si pele i dona a liki mui.

Raro, si i pensa an i en gen a koredore. Si i begin a fogo en lampa, an i voka via a lada-fense:
–Salu, ani e da, u ke?

A no reaki. Bon, ja a natura rason, si i pensa.

En sige dia, si i sede en si buku-kamera an i labora a no i-de paga biletas.

En uno tempo, e fo mini kuneli, an si-ale name e Flopsi, Mopsi (skrev Topsi Flopsi först), Coton-Teil an Piter. Si-ale i viva kon si-ale feme-paren en farina-kolina unda radika [rädisa!] de mui mega igi.

' NOW, my dears,' said old Mrs. Rabbit one morning, 'you may go into the ﬁelds or down the lane, but don't go into Mr. McGregor's garden: your Father had an accident there; he was put in a pie by Mrs. McGregor.'

## makiu niu malia (haga)lau
### missing words

list, system -->
## jumukio ban de 'birger 'erik din 'valdemar

[![daganeigos 'biryer 'magnuson](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/Birger_of_Sweden_%281280%29_c_1322.jpg/182px-Birger_of_Sweden_%281280%29_c_1322.jpg "daganeigos biryer magnuson")](https://commons.wikimedia.org/wiki/File:Birger_of_Sweden_(1280)_c_1322.jpg)

### dofafai 'hotuna

1306 pa gos[stokholm] – 'biryer 'magnuson, _Birger Magnusson_ pa lengos[S], bio jin 26 nen, din muta niki nei gos[s] pobio daganeigos tile 1290 – keu no gos[S] sou – jumukio janmeilin pus de biryer, 'erik din 'valdemar nei pa soi de gosta, din 'erik tua nebei gos de se – jumukio niki ban jin lanei de gos'S ~ 'valdemar nepusjanli 'erik, din danei pus de gosta, _Torgils Knutsson,_ niki nepusjanli 'biryer daganeigos – hic, 'biryer niki fosim pa ladil ko jumukio muta, din nejantallunemeu 'torgils dofosim ko ladil ta le? ledi 1306 –

bio sinienef ledu, din tolus janbeilun ba pe 'erik din 'valdemar gobepa tu 'biryer pa hudom hotuna ti ladofalibefafaidisem – tolus ta ba pe 'biryer ne lamitbepufafai tu jumukio muta, keu lesim jumuta de jumukio nejanmodi juse din kaban judata – le dilelau, judata nesejo daganeigos 'biryer din misem muta –

ji de danepusfei 'biryer sel nesejo din nedonoso mupai 'biryer, din mupai ki dogagokoten tu gos[D] – 'biryer ki dogagokoten tu 'niceping hudompun din sit pa madakem/sitmadakem/pamadakem pa topata pole dua nen – pos nejo dua miftui de gos tu jumukio muta, muta ki dinodokei din du pamakadem –

lodofa ta kuc bio dobabio dofafai 'hatuna – tagau doba sol pa tolus ji, tolus pe din bio dolus pua lanefopuf din lanepusjanli tu 'erik din 'valdemar – huc donomo bio dopufo jin lo gepun, hasko nel lo pe bio dodigadosodei pa san tou –
<!--
to | *tō, *ta (“to”)
and | *andi, *anþi, from Proto-Indo-European *h₂énti (“facing opposite, near, in front of, before”)
the | *sa - from Old English þē m (“the, that”, demonstrative pronoun) | Nominative *sa *þai *sō *þôz *þat *þō
know | *knēaną (“to know”) | 1p pres *knēō

till | *tila- (“goal”) | *tilą - (n) - A planned point in time.
och | *auk (“also”)
and | *anadz
vet | *witaną, ultimately from Proto-Indo-European *weyd- (“see”) | 1p pres *wait

*witą n | knowledge - reason; sense; understanding - wit [vett]

thu sehwoo sa fuglaz
*Þu *sehwō *sa *fuglaz (ack *fuglą)

thu *garwijō *fōdô|*matiz|*etaną

bird | Old English bridd (“chick, baby bird”), of uncertain origin and relation; but its stock root is possibly onomatopoeic.

fowl | from Proto-Germanic *fuglaz, dissimilated variant of *fluglaz

make | Proto-West Germanic *makōn (“to make, build, work”), from Proto-Indo-European *meh₂ǵ- (“to knead, mix, make”) | 1p sing ind *makō

koka | from Proto-West Germanic *kokōn | *kokō (ej germanskt urspr!)

skappjan, via makoon | From Proto-Germanic *skapjaną.  | *skapjō - to make, to create - to shape

PWG skap, shape | From Proto-Germanic *skapą. | From Proto-Germanic *skapjaną. - *skapą

laga | From Old Swedish lagha (“bring to order; form into teams”)

göra | *garwijaną en:yare släkt (ready, prepared, eager etc), och garva, via medellågsaxiska | garwi|jō

food | *fōdô | *fōdô

mat | *matiz

Essen | *etaną | *etō (mkt varierande, PWG har *etan - *etu) (dessa båda dock verb, svjv)

drink: from Proto-Germanic *drinkaną
year: from Proto-Germanic *jērą (“year”)	from Proto-Indo-European *yóh₁r̥ (“year, spring”)

person: In this sense, displaced native man, which came to mean primarily \"adult male\" in Middle English; see Old English mann.
man: from Proto-Germanic *mann- m (“human being, man”)
woman: From Middle English woman, from earlier wifman. The Middle English forms are from Old English wīfmann (“woman”, literally “female person”), a compound of wīf (“woman”, whence English wife) + mann (“person”, whence English man). 

*abraz: strong, severe, exceeding, immense, great, huge, terrible
*mann- m: human, human being, person, individual | man
*afteraz (comparative *aftrōzô, superlative *aftrōstaz): back, behind
wif: From Proto-Germanic *wībą, of uncertain origin. Cognate with Old Frisian wīf, Old Saxon wīf, Old Dutch wīf, Old High German wīb, Old Norse víf. | woman, wife
wǣpnedmann: wǣpned (“male,” derived from the noun wǣpn “weapon or penis”) + mann (“person”): man, male
wǣpned: male, (substantive) a male or a man
*magaþs: maiden, girl | virgin | Synonyms: *gurwī, *mawilā
*maguz: boy
*mawī: girl

## Svenska dutton
Jag har gjort flera experiment med att försöka göra en svensk version av Dutton speedwords (och liknande scheman). En vän till mig uppfann en liknande metod, och man kan säga att den här metoden också baserar sig delvis på hens metod (som i sig är snillrik, men som jag inte kan ta cred för).

Det finns ett par anpassningar av/scheman inspirerade av Dutton. Man vinner ett på ett eller annat sätt mer optimerat system, särskilt om man inte använder schemat på engelska, men man förlorar förstås all potential till internationell kommunikation som ingår som tanke i Dutton. (Att använda Dutton för att skriva merparten/själva ryggraden av ord i en text internationellt skulle säkert gå bra, men att använda Dutton för alla ord låter svårt att lära sig, likaså att tala Dutton, också eftersom uttalssystemet inte är helt välgjort och inte fullt dokumenterat. Om någon gav sig den på att kanske finslipa och fullända det, eller bara säta det i användning, så hade det nog kunnat komma långt)

j	jag
tr	tro, tror, trodde
n	nej, inte
a	att
d	den, det
e, r, ä	är
B	bra

nu först
--------

j n a d
(tr eller tkr senare, tr "tkr" först nu kanske, tänkte bara på en av dem)

tr	tro, tror, trodde
tre	trodde
z	så?
s	som
m	med
b	men
l, el, lr eller
D	de, dem
o	och
T, tk	tack
P?, p, å	på
u, U	du


fler
----
tkr	tycker
i	i
k	kan
x?, ks	kanske
h	ha, har, hade
he	hade
H	hen, hon, han
He	hen
Ho	hon
Ha	han
e	en, ett
f	för
w	vi
M	man
oz	också
O	om, tänkte kanske tidigare med

ks, x kanske förstås
g	göra
G	gå?
gnm genom

mkt mycket
sen sedan

ngn, nån någon

urspr	ursprungligen
pG, PG	pågående
komb	kombination
pol	politisk
mkt	mycket
enl	enligt
enlh	enlighet
sept	september
>	mer
>sen	senare
därm	därmed? där borde ha något också, typ.

senare–tidigare
där~här
där, därmed

### 1~50
i	i		a	att
och	o		b	men
att	a		c	- (se??)
det, den	d		d	den, det
som	s		e	en, ett
en, ett	e		f	för
på	p, P, å		g	göra?
är	r?		h	ha
av			i	i
för	f		j	jag
med	m		k	kan
till	t		l	eller
			m	med
har, hade	h, (he)		n	nej, inte
de	D		o	och
inte, (nej)	n		p	på? kan?
om	O		q	- (om??)
			r	är?
han, hon, (hen)	H (Ha, Ho, He)		s	som
men	b		t	till
var, vara?	v, re?, äe		u	du
jag	j		v	vara, är?
sig	sg		w	vi
från	fr		x	kanske?, ska?
vi	w		y	(kommer??)
så	z		z	så
kan	k		å	å, på
man	M		ä	är?
när	N?		ö	ö
år	Å?
säger	S?, sä, säe?

under	un, und
också	oz
efter	eft
eller	l, el, lr
nu
sin
där
vid	v
mot
ska, skulle	sk, x?

kommer	km, kmr, k?
ut
får
finns


alla-->

<!-- ## kius de ma 'lord 'haue

'bals 'piramid bio makiuniumalia pa gos'austral - ta bio fomnutcil cun fomsekli ko lamei gepin - le 1964 juda kua pa ma ta, din so bukofdou de nis ke datekbesakohom ki fo meugau tile 1920 - juda puslem kua pa ma ta kuc lodofa ta, keu juda so sol bukofdou de nis, no nis mei -

le 2001, datekbesakohom fo pe domo tu nis ta mei pa ma ta, din puslem kua ma ta - kuc kua le lelai noko so nis, juda sel so nis mei le kuc le lelau -

ke nis judata ki so? - nis ta dobanom "kiusbubansek malia def", keu nui "niston de makiuniumalia 'lord 'haue" - ta bio nis pupun de tucu tu 20 piduamet din 25 pitiukil - nis ta puski meu pa makiuniumalia 'lord 'haue, keu meugau pa ma ta le 1920 hoi mauspun gobepa ma ta din gin nis ta -

nis ta peu pa def, dun nopisti nepa gof juta pa def - da ki nesejo fos nis ti 'bals 'piramid, din sen bei putui gepun pa manalijinkius gepun pa sumegau -

* [pa len'eng](https://en.wikipedia.org/wiki/Dryococelus_australis)

\#nao
-->
[!['bals 'piramid](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Balls_Pyramid_near_Lord_Howe_Island.jpg/640px-Balls_Pyramid_near_Lord_Howe_Island.jpg "'bals 'piramid")](https://en.wikipedia.org/wiki/File:Balls_Pyramid_near_Lord_Howe_Island.jpg)

## kius de ma 'lord 'haue

'bals 'piramid bio makiuniumalia pa gos'austral - ta bio fomnutcil cun fomsekli ko lamei gepin - le 1964 juda kua pa ma ta, din so bukofdou de nis ke datekbesakohom ki fo meugau tile 1920 - juda puslem kua pa ma ta kuc lodofa ta, keu juda so sol bukofdou de nis, no nis mei -

le 2001, datekbesakohom fo pe domo tu nis ta mei pa ma ta, din puslem kua ma ta - kuc kua le lelai noko so nis, juda sel so nis mei le kuc le lelau -

[!['bals 'piramid](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Lord_Howe_Island_stick_insect_Dryococelus_australis_10June2011_PalmNursery.jpg/320px-Lord_Howe_Island_stick_insect_Dryococelus_australis_10June2011_PalmNursery.jpg "nistun ma 'lord 'haue")](https://en.wikipedia.org/wiki/File:Lord_Howe_Island_stick_insect_Dryococelus_australis_10June2011_PalmNursery.jpg)

ke nis judata ki so? - nis ta dobanom "kiusbubansek malia def", keu nui "niston de makiuniumalia 'lord 'haue" - ta bio nis pupun de tucu tu 20 piduamet din 25 pitiukil - nis ta puski meu pa makiuniumalia 'lord 'haue, keu meugau pa ma ta le 1920 hoi mauspun gobepa ma ta din gin nis ta -

nis ta peu pa def, dun nopisti nepa gof juta pa def - da ki nesejo fos nis ti 'bals 'piramid, din sen bei putui gepun pa manalijinkius gepun pa sumegau -

* [pa len'eng](https://en.wikipedia.org/wiki/Dryococelus_australis)

\#nao
<!-- Mi bone sociumas kun uloj kiuj ŝatas priparoli teoriaj kiu ajn-aj aferoj (iom vertikale kaj iom horizonte, en la strukturo de la temo-fadenoj). Mi ne bone sociumas kun uloj kiuj ŝatas rapide salti inter variaj temoj.-->

## Infinita numero de simeoj dum duonjaro

Eble mi ne bone memoras ĝin, sed ĉirkaŭ du jardekoj antaŭ nun, oni foje aŭdis la diraĵo ke se infinita numero de simeoj havus infinitan tempon (kaj infinita numero da skribmaŝinojn), ili skribus la kompleta verkaro de Shakespeare (ne esperante konata kiel Tremlanco). Do, eble mi mismemoras la diraĵo, sed mi pensas ke ili ne bezonus tiom multe da tempo. Ĉar ili estas infinitnumeraj simeoj, ili nur bezonus la tempon kiu estas bezonata por skribi la verkaro.

Vi kompreneble jam komprenas, sed ĉar mi ŝatas skribi, mi diru ke multaj simeoj ne tuŝus la skribmaŝinojn, multaj aliaj tuŝus sed ne skribus, aliaj sociumus, aliaj inventus komputiloj, ktp, sed ĉar ili estas infinitaj, infinita numero (ĉu ne?) ankaŭ skribus. Kelkaj skribus, kelkaj de ili tuj kaj ofte skribus (mi supozas ke ili ankaŭ devas iam manĝi, dormi, moviĝi, fejsbuki, ktp), kelkaj de ili skribus verkoj de Shakespeare, kaj kelkaj en la verkoj en ĝusta ordo. Fakte, oni havus infinitan numeron (ĉu ne?) de *plibonigitaj* versioj de la verkaro de Shakespeare (kaj ekzaktajn kopiojn de ĉiu manlibro de teknikaĵoj, ankaŭ kaj la kun-eraraj, kaj plibonigitaj, kaj ankaŭ ĉio ajn kiu la homaro iam skribis aŭ skribos, kaj ne skribis kaj ne skribos). Do, sufiĉas kun la tempo kiu la plej rapidaj simeoj bezonas por skribi la Shakespear-an verkaron.

Infiniteco estas afero kiu homoj devas bone kompreni, ĉar ni inventis la koncepton (kiel ĉiujn konceptojn).

## Stultaĵoj pri balalajkoj
Mi hodiaŭ aŭdis pri sveda balalajko-grupo kiu volis partopreni en konserton por <!--helpi kaj -->subteni la ukrainian popolon. Tamen, pro la kialo ke ŝajne multe nervozaj homoj asociis la balalajkojn kun Rusujo, la koncerto estis nuligita. Tiuj homoj ŝajne preferas ke la homoj en Ukrainujo estu sen la subteno, ol ke ili mem devas suferi la terurega afero esti pensigita pri la ekzistenco de Rusujo. Kompreneble pli gravas protekti la sentojn de ret-uzantoj ĉi tie, ol homojn en lando kiu suferas ege brutalan invadon kaj mortigadon de la popolo. Ege stulta, se vi demandas min.

* [artikolo je SVT.se](https://www.svt.se/kultur/balalajka-sodra-bergens-balalaikor-balalaika-orkester-ryssland-ukraina-ryskt-instrument-installd-konsert)

Nu, notu ke mi ne scias kiel la konserto subtenus la homoj en Ukrainujo. La grupo ŝajne ludas kaj svedajn, ukrainujajn kaj rusajn muzikaĵojn.

Aldonaĵo: mi kompreneble ne subtenas la bestian, teruregan kaj aĉegan invadon de Rusujo de Ukraino, kaj la rusujan militon kontraŭ Ukraino — mi ja kondamnas ĝin, kaj mi urĝas Rusujo tuj ĉesi kun la milito, la invado, kaj la "precipaj militaraj agoj"<!-- (se ĉi tio ne estas milito, ŝajnas ke milito eble estus preferebla)-->. Mi ankaŭ urĝas la rusa registraro ekpensi iun manieron komenci vojon al demokrata societo, resigni, kaj iri kaj raporti sin mem al la taŭgaj estraroj pri krimoj kontraŭ la homaro.

Por citi iun rusuja registarkritikulo: "mi restos en la flanko de la bona, kaj nomas faŝismon faŝismon, militon militon, persekutojn persekutojn, kaj Putin diktatoron."

Ankaŭ, antaŭ tion ke vi aĉetas vian venontan poŝtelefonon, memoru ke Ĉinujo estas alia lando kiu ridas pri homaj rajtoj, kiu ekde la vintro havas kontrakton kun Rusujo pri "senlima amikeco".

## Memma<!--https://upload.wikimedia.org/wikipedia/commons/e/e1/M%C3%A4mmi-2.jpg-->
estas finnlanda deserto ĉefe konsistanta de<!--/enhavanta/farigita kun--> sekalo kaj maltigita sekalo, kiu oni manĝas ĉirkaŭ Pasko. Kaj ĝi ja bongustas! Mi vere ŝatas ĝin, tamen mi ne manĝis ĝin multajn fojojn. Ĝi fakte memoriĝas onin (!) pri la sveda deserto kladdkaka, eĉ la gusto, sed ne minimume la konsistenco.

Ĉu ni nomu ĝin *memeo* esperante? 🙂 Nu, la finna nomo evidente estas mämmi (proksimume memmi kun longa m meze, mi supposas), kaj memma la (finnland)sveda nomo. Ĉu eble *memio?*


## Kapitalismo
**kapitalismo ·** politika sistemo, en kiu la <!--proprietuloj-->propraĵuloj de la produkto-povoj konstante provas konvinki la civitanojn aĉeti aĵojn kaj agojn.<!--  a220406 b220410 #afprismoj-->

<!-- Älsborgs lösen -->
<!-- ## Spraxkya -->
<!-- Unue, mi mismemoris kiu skribis pri la antaŭe menciita fervoja vojaĝo, kaj kredis ke ĝi estis Erik Eriksson? en Spraxkya. Ne estis. Tamen, Spraxkya ankaŭ estas ete interesa. Nu, unue, Eriksson en Spraxkya kiel Johansson i Mörkhult sekvas la sama modelo por distingi malsamaj parlamentanoj tra aldoni la loknomo kiam oni priparolas ilin.

Kaj la nomo Spraxkya estas elparolita, laŭ svwp, simile al inter Spraĥa kaj Spraŝa - ankaŭ depende de via dialekto - kun la sveda [sje-sono](), kaj kredeble estas la nura sveda vorto en kiu la sje-sono estas literumita kun ”xky”.

\#literumado #lingvo #sveda -->
<!--## Lindblad kaj la anakondo-->
<!--## Jan Ersa kaj Per Persa-->

## Vojaĝo de parlamentanoj en 1921

*Några minnen och erinringar från riksdagsmännens resa genom norra Sverige år 1921* — ”Kelkaj memoroj kaj memoreroj de la parlamentana vojaĝo tra norda Svedio jaro 1921” — (libere tradukita) estas libro de la sveda parlamentano, juĝo kaj bienisto Carl Johan Johansson, aŭ [Johansson en Mörkhult](https://sv.wikipedia.org/wiki/Carl_Johan_Jonsson), de 1922, kiu poste iĝis konata por la maniero en kiu ĝi estis skribita. Jen la eka frazo:

> La vojaĝo komencis en mardo la 12a julio je 10:10 vespere, kun de la registaro aprobita kaj de la Reĝa Fervojestraro akirita plia trajno, de Stokholmo tra Uppland, kies belega naturo kaj grandaj kaj ebenaj agroj, kiuj plue ni-scie estas bone prizorgita, ni kaŭze de la ekado de la nokto ne vidiĝis.

Ete libere tradukita. Jen la sveda:

> Resan anträddes tisdagen den 12 juli kl 10:10 e.m., med av regeringen medgivet och av Kungl. Järnvägsstyrelsen anordnat extratåg, från Stockholm genom Uppland, vars härliga natur och stora samt jämna åkertegar, som desslikes också torde vara väl skötta, vi tillfölje nattens inträde ej fingo skåda. 

(de [pekoral je svwp](https://sv.wikipedia.org/wiki/Pekoral))

\#anekdotoj #Svedio #kuriozaĵoj
<!-- [ne de] Erik Eriksson? en Spraxkya-->
<!--## Numminen kantas Wittgenstein--><!--

[M. A. Numminen](https://eo.wikipedia.org/wiki/M._A._Numminen) estas finnlanda kantisto kiu faras kantojn en la finna (mmi kredas, ankaŭ), la sveda kaj la germana. Li popularas kaj en Finnlando kaj en Svedio, minimume, kaj ~~laŭdire li havis esperanto en lia infana hejmo (GEA), aŭ ~~ laŭ eowp fakte lernis esperanton en la lernejo, kiu tiam instruis esperanto (kaj ke li partoprenis je la 54a UK kiel muzikisto)~~, li ?? ~~. La akordionisto Pedro Hietainen ofte akompanjas Numminen iom kiel lia bando, [ŝajne aliaj laŭ wp ankaŭ, tamen pedro nura menciita en sveda] kaj Numminen estas konata iom por sia unika stilo de kantado.

Inter alie, li faris albumon kie li kantas tekstojn de la germana 1900um-jarcenta filosofo [Ludwig Wittgenstein](). Mi fidas ke la frazo ”pri [tion] kiu oni ne povas paroli, oni devas silenti [pri tion]”, tradukite, donos eblecon al malgermanparolantoj komprenu multe de la kanto -- oni ja povas diri ke ĝi estas kerna aŭ ŝlosila frazo. (Cetere, enwp fakte diras ke la teksto de la kanto haveblas en esperanto en la albumo).

--><!--## Tri bienistoj-->
<!-- Mi eke elpensis|ekpensis anekdoton.

En vilaĝo loĝis tri bienistoj. Estis primtempo, kaj un de ili|_la unua bienisto_|Un bienisto diris al si mem ke ri ĉi tiu jaro devis frue planti|SOW la granoj? ktp ktp, ke ĝi *nun* devas fari ĝin, ktp ktp. [eble ke ri faris, sed malfrue kaj malbone planita.]

La dua bienisto komencis kun plani [faris bone, sed malfrue]

La tria bienisto ne pensis pri ke li devas planti, aŭ planis kion fari. Li nur komencis. [faris frue, sed iom malbone.]

La venonta jaro? la dua kaj la tria bienisto, kiu la pasinta jaro vidis la problemojn de la unua bienisto, kompreneble helpis la unua iom plani kaj komenci sen pensi tro multe (pri la fakto ke ri devas komenci), ĉar ili estis DECENT homoj.

[ne certas kiun plej bonas, plani aŭ komenci. mi ŝatas planadon, persone. Sed mi supozas (kaj aŭdis, de lertuloj, aŭ lernitaj uloj) ke povas esti problema se troiĝas. Ekze, ekzistas aferojn kiuj oni ne devas plani multe, se oni pripensas fari tiuj tro multe antaŭ komenci, oni SPEND/uzas/konsumas energion sen resulto/NYTTA.]

\#anekdotoj #psikologio
-->
## La soldato Lansa

[Västgöta-Bengtsson](https://sv.wikipedia.org/wiki/V%C3%A4stg%C3%B6ta-Bengtsson) (1908—2000) estis instruisto, kiu ekinteresiĝis pri la historio kaj la dialektoj de lia lokumo. Li komencis intervjui rurajn maljunulojn pri iliaj spertoj, vidpunktoj, rakontoj kaj historioj kiuj ili mem aŭdis — foje ege malnovaj. Tiel, li kolektis multege da materialoj pri la vivoj kaj spertoj de la popolo en Västergötland en Svedio. Kaj anekdotoj. Jen anekdoto kiu laŭ Bengtsson mem kreis lian reputadon kiel rakontisto — miakomprene tamen unue prezentita en periodo antaŭ lia kolektado de popol-spertoj:

> Longe antaŭe nun, je la tempo kiam 'Napoleon Dundrapartne' umis kaj tiaj aferoj en la mond', venis mesaĝon ankaŭ al nia soldato, Lansa, ke ankaŭ li devis esti en la milito, tamen tio la uliĉoj hejme pensis estus multekost', se ankaŭ li devis esti en tiu afero — ”Li devas havi novajn vestaĵojn. Ne, tion ni ne povas fari”, kaj oni kreis grupon kiu iris al la urbo por paroli antaŭ la altaj sinjoroj.

> Kiam ili alvenas al la tabulo tie, estis Johansson de Storegårn, ĉar estis li kiu havis la donaĵon de parolado, kiu estis elektita por diri kion oni volis estu dirita:

> ”Nu, estas tio ke venis mesaĝon al la kampar' nun ke nia Lansa devas kunesti en la milit' ankaŭ, sed ni opinias ke tio estos multekost', ĉar li devus havi novajn [spronojn](https://eo.wikipedia.org/wiki/spronoj), kaj novajn botojn, kaj nova 'dölma'.. do ni nur scivolas tion ke —, nu, ĉu ĝi ne estus pli malmultekosta, eble, anstataŭe *hejme* pafi Lansa?”

#### Komentoj
Mi provis imiti la senton kaj stilon de la rakonto kaj la dialekto, tra strukturo, vortformoj kaj -elektoj. Tiel ĝi eble ankaŭ iom strangas. Bengtsson, kies vera nomo cetere estis Sixten Bengtsson, en familia maniero ja diras ”nia soldato”, eĉ se la rakonto temas pri tempo ĉirkaŭ 100 jaroj antaŭ lia naskiĝo (ankaŭ ne necese en lia hejmvilaĝo). La nomo Lansa, se venas de kutima vorto, korespondas al standard-sveda vorto lansen, ankaŭ uzata de Bengtsson la unua fojo en la rakonto, kaj kiu signifas la lanco. Laŭdire, kutimis ke oni donis familnomojn al soldatoj kiu temis aŭ pri ecojn, kiel *Flink, Glad* kaj *Frisk* — Rapida, Ĝoja kaj Sana, aŭ pri armeaj aferoj, kiel *Lans, Granat* kaj *Svärd* — Lanco, Grenado kaj Glavo.

Pri elparolado, ”västgöta”, kiu kiel prefikso signifas proksimume ”rilata al Västergötland”, estas proksimume *veĥeta* aŭ *veŝeta,* ankaŭ depende de la dialekto, kun la sveda [sje-sono](https://sv.wikipedia.org/wiki/Sj-ljudet). ”Västergötland” estas proksimume Vesterjetland. La sveda sono skribita ”ö” (fakte kelkaj similaj sonoj) pli-malpli samas kiel la vokaloj en germana *mögen* kaj la franca *bœuf,* kaj similas la vokalon en angla *bird.*

\#anekdotoj #Västergötland #Svedio
<!-- a220401 (wp→gl220402) -->
> _Ĉiu videbla teksto en ĉi tiu paĝo havas liberan perimisilon – cc-by-sa. Tio licenso signifas, ke vi povas libere kopii ilin, kaj uzi kiel ajn, kondicie ke, se vi iel kun aŭ sen ŝanĝoj publikigas ilin denove, vi deklaras kiu faris tiun kiu mi faris en la partoj kiuj vi uzis – eiri je github.com/eiri2/kialne/kialne.md – kaj ke vi mem aldonas similan aŭ la sama (mi iam kontrolu) permisilo._
