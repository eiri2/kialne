## Rules in Esperanto

A try to express grammatical rules in Esperanto in a logical and condensed way (if possible, see below), for comparison of complexity (and also utility in it's own right). Pragmatically started in English instead of Esperanto, because I've heard something about most things about Esperanto is in Esperanto, which keeps things from outsiders, and doesn't give publicity.

* Word endings: accusative: -n (also see below), plural: -j(n), nouns: -o(j)(n), adjectives: -a(j)(n), pronouns: -i(n)|((a)(j)(n)), verbs: -i, -is, -as, os, -us -u, adverbs: -e, some words that end in -aŭ, and some with no regular ending at all.
	-int-, -ant-, -ont-
	-it-, -at-, -ot-
* Word order: very free, but not limitlessly so (?). SVO is most common, after that perhaps OSV – which however is rare. Accusative therefore needs to be marked (because of the free word order).
* Articles: only one, definite: la.
* -o, -a can be skipped and la turned into l' (la arbo→l' arbo, written that way), but this is normated against, accepted and occurs in poetry and lyrics.
* po
* da, de
* centuries are usually called by their ordinal number: la 19-a jarcento – the 1800s (but other options are possible).

### Notes and discussion
A difficulty with this is clearly what to count as a rule or not. Different special words, such as ''da'' vs. ''de'' – foe people like me that doesn't have anything similar in our first languages, it feels like at least a rule, but for someone that has a natlang that functions in a similar way, it's just two regular words (Swedish actually neither has any da, nor any de in common usage). The most awkward words could count as rules or rule-ish phenomena then, by creating complexity – or at any rate, they do the latter.

There are some complexities in Lidepla as well, such as yok ("none at all"?), double negation is just emphazis (not inversion) (for example, no, we don't have no banana means that there is no banana), double systems for some things (pronouns for instance), dan (accusative marker with irregular word orders), and so on – but, as an example from languages I've recently used, it feels like Lidepla is simpler and less complex in grammar (which for an auxlang is crucial, in my perspective) (and there are many that are quite a bit more simple) – but, this could just be my subjective feeling/experience, without actual objective basis. In objective terms, if someone ''orkar'' do the comparison, it could come to show that Esperanto is actually less complex, or they're equal, or Lidepla is simpler. That's something that makes a comparison like the above – or one based on this type of summary, rather – would make sense.