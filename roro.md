## Ro

Looking into Ro felt like something that could be fun (and that not many has done).

solresol has more redundancy and might be better in that regard. maybe same with babm, but not in writing (the letters are syllables, if they have different vowels it gives some redundancy and fallback, but I'm not sure if they have that). nao is also similar and has more redundancy (meaning-carrying units are generally/all? longer than one phoneme/letter), except with negations/inversions. the ses variety of solresol, which is clever, is also lacking in redundancy (meaning-carriers 1-2 letters long), sadly (compressing it like that is otherwise a very tempting hack, one could also use single consonant letters, for instance, for writing it).

though, in all those cases, context also givs some redundancy. a tricky case is if a pronoun is mixed up, then it might often be difficult to infer from context.

apparently, dh is pronounced as th in this. I was pussled at first at how to pronounce the h after a d and a t. adh has dh, and ath has th. the dictionary says that except the exceptions, which doesn't list th, everything is pronounced as in English, so I presume th is like th in thing.

I guess ad and ag are confusable, and even more so ad and ab. might be safest to use adh over ad and thus also af, or modify to adad and maybe afaf, if those are not in use (abab was my first thought, but makes more sense to repeat a less common one) (is he less common than I, me? not granted at all, but if one use a general animate 3ps, adh, I guess it is anyhow).

maybe one could add a rule to always add a o after b, for instance, for better clarity. yellow would become boofof, hmm. (And I abo.) or replace b with "bob"? b and d are difficult, maybe also p and b and even d and t. one could call a modified variant bobro or robob. now yellow would be bobofof, and I abob (a little impractical that that word is affected, one could maybe allow for an exception there – and have adad and afaf). (k could potentially become kak, and t tet. We don't want to reduplicate genitive, I guess – otherwise z could be zoz – so s could possibly be reduplicated to sos or sis. we would get ekaka instead of eka for was, might be good to look through common words – maybe egaga is better for having been.)

Rev. NAME developed Ro in collaboration with his wife, who he praises for her ability to pinpoint with exactness the sought after word-sense.

## Day 2

My thought was for an disambiguating dialect/reform, one could make maybe b, g and t (g instead of k) into a syllable, bob, gag and tet, or just bo, ga, te would work too. (any previous bo would then become boo, same with ga→gaa, te→tee – all spoken with a pause between the vowels, not diphtongs in the case of f ex boa, and not long vowels, I'm thinking.)

This would keep apart very common ag from ad (the latter common depending on usage), which wouldn't be achieved if we "marked"/syllabified k (which was/is my spontaneous impulse). More common verb tense _ek_ (past tense) would also be unmarked, but (instead) rarer _eg_ would turn into _ega._ I'm thinking those are good effects (possibly there are numbers of also common words that would be affected in a less optimal way).

If we continue this weird idea of doing things rationally, however, it makes sense to count how many of each letter it is in the dictionary. "grep g [file with all ro words, only]" etc tells us that there is 555 g, 786 k, 955 d, 378 t, 1441 b, 534 p. t vs d and g vs k thus are rational choices, but choosing b is to choose the most numerous of these. I'm not sure I need or want to do this 100 % rationally, but with that said, it is also so that b disambiguates from both d and p (sum 1489, right?), so it fills a double role, sort of. This thus has a better effect, and.. I don't know if the alternative would be to mark both d and p, but those are slightly more numerous together. Let's skip the rationality on that one for now, and see how things work first, at least.

And to get a good measure of the impact of the changes, one should check frequencies on a big corpus of texts/actual usage. Even if t is rare among the words, it might be much more common in the most used words, say. However, there is no large corpus of texts in Ro.

But there is some small samples (and one could try to make translations).

(Ne, mi ne adresis m kontraŭ n. Eble ĝi ne estus bezonata.)

## Day 3

So, it seems that the books contain at least two different versions of Ro, with at least somewhat substantial differences. So far, I've been relying on the version in the 1919 dictionary.

[translation started here, see below]

Can't find a word in the translitterated dictionary for problem, worry, trouble or anything similar. There's two words for "of", and it's not very clear from the list in the dictionary any difference between them. "evening" is defined as "at evening", and that context doesn't tell how to say just "evening", unless it's the same word. I'm going to go with the latter for now.

Okay, i-words and o-words are both prepositions, opposite of each other. What that now means (in this case). _on_ means "not in", apparently.

_in_ and _ov_ doesn't feel extremely a priori. not extremely so, and I think there are examples like that in other a priori languages, well, there are in Kah, at least.

A covered street, is that in passive voice or is it past participle? Reading WP, both feel a bit off/weird.

The examples in the beginning and the transdictionary (the transscribed one that is, but I checked the pdf too) doesn't seem to agree about the word to write and it's root (rufieb vs rufibeb).

> ebi ruf, or, rufieb — to write  
> ebo ruf, or, rufoeb — to be written  

> rufibeb – to write

This is confusing. An active verb should have a final i ending, a passive a final u. What is the b doing there? And I learned just now that the tense forms can be put before the verb, or as affixes (still, help verbs seems to have 'em as prefixes). What is then the prefixed form of _rufibeb? ebi rufb_ or _ebi ruf?_ And will _defacu_ from the noun _defac_ then mean "covered"? I've been looking in the dictionary for any pair as precedence, but failed so far.

There are 45 -ibeb words in the dictionary, and 47 -ieb ones. All seem to be verbs ("to X"). 4 -fibeb words, 2 -fieb ones.

Anyway, the passive of rufibeb must be rufobeb (rufiboebo vs. rufoboebo in disambiguated version).

And actually, we don't want the literal infinitive, but we want the "(is) written" form. Maybe _defacoel_ (unless a random b is supposed to go before the suffix).


### Pronunciation

  “Pronounce the five vowels a, e, i, o, u, as in father, they, police, no, truth; c as sh, or c in ocean; g as in get; j as s in azure (French j); q as ng in sing; x as Greek x or German ch (sonant h); dh as th in this; dj as j or dge in Judgre; tc as tch in match; aw as ow in owl; ay as ay, or ai in aisle; the other letters as in English.”

(W and y are consonants, and accent is only used for emphasis.)

sorabji:

ab – I, me\
ac – you (s.)\
af – she, her\
ad – he, him\
adh – he, she\
ag – it

abz – we, us\
acaz – you (pl.)\
adz, afz?, adhz?, agz – they, them

possessive forms:\
abe?, ace, afe, ade, abze, acaze, agze etc

at – the\
ate – of the\
ap – a, an\
ay – some\
ayar – someone\
ayid – somewhere\
al – this\
am – that

ud – and\
ub – but


one could easily do a table of correlatives.

genitive is thus -e and plural is -z.

### Verb tenses

The various forms of "to be":

eba — to be\
eca — be!

efa — has been\
eda — had been\
ega — having been\
eta — will have been\
eva — to have been\

eka — was, were\
ela — is, am, are\
ema — will be\

era — being\
esa — about to be

~~I'm not sure when to use "be" when there is "to be", and also "am, are, is".~~ edit: apparently -ec- is imperative! added exclamation sign.

The common tense forms ~~prefixes~~ affixes ought to then be:

eb – infinitive  
ek, el, em – past, present, future  
(er – continous)

ef – has, have X\
ed – had X\
(eg – having X)

a prefixed n- negates the meaning, and w- creates a question.

I guess it would have felt lighter with free standing tense and mood (and aspect?) markers, imo.

Apparently the tense affixes are prefixes only in help verbs (or short ones?), in the others the verb root seems to come before the tense affix/suffix.

## <Worry> iv/ov Hubbard
Eka tagnib iv/ov Talrad in London. Dujab eka <covered defacoel?/defacoer?> in githob bifab (mist, fog).
