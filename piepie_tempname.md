One thing that would be really cool to do, would be a simple-grammar simple-vocabulary etc language based on PG, but what's difficult about it is that there's no quick way to look up the relevant roots and then decide what to do with them, eh, what to use them for – cognates in various languages are listed, but they often differ in meaning to the PG root (I suspect one would end up with many words for certain things, maybe baskets for instance, and then many gaps where the decendants use PG words used for one thing for a completely different thing, and then what PG word to use?).

So awaiting that, and thanks to the new proto language dictionary (kudos to the creator hmslima for that), here's a simple language based on PIE roots (included in the dictionary) and Mundeze word endings, as an "Esperanto with PIE roots", but instead based on Mundeze grammar (because it's ~~better~~ simpler – less complex, more regular). [Okay, the ProtolangSearch is gone, back to the drawing board, I guess.]

Might as well start trying to harvest PG-words again then, maybe? Because the accessibility was a big reason to try with PIE. Or maybe a PIE Mundezido is still a fun idea?

Rather than looking at an English word frequency list, it's probably better to look at an Esperanto one to start to dig for vocabulary, because English, like many natlangs, has lots of homonyms, and then you don't know which senses of a word you need to look for (could look for all, but somehow feels easier if there is one sense per entry in the to-look-for list), and (systematic) auxlangs generally avoid that, and the only auxlang I'd expect there to be robust frequency lists of, is Esperanto.

(I had previously btw thought of what simplifications of Esperanto grammar to use/employ, until it dawned upon me that I could use the grammar from a good esperantido, or even esperantidecaĵo – like, Angos and Mundeze are both sort of esperantidos, but very elaborately so, so to speak – as in, esperantic in a sense, but still quite remote, in a good sense, from actual Esperanto. Not sure Angos is defined as an esperantido (necessarily), btw, I think the Esperanto origin/influence is at least clearly expressed in Mundeze iirc, probably mentioned in Angos contexts too, but then what is an esperantido or not is a philosophical question – which usually means that it actually is a semantical question (matter of definition). Btw, Esperantidos – that's a biker gang you don't want to meet/encounter in a dark alley!)

The dictionary-app for PIE that I found is great, but, it's a bit confusing that it doesn't specify for instance what sense of "to" the two translations for that has. So we're back to the English ambiguity problem, even with using an Esperanto word frequency list.

https://indo-european.info/dictionary-translator/list.inc.php/English/Indo-European – there they are, the baskets! I told you!

But that's great, there is a list page too, that's useful (easier imho than typing in the search field). Oh, I have a (actually a few) "lookup in Wiktionary" plugin, that might come in handy now. Except that the PG-words are not in regular entries. Oh well. (Why does ddg sometimes send me to svwikt? I don't want to know what to means in Swedish Romani. Or maybe I do. Oh, interesting, same as in English.) If I understand Wiktionary correctly, both the preposition to and the particle comes from PIE de/do.

Yes, I'm gonna want phonological simplifications (I think – well, yeah, I mean, certainly those unknown phonemes), but maybe it's good to collect some words first and see how they look. That's what I'm doing anyhow.

So, for "to", wiktionary doesn't give any of the two translations on the .info page. The Swedish word "till" somehow has it's origins in an "*ád (“near, at”)", so that's not a good candidate anyhow. de/do: ablaut, they say (not quite sure what that is). Maybe do is the less confusing one of them.

There's a language called Lolopo.

Oh, the all-words-in-one-page page doesn't have all words in one page.

So far, by looking up things in Wiktionary, we're getting words biased to English and Swedish word stock (because that's what I look up).

and now there is a coup in Niger. what's that called in swe? Niger, ~~tydligen~~ apparently.

## Day 2
Nouns and other more regular words are probably easier to choose than function words such as conjunctions and pronouns and so on, that sometimes are hard to choose due to differences between PIE, and say, standard simple auxlang grammar. If it sounded like I would follow/copy Mundezes grammatical structure in a syntactical way, fully, I won't probably do that, um, maybe. I'm thinking about semantics being kind of trimmed and the smallest of word being used to it's full utility by being used with different endings. Following Esperantos vocabulary, I also won't get that, I guess. Also not saying it's bad in Mundeze, possibly it's needlessly ambitious for this project at least, and not the least, it would probably be too complicated to copy it (too laborous, I mean).

I spot the word international. That might be a slight challange for PIE. Well, find a suitable candidate for inter- or between, and national/people-related or state/country and combine them, I guess. The word is of course overweighted in eo frequency, I presume.

Of course there needs to be some kind of method regarding simplification or transscription of sounds. It could be a clumsy and, hm, coarse-grained, even, but preferrably consistent.

The simplest would be to not have vowel distinctions, and just reduce every long vowel to a short one, if that is possible without too much conflicts – I mean, it's a quick way to do it, less work. [If I got it right, there are not so many minimal pairs, and there is some theory that originally the language only had two vowels.<!--something I didn't understand is that it presented regular PIE as though it only had long and short e and o, while the reconstructed words have at least u and a too.-->]

Same with the unknown h1–h3 consonants (laryngial theory), or I mean, it would be easiest to remove them. It is also so that they won't be very familiar, because they have vanished in the descendant languages to the point that they barely know that they did exist. On the other hand, it should perhaps *not* be recognizable, in order to be more neutral, and some h:es or what one would use would make it truer to PIE, if that's what~~'s~~ one's after. Then again, word structure gets weirder and less simple, and the simplest is ~~perhaps~~ [probably] to skip them.

## Day 3 – name and revision
(I'm going to name the entries here not after days from start, but which day of making a notice. Least messy, difficult and confusing. Rather obvious, of course.) (I have done some additions since my last notes.)

I could add to what I wrote the other day that among the most frequent words there are many of those ‘structural words’ that are (sometimes) difficult (pronouns, conjunctions, other function words). Also, I don't have to follow a frequency table, I could just collect words, or collect common words (common verbs, for instance), and things would roll on and develop. Often when I look for one thing, I find another, also. That could be taken advantage of.

I have also found a database of PIE words, yay! It's much easier to search there for a particular sense than in Wiktionary (where, apart from that big list of spread words, I look up a root in a language I know, and see what that results in in PIE). [Though, the database is sorted in a hierarchy of languages, and above the one I use it says "Nostratic", which iirc sort of questions the validity of the database. Otoh, it is used on Wiktionary – but I don't know how often.]

Rather than going bottom up (or top down, whichever way you see it), and going from a modern root and see if you can squeeze in the meaning you want into the root you find, it might be better to just harvest PIE roots and see what senses one gets, so to speak – still by searching for the sense i want in modern speech, but using the database above or the list.

And in that way there might be some revision needing to be done, and most of all I should revise the earliest entries that I took from the Dnghnu (?) project lexicon.

I have thought of a name, by coincidence – _Pangea._ This when envisioning a logo, so I have an idea for a logo/flag/symbol as well. I might return to that. Oh, btw, this was the second part/version of the idea – a green star (representing earth/land) (in a nice shade), in a nice shade of blue (those tend to be somewhere between light blue and dark blue). Five pointed stars (yes, I know, extremely Esperanto of course) are usually the normal/nice looking ones, so if we say that the legs (?? or arms?) of the star represents continents/parts of the world, America would be one part, then Africa, then Eurasia (!), and then Oceania. Which leaves out Antarctica (not very green, but yeah).

In an interview on Youtube with that American professor of Old Norse, to like is mentioned as difficult in PIE. I've made a try below, if the info in a greek word entry is right it would be _prei._

## Day 4
A clever way – which would maybe give some more variation – would maybe be to keep one of the unknown consonants as "h", or some other suitable consonant – in other words, h1 turns into regular h, for instance.

Question is also if it makes much sense keeping the "superscript h-es". Some time it has looked like that, but I guess maybe they are just something that affects the pronounciation of the following vowel. I might check that.

## Day 5

A much simpler process of getting the roots would be to just accept the form in Pokorny or American Heritage Dictionary. Using Wiktionary only, I have to ~~take a language~~ look up a word in a language I know (bias Germanic, slight bias Romance), and see if the ancestor of that word seems to be a good contender – often it has had a different meaning, with the words I've tried. However, Pokorny has dated forms, that has often been changed in light of research that, if I got it right, in some cases were around already when he compiled his dictionary (in other words, it's a conservative estimate/compilation, which has both benefits and disadvantages, I guess), and American Heritage seems to be roots that are existant in English (Germano-Romance ones, in other words).

A possibility that occured to me now, is to choose an arbitrator language to look for roots from. Maybe Latvian or Lithuanian, which is sometimes said to be close-ish (mainly the latter I think) to PIE (and maybe mainly in grammar features), or say, Turkish for its location (hey, is it even IE? Not quite sure.), or ~~Hindi~~ Sanskrit or some Indian variant, or why not Persian (the eastern ones to mitigate western bias, also). It's practical if there is word info in a script that I understand (the list includes several varieties of the Latin alphabet), and there is more documentation if it's not a small or slightly obscure language. I thought of testing Lithuanian, Persian might work too so I might test that one too.

## Day something

So, the feeling is that actually researching and veryfying PIE forms from Wiktionary is way too cumbersome for me to get on with the project. One obvious option is to accept the bad data, and use Prokoptyn, eh, the actual name evades me now, and AH. It would be quite all right for something that is mainly a sort of demonstration/alpha thingy, but it does bug me a bit. I had decided now, though, that that's the viable way forward.

Or, as I thought just now, one could learn how to scrape for a PIE link in the article with a certain name, and output data from the target of that link. I've been trying to find Python Wiktionary scripts and similar before, but maybe I've found a possibly more fruitful entrance like that now.

If using P or other not-so-up-to-date sources (otoh, however, Wiktionary is not perfect either), checked with Wiktionary vs. other source could be marked in the dictionary (the Wiktionary ones in this csase marked, I've settled, because there are fewer of those), either with an apostrophe before the word, not used in writing, or simply a marking letter after the word, WT maybe (I had though of a P for marking the other way around, except the other option of ', which I thought of first). I'm rambling, should sleep.

## Day 8

So for now, it seems like using Prokorny/AH is most viable. I've been thinking that a possible name then would be _Prokie._ It sounds nice, and underlines the dependency on that source, and sort of honours it too.

## Workbench
*ǵʰmṓ m (Lindeman variant: ǵʰm̥mṓ m), earthling – *dʰéǵʰōm f, earth, human – *mon- man, human being  ghmo, gmo, **mon**  ghmo etc means farmer, ploughman? should be used for that, probably. "man", does that mean male human, the man, or just human? unclear with English.  
*h₁er-, earth	er	as in what we live on, or the soil? (maybe both, and that's a possibility too, anyhow.)  
*yóh₁r̥ n, year – *wétos n, year – *wet-, year, year-old	yor, yohr, wetos, wete  
*ǵʰéyōm f, winter, year (measure of time), frost, snow	gheyom, geyom (winter, frost, snow?)  
*h₁édti (imperfective) – to eat, *h₁ed- (imperfective) – to eat	edti (edi)  
*menth₂-/*mendʰ- – to chew	menti, mendhi, mendi  
*sekʷ- (imperfective) – to follow	sekwi  

<!--ahaks `Taube' (als Seelenvogel) – just interesting, seems related to to understand or something?-->  
men-3  
to think, mind; spiritual activity  
`denken, geistig erregt sein'  
*men- – to think, mind · spiritual activity	meni  

tong-1 (*teng-)  
to think, feel  
denken, fühlen'	tongi  
*teng-, to think · to perceive	tengi  
  
*h₁é	and, then (revision/check)  
kʷe  
and (encl.) – as a suffix  
enklit. `und'	(-kwe)  
*ḱóm, beside, near, by, with	kom  

like:  
*h₂ew-, to enjoy, to consume	ewi  
*keh₂-, to desire, wish	kei  
*preyH-, to please · to love	preyi  
*preh₂- to like, feel friendly/well-disposed (in greek entry) (is it correct?)	prei  

[some more]  
*h₁ey- (imperfective), to go	~~h~~eyi  
*h₁éḱwos, stallion, horse	ekwose  
*h₁su-, good	sua?  
*h₁wésus, good, excellent	wesusa?  
*dʰéǵʰōm f, earth · human	degome, dheghome (just human?)  
‌‌wen-1, desire, strive for.	weni, wene (AH)  
wes-1, live, dwell, pass the night → derivatives meaning "to be." *h2wes‑. (AH)  
tkei-, to settle, dwell, be home. → home, hangar, situate. (AH)  
*treb- settlement, dwelling · to build, dwell → thorp, village, hamlet.	trebe, trebi (dwell)  
‌‌weik-1, clan (AH)  
*deh₁- to bind (compare deu- in AH)  
deuk-, lead (AH)  
*déḱs, right (side)  

not in mz, nor PIE	-  
*h₂epó off, away	epó	works for both in PG	PG *ab 1) away from, off of [+dative] 2) of [+dative]  
atqe, enim, itaqe, joqe, qe, *éti, *h₁é	~~enim~~ ~~eti~~ he/e	eti: beyond, over, and  
en (!), h₁én (adverb)	en?	PG *in  
bhewmi, esmi, *es, *H₁es-	~~esmi,~~ es  
de, do	do?	to (seemingly any meaning)  
*h₁me-, *éǵh₂	eg  
*ne, *nē, *nēy	ne  
*kʷos, *kʷis – related *kʷo-, *kʷeh₂-, relative pronoun origin	kuis, kuos, kuo, kue el. kwis etc		vilken från vad likt, typ. som från *somh₁ós, same, alike, alone?  
*ís or *h₁e 	is	e krockar med och. via ”er”.	the (just named, anaphoric). This demonstrative was used instead of a third-person pronoun.	[han lite oklart], he from *ḱís  

*pro- toward, forward, *per- before, in front     first	pro, per	för: *preh₂- before, in front – From *per- (“before”)  


só: pl. acc: *tóy, *téh₂es, *téh₂, pl. nom:*tóms, *téh₂m̥s, *téh₂	té	också OE hie, från *ḱís, this (here)  
*túh₂ you, thou (2ps), *yū	tu, yu  







*wei "we"; *n̥s-mé, encl. *nos "us" 	wey?, nos?  



revision of só:  

masc sing pl  
nominative 	*só 	*tóy  
accusative 	*tóm 	*tóms  

fem ditto  
nominative 	*séh₂ 	*téh₂es  
accusative 	*tā́m 	*téh₂m̥s  

neut ditto  
nominative 	*tód 	*téh₂  
accusative 	*tód 	*téh₂  

Okay, seems like this should mean _this, that_ (s, pl) rather, and that the 3p (s, pl) is rather \*ís. Or, according to Pokorny, _so(s), sā_ means dem. stem; he, she, which is \*só I presume.  
  
tu revision, various reconstructions:  

singular	plural  
nom 	*tī̆ (tū̆)	*yūs (?yuHs)  
acc 	*t-wé ~ *te	*usmé ~ *wō̆s  

nom 	*túh₂	*yū́  
acc 	*twé ~ *te	*uswé ~ *wos  

(nuclear PIE:)  
nom 	*tuH 	*yuH  
acc 	*twe(ge) ~ *twē (?) 	*us ~ *wōs (?)  

nom 	*tuH 	*yuH  
acc 	*twé 	*usmé ~ *wōs  

nom 	*tu- 	*yuh₁- 	*yu-  
acc 	*twe 	*uh₁e 	*usme  

tu, twe (but is follows Sihler, to not collide with e, which would be ti, twe)  
yu, usme/us  

epo	of, off, away  
e	and  
en?	in  
de, do	to  
ne, ney	no, not (due to esp)  
kwos, kwis	which, that  
mon	human  
ghmo	farmer, ploughman  
ere	earth (what sense?)  
yore, wetose, wete	year  
gheyome	winter  
  
me, eg	me  
tu	you sg  
is	3ps  
wey, nos	we  
yu	you pl  
te  they, them  

es	to be  
edti, edi	to eat  
mendhi, menthi	to chew  
sekwi	to follow  
meni	to think, mind | (spiritual activity?)  
tengi	to think | to perceive  
(-kwe	encl. and)  
kom	beside, near, by, with  
ewi	to enjoy, to consume  
kei	to desire, wish  
preyi	to please | to love  
prei?	to like, feel friendly (inclined maybe?)/well-disposed  
eyi	to go  
ekwose	stallion, horse  
sua?	good  
wesusa?	good, excellent  

## Swedish ON
Starting this up a bit again, one thing that would be possible and a bit simpler is to either do an isolaing PIE or PG, based on Swedish.

Oh, it doesn't make much sense? Hear me out. What I mean is that for simplicities sake I could construct the Swedish 'dialect' of such a thing, by simply looking up the origins of the Swedish words, and not worry about which root and meaning is more common or which one to use, just take the Swedish one (some words have a shared ancestor, so some deciding would remain (buggar)). Or some other language I know, such as English (but from that end, Swedish is simpler of course) (there might be less data, though).

So, ~~adapting to~~ combining the two trends of back to the roots and minimalism, here's Swedish PG or PIE or maybe even PN. (Or ON, maybe rather. Don't know if I thought of the distinction.)

Oh, yeah, also, another way of simplifying things could maybe be to utilize this new Robotic Overlords-technology (RO) that everyone is talking about, if one could teach that to scrape the form and the meaning from every word in a category on enwikt. (Or maybe just go through the category in question manually.) (Which might be what I'm just about to do, hm.)

### Unrelated
* https://medium.com/@Crualiaoch/an-updated-report-on-the-yola-language-55c2334ae377 – a bit messy about the origins, where did the flemish-normans start speaking old/middle(?) english?

### Work bench

Preposition	af 	of, from, off, by			
	hjǫlp	help			
Conjunction	at 	that			
Particle	at 	to (infinitive particle)			
Preposition	frá 	(with dative) from, away from, about			
Adverb	frá (not comparable) 	away	hence, henceforth		
Preposition	fyr, fyrir	before, in front of	for		could split in fyr – for, fyrir – before, in front of
Preposition	fyrir útan 	(with accusative) outside			