An experiment in creating a simple and neutral langauge, by taking vocabulary from one or a few not large languages, using simple generic grammar, and maybe adapting/distorting phonology. Working title _ei soumi._

This has been preceeded by ideas since quite a few years abouit making isolating versions of very non-isolating languages, and recently by ideas of what I would want in my language, if I would create one. Maybe there will be some influences of that.

I know already from the start, or have already thought about, that the long vowels and long consonants of Finnish might be something that could be a bit too complicated/problematic. I've also thought of changing maybe (some) t:s to d:s, k:s to g:s, and v:s to w:s[for a less cold/softer sound]. Another preliminary name idea has thus been (and is) _Derwe._

And first of all, other considered languages has been Japanese and Are'are. The phonology of Are'are are probably unfortunately a bit too simple.

Lithuanian (or is it rather Latvian<!--that is said to be close to PIE – no-->?) also popped into my head for something similar, after the main idea. And now while writing, Basque could be a contender.

Yet another good contender is Hawaiian, which is supposed to have a nice and simple phonology – ~~some conlanger called~~ David J. Peterson has used it as a basis for an experimental language for reasons like that, iirc.

A problem that will occur with more or less any language, is that it might not have words for all the concepts we might want, because of things being expressed with suffixes, strings of words, word order, intonation etc. In Finnish a lot of things are expressed with cases, at least prepositions. I imagine one could use words for "from something", "from this", "from that", or simplify endings. I did the latter with _ta_ and _tä_ below.

Also, being 100 % correct with word choices might not be super important. Adfter all, it's supposed to be neutral (and natural) and distorted – or rather, (some) distortion is not a bad thing (as long as it doesn't make things completely off or unnatural) – and also simplify the word generation process. Thouigh, how much will it make the langauge difficult for Finnish speakers? "This word is used super weirdly in ei soumi, I always get confused." (The same phenomena that makes me imagine learning a language close to one you already know might be difficult, because confusion and mixing up will be plentiful. Roligt, so to say.)

lukien, **luota, ta** or tä – from

tämä – this, this one, this fact

se – it, that, this, he, she

tuo X – that X {kanske också som DTRM, för symmetri med tämä}

on – is (there is, there are?)

Let's skip definite marking if possible, this and that could be used for that, possibly merging those two senses would be neat.

'an/'na? – of – seems to be -n, but not in all usages (genitive case otherwise). could use na as in Kah (compare na from Esperanto sen Fleksio). (-na – as.)

'en – genitive marker, opposite of of (also 1ps ‘not’, but 3ps is probably better choice there.)

ei – no, not

joo – okey, yeah

kyllä – yes – 'kylle, **'kyle**?

ja (niin) – and

'vaile (vaille), 'le – to (prep.)

Probably we can do with the indefinate article as with the definate, skip it. When clarification is needed, one could probably use "any"/"whatever"?

sisään, 'siseen, 'sisen – in

sinä, 'sine, sinua – you

The apostrophes marks modifications of finnish roots and affixes.

'mine – I

joka – that, which, sv:som

? – that, sv:att

hän, 'hen – he, she, sing. they

(Loosely based by word frequency on espressoenglish.net)

<!--sammanställde ordlista

-->
I don't like the company Google (for obvious reasons, if you ask me – same applies even more to Facebook), but I do use Google translate for finding the words.

A good complement would probably be a language that uses free-standing prepositions etc more, instead of cases, to fill in those blanks with. We'll see if I'll do that (if I'm bothered enough, if it's worth the trouble), but it's a possible method at any rate.

## Grammar

Let's try out some grammar, I thought of starting with here today.

I also had a comment on phonology/ortography, which led to another thought. I have so far, as you can see, reduced doubled consonants (long consonants, afaik) to just one ~~consonant,~~ and changed ä to e. The sounds are fairly similar – as as swe speaker, since some words with the ä sound is spelt with e, this is intuitive to me, but maybe less so for people with other language backgrounds. Maybe some other vowel would be actually closer, might look on it some time. Then again, most (theoretical) speakers (or the theoretial target group) won't know the Finnish words.

When thinking about writing about the above, I realised that the vowel y is not one of the ~~v~~[c]ommon ~~c~~ones, and might be unfamiliar to many. It should probably be reduced/altered into _i._

I discovered now that -na/-nä 1) is a pair (meaning sometimes ä in finnish stems from a, it's an omljud/umlaut of the a. no, umlaut is not the sign on top of the ä.) 2) is essive, expresses the state, well, it can express a state of being something – as an X, while X, being X. X na Y is not very logically compatible, it's a bit too confusing to try to invent a logic based on the finnish meaning there – [cat] na sine – your cat, sounds like it should have the meaning "while being cat you". it's not intended to be compatible, but this might still feel too weird (and it seems na could fit for continous action instead, if anything – wasn't planning on adding that, but still).

Furthermore, things like that does not only concern Finnish speakers, but in the case of ta/tä and na/nä also speakers of related languages – tho, generally they would mainly be aware of states of things in their own languages, not in other langauges (which means it feels less like an oddity, just something that is 'not the same as in my language, but probably makes sense in Finnish').

ta/tä is partive, denoting partialness, without result, without specific identity. Hm. I have only looked at some nouns in the to-form (or was it out?), but maybe also in the grammar that ending is similar to partive. [Partive is also used where a subgroup is selected from a larger group.]

Genitive is apparently –n. Also possibly something to check again – writing without looking at my previous notes, did I use _en_ for that? Might be good. (But what to use for na, that is less colliding, intuitive-wise? 'no'?)

> In Finnish, the letters ä, ö and y are the "front vowel" counterparts to the "back vowel" letters a, o and u — grammatical endings and word suffixes using these letters will use either the front or back form depending on the vowel harmony of the word they are affixed to.

These things speaks for maybe turning äöy into aou (instead of e?i – maybe eei?). On the other hand, that might create more duplicates/'collisions' (we'll see). Also, afaiu not all ö and ä are parts of this type of relation (where there is a variation depending on – I guess – preceeding/proximate sounds or grammatical role, they're just regular vowels then).

So far no grammar test at all. I mean, it could either be head-initial or head-last, and.. it feels like SVO is the most reasonable (SOV is the other globally-common contender, right? iirc, SVO trumps in number of speakers, SOV in number of languages (the latter being a vague measure)).

* **mine katso kissa**
  1ps see cat
  _I see the cat_


## Preliminary word list, so far
**'ta** ('te) – from

**'teme** – this

**se** – it (that, this, he, she?)

**tuo** – that

**on** – is (there is, there are?)

**na** – of

**en** – genitive marker, opposite of of

**ei** – no, not

**joo** – okey, yeah

**'kyle** – yes

**ja** (niin) – and

**'le** – to (prep.)

**'sisen** – in

**'sine, sinua** – you

**'mine** – I

**joka** – that, which, sv:som

? – that, sv:att

**'hen** – he, she, sing. they
