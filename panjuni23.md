# In and about Pandunia

**Ra I** e **Ra II** pas es <primitive> nave fete/bine de norge di venturer e brauzer Thor Heyerdahl.

**Sante Olaf, jong zaman su Elvis**

(o bil di ”jong zaman su Hitler”)

a dur ki mi lete a tema de Sante Olaf a dur ki mi kitabe la kitabe 'a dipe, mi eureka no unike ki Sante Olaf pas es ~~pigu kung~~ horifike jen,¹ ama a max ki a pos sata ki jen fikre ki da mata, es reporte de jen ki dite ki damen pas vide Olaf.

in yo men logia, Olaf mata de pada o jampe de nave Serpe Long a dur de la jang de Svolder in nen 999 o 1000, ama in yo men alo, da < swim > o es be salve e trajiva, e yo men logia dite ki a pos vo, da xuru es biku.

de vo, juste ka kon Elvis e Hitler, es raporte de vide de da, e vo a men gogia a Europa – hata en < Middle East >. a max, jen pas dite ki la nan reger [Æthelred ”de dus sel”](https://en.wikipedia.org/wiki/%C3%86thelred_the_Unready) e Olaf su ni sau Astrid na donaje de Olaf long a pos ki jen kredi ki da es mata.

sual tu pas vide Sante Olaf a yo loke? cing kitabe in la notaje 'a dipe!

¹ to es adil, la tipe de logia ki loga tema Olaf su < violence and torture > a poli mar no es _pul di_ amenike (poli es poli di anamenike, ama ye no es vo lau o guai). dume, bil ki vo tipa de konkenaje es < propaganda > kontra (o to) la jen, ama mi no pas vide sine ki jen kredi ke ye es vo. a max, hin polis e alo sam sifa di xe, la yau de jen de fete¿ andoste fobike de <!--tu/jen/-->se pas es <!--tre/-->'ver max dai.

**Serpe Long**

pan un de Suen, Norge e Danske gogia ha da su nasial<!--gogia di--> sante de regal famil de xuru di 'jong zaman – Danske ha [Sante Knut](https://en.wikipedia.org/wiki/Canute_IV_of_Denmark), Norge [Sante Olaf](https://en.wikipedia.org/wiki/Olaf_Tryggvason) e Suen [Sante Erik](https://en.wikipedia.org/wiki/Eric_IX_of_Sweden) (suen baxe di _Knut den helige, Olof den helige_ e _Erik den helige_). a tema de Erik, historologiste yake no<!-- es--> ding ki da pas zai o no zai (a zai zaman, damen ding tema _vo_), ~~Sante Knut ha prive den a Suen di~~ (no, vo es alo regal, jong zaman di Sante Knut de Danske gogia), e ka Luterisme di gogia, jen a vo (o a ye) no poli fikre o feste tema damen.

la norge di Sante Olaf no es minim be san¿ de vo sante, e vo Sante Olaf pas bine ver be san nave – Serpe Long.

la lau logia dite ki _Sante_ Olaf, a pos ki da jang ankrististe nan xiu reger e <!--balata/-->pake<!--/prese--> serpe to in da su gala (! – ase di no ver, ama a zoku) ki matafa la reger a dur de da dante to vai, cepe la xiu reger su nave, la Serpe. vo nave pas es max dai ka pan Olaf su men nave, ha gau di late, e ha<!-- serpe o--> lung kapa a <front/stern> (be rupa ki serpe e lung bil di es la sam loga a lau norde baxe).

Olaf bine neu di nave, hata di max dai kon Serpe ka modele. la lau di (dai) nave, jen xuru be nam Serpe Korte, e la neu di Serpe Longe – _Ormrinn langi_ a lau norde baxe.

la a poli mar baxe-tema di nave Serpe Longe (size, sinking)
<!--Vid sidan av madonnan är Sankt Olof det vanligast förekommande helgonet i Norden-->

**Beda,** nen 672 o 673 – 26 me mes lima de 735, pas es englix di biku, be xule ja e kitabiste. da es un de maxim maxike/dai guru e kitaber de xuru de 'jong zaman, e da su maxim 'be ken kitabe, ''eglesia di histor de englix nas<!--jen, pope-->,'' faide to da <!--la--> 'ken nam<!--title--> ”pa de englix di historologia”.

da es be nam **Sante Beda** o **Puje bil<!--both worshippable and venerable--> Beda.** a zai zaman di englix baxe la nam es _Bede,_ ’Bed’ kon long ’e’, ama be rupa<!--not dict now--> to me ki la nam pas es Beda kon ’a’.

![Bede a grafe de 12 me sento nen](https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/E-codices_bke-0047_001v_medium.jpg/180px-E-codices_bke-0047_001v_medium.jpg)

_[Bede](https://en.wikipedia.org/wiki/File:E-codices_bke-0047_001v_medium.jpg) a grafe de 12 me sento nen_
