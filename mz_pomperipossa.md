## pomperiposa kon longa naze

yos, guo, guo terti yare preoso si <!--terribly, horribly--> guguo adja mahumagane, ke namisi<!--nami, lite ologiskt? även om ambitransitiva, använda namn borde ju betyda att kalla någon något.--> pomperiposa. ne reko si bela name, may no<!--still, yet--> lo si vero guo mas bela dan malo si suló. ki voy abli mentweli, kio lo oydi? lo avi bi tcia wele ni gua muke kon solo ter vedey. ni lo avi multe a 'warts' en maney ni gua 'hump' en ruge. may mas wana si loa naze, as lo si bi pede longa. voy abli mentweli, kiom tabake si uzia, os lo snifi tabake!

Ett helt skålpund åt gången!

## fufa lato rive<!--willow, Fufa in lajograse-->

### en rivlate
badone Mole jo ofi oloma diese kon swelisesta safife in suloa dealta<!--small--> dome. uno, lo preo jo guprobi' kun gutcihe'<!--bastotcihe--> ni pososo kun pulvotcihe. pososo lo pedit(un)i sop opitiley ni dorosedey/bazey ni sediley<!--chairs--> kon akwoberile<!--likoberile – limhink, faktiskt--> ni tcihe, hata (loa) kole ni weley si usa a' pulve, kala cere olomo usa<!--splashed, sprayed, covered--> a 'chalk'mace, ni ruge ni brakey davi aye<!--ache--> da lase. sweliseste buri<!--jäste--> en awe opu lo ni en terte subu ni ru/rueno lo, si, hata<!--ja, till och med--> loa dealta tuma niste so' si perulia<!--genomträngdes--> a deva dekyete/zorge ni spere a tia este. sa, ne si rara<!--strange spelling error dict, weird, peculiar--> ti lo deci<!--slängde--> tcie sop bode<!--bodo, nja?--> konu: –ne, na obare kon lo! me ne zorgi tem lo! hua safife! –ni daiti<!--itesi--> sen hata' davi suló tempe nas ici' tegice.

## Pomperipossa kun longe nos

Unves, mucho, mucho mil yar bak, (gwo) ye-te ga<!--förfärligt--> lao jadugina, kel nami Pomperipossa. It bu exaktem es jamile nam, bat haishi it es gro'/muy mucho pyu jamile, kem ta selfa bin. Ob yu mog imajini, komo ta aspekti? Ta<!--ela--> hev-te dwa syao rude oko e gran muh kun sol tri dentas. E ta hev-te fule de' veruga on <!--suy--> handas e gran gorba on bey. Bat zuy buhao bin sua nos, bikos it bin dwa peda longe. Yu mog imajini, kwanto tabak bin usi-te', wen ta farfari tabak! Tota paun per kada ves!

May to bu bin problema fo<!--a--> Pomperipossa<!--den to Pomperipossa 'afford'-->, bikos ta bin dashat-ney<!--rysligt--> riche. Ta habite in syao dom in shulin, e toy dom bin bildi-ney de sosises e hame, e inplas brikes ye-te gran duan<!--pes--> de karamel¹<!--sirapsknäck-->. Tanto riche bin Pomperipossa.

<!--Bat bu ye jen, ke kuraji-->Bat bu hi jen (bu) kuraji lai a ta, e apena bade-feya 'either', bikos ta bin tanto ga<!--foba-ney' – förskräckligt--> bade. Si koywan lai a ta, Pomperipossa<!--hon--> transformi<!--shanji--> ta inu tabla o ketla o kreta<!--griffel, chalk--> o koy otresa. Ta bin muy danjaful jadugina. Suy zuy gran griva bin, ke suy nos bikam-te puy longe<!--längre och längre--> kada ves ke ta transforma koywan. To bin elay puna.

I ye-te rega in toy landa, a wo Pomperipossa jivi, e ta hev-te syao prinso', kel nami-te Pipi, i syao prinsina', kel nami-te Fifi. Un day, li promeni in parka, akompani-te de korta-marshal, kel ~~bin~~es talgrad faine<!--o delikate-->, ke ta sol mog jawabi ”A!” o ”Ha!” a olo, kel oni shwo a ta.

(correct tense, repetition not needed.)

Oli faula tuy begin chiriki:

– Bu go adar<!--dar-->! Bat prinso Pipi bu samaji faula-gana, bat shwo a suy sista:

– Ob nu go<!--om vi skulle--> aventuri'? Es tanto boring pa dom. Me sembli-shem hev sit sentu/shiling/peni KOLLA UPP. Kwanto tu<!--order--> hev, prinsina?

– Me sol hev un single shiling/peni, Fifi jawabi, bat me mog frai yablas.

–Dan yu mog hampi kuki fan. Yu mog bi may kuker<!--hushållerska-->, prinso Pipi shwo.

E poy li go aventuri<!--vandrade de ut på äventyr--> in tume shulin, til li lai a syao dom de Pomperipossa, kel es/bin bildi-te de sosises e ham e karamel.

–Hay nu chi akshamfan! prinso Pipi, kel senti hunga-ney<!--hunge-->, shwo, e tori gran sosi de dom.

Dan Pomperipossa pon suy longe nos tra winda e kan on' li.

– Lai<!--ba--> in ahir! Me es yur karim kummata. Yu ve pai' jem-blin de me.



Mah-jadu-ney (KONSEKVENS-FÖRTROLLADE) gansa mus chi syao swate bakak-kinda – to verem es koysa otra kem KONSEKVENS-SYLTPANNKAKA!

Pomperipossa stan on playa e apogi (selfa) on suy longe nos, kel nau es kom'/egal longe kom suy gamba. Turan<!--I detsamma--> ta audi sususa in shulin baken ta<!--omvänd ordning--> e turni (sirkum'). Es korti-marshal, kel he es pa dom fo suy KRAGSTÖVLAR e depos dan (he) nokalmi-ney shuki desapari-ney regakinda/regikinda[m].

–Ha! korti-marshal shwo, al ta vidi/detekti dusjadugina. –Pa! ta shwo e tiri suy longe zian dabe haki wek/dehaki/haki los nasa de ela/elay nasa.

Bat Pomperipossa tuy (jadu-)transformiFÖRTROLLADE lu a lao wuya, kel nokalmem zai salti<!--hoppade omkring--> on playa e shwo:

–Kra, kra!

–Ya, yu ba rauki e kluki! Pomperipossa satisfakti-ney ridi.

¹ Fakta-nem _sirapsknäck_, kel me suposi es same kom _knäck_, kel es twerde krem-karamel [mah kun egale partas de sukra-sirop, sukra, e krem](http://sverigesradio.se/sida/artikel.aspx?programid=2054&artikel=525935) (in inglish).

## EU produkti rekor-kwantitaa de buemisa' elektritaa ⚡

  _Sey makala es tradukta de https://growsverige.se/2024/04/13/eu-producerar-rekordmycket-utslappsfri-elektricitet_

### Kwo eventi?

Duran un-ney charfenka de sey yar, landa[s] de Europa-ney Union (EU) he produkti 375,9 tWh elektritaa de akwa-, surya- e feng-energia.

### Kontexta
Segun dataanalis-kompania<!--firma--> Montel EnAppSys, EU krushisi[-te]<!--defeat--> rekor e produkti-te duran un-ney charfenka 2024 pyu elektritaa de libre energia kem enives bifoo. In sum/sumarem, akwa-, feng-, surya-energia, laza e biologike energia bin pyu kem haf de el-produkting de EU-landa. Shefen, produkting fon akwa-energia granifi 126,4 terawat-ora (tWh) duran char-ney charfenka 2023 a 136,6 terawat-ora un-ney charfenka,' sey yar.

Duran laste charfenka 2023, EU-landas produkti-te 358,4 terawat-ora elektritaa. To granifi duran un-ney tri mes kun 4,5 prosenta, a 375,9 tWh.

Feng-energia produkti-te 175,6 terawat-ora, premaximale' namba enives.

Surya-energia dai-te 36,5 terawat-ora, kel es rekor fo' eni un-ney charfenka e also indiki/sugesti ke surya-energia ve krushisi rekor duran dwa-ney e tri-ney charfenka pyu surya-meteo<!--utan de? - soligare väder-->.

Demanda bin<!--blev--> 800 terawat-ora, kel es 5 prosenta meno kem same' perioda 2022 e 6 prosenta meno kem un-ney charfenka 2021.

### Kwo es muhimtaa de to?

Nise demanda e gao parti de libre energia dukti-te a dekresi de el-produkting fon gual e 'fossil' gas. Shefem gual dekresi, fon 94,1 terawat-oro laste charfenka 2023, a 84,8 terawat-oro un-ney charfenka sey yar.

### Sursas

the language search tool sometimes bugs, you have to search for "a lot" to find "much", and "long", well, it appears further down, apparently.

it seems tense and plural are mandatory to mark.

> However, even in texts repeated past-tense marking is quite often superfluous, e. g., in narrating a series of past events.

Oh, nice, it's not mandatory if context is given. I wonder if more tense marking/context is still needed than in Mundeze?

useful words:
ye – there is
-em adverb from adjective

> Kola förekom bland de arabiska haremsdamerna i syfte att ta bort oönskad hårväxt, och kallades kurat al milh. Den bestod av klibbiga och bruna kulor av socker som kokat i hög temperatur.



## Pomperipossa kun la longa nazo

  _Traduko de rakonto de la sveda aŭtoro kaj humoristo Axel Wallengren, publikigita 1895._



–Nun vi naĝos, miaj anseretoj! diris Pomperipossa kaj pelis Pipi kaj Fifi antaŭ sin kun sia granda kano. Estas sanfavora/(san)bona (por vi), kaj (do) ankaŭ vi <!--gustas pli bone-->pli bongustas, kiam mi fritos vin ĉi-nokte.

Kaj do ŝi pelis la kompatindaj princo kaj princino malsupren/suben al la lago.

–Kakaka! ili lamentis/veis/(vekriis), sed ili tamen devis iri (al) en la malvarma/n akvo/n.

–Ĉu vi diras kakaka? Pomperipossa demandis. Ĉu vi eble celas _patkukon_? ŝi diris, mokante ilin.

La transsorĉitaj anseroj devis manĝi malgrandaj/etajn nigrajn ranid/et/oj kaj malsekan greson – tio certe estis iu alia ol patkukon.

Pomperipossa staris sur la strando kaj subtenis sin sur sia longa nazo, kiun nun estis same longa kiel ŝia kruro. Tiumomente ŝi ekaŭdis susuron malantaŭe en la arbaro kaj turnis sin<!--om-->. Estis la marŝalo, kiu estis hejme por siaj altbotoj kaj poste tio maltrankvile serĉis la malaperitajn reĝidojn.

–Ha! la marŝalo diris, kiam li ekvidis la aĉsorĉistino. –Pa! li diris kaj (el)tiris sian longan glavon pro forhaki la nazon de ŝi.

Sed Pomperipossa tuj transsorĉis lin al maljuna korvo, kiu anksie saltadis sur la strando kaj diris:

–Kra, kra!

–Jes, graku kaj gaku, vi! ridis Pomperipossa kontente. Nenio povas liberi vin de la enŝorso, se vi ne aŭdos la plej teruregaKONSEKVENS kriego en la mondo, kiu oni povas imagi. Ĉar do ĉio, kiu mi enŝorcis, reakiras' sian veran formon <!--sin riktiga gestalt igen-->, kaj mi iĝas ŝtonon. Sed tio ni esperu ne okazos dum paro de miljonjaroj jam... diris Pomperipossa, kaj enflaris du funtojn da naztabako.

–Tion vi meritus, ĉiuj la malgrandaj birdoj kriis, kiu revenis kun la saĝa cikonio.

–Vi ĉagrenas min, tiel ke mi varmiĝas en mia nazo|mia nazo varmiĝas|varmiĝas mia nazo, siblis Pomperipossa, kaj metis la nazon en la lago por malvarmigi ĝin.

Sed nu – tion ŝi devis neniam fari.

Ĉar sube en la lago estis granda kankro, kiu ne matenmanĝis ekde tri tagoj. Kaj tiu kankro pinĉegokaptis la longan nazon de Pomperipossa kun siaj ungegojn. Kaj Pomperipossa tiom terure kriegis, ke ŝi iĝis tute blua kiel pruno en la plena' vizaĝo. Sed la kankero malgraŭe/tamen ne lasis sian tenon.

Do la sorĉistaĉino kriegis eĉ pli abomene, tiom ke oni aŭdis ĝin eĉ ege for (sube) en Afrika.

–Tio estis la plej terura krio en la mondo, kiu(n) oni povas imaĝi! diris la cikonio.

Kaj li diris la veron, ĉar neniu birdo povas mensogi.

Fluks kaj kruks! Do la ensorĉo' de Pipi kaj Fifis tuj dissolviĝis', kaj la etaj blankaj anseroj iĝis princo kaj princino denove. Kaj la maljuna korvo, kiu salt(ad)is (tien kaj reen) sur la lagobordo, iĝis kortego–marŝalo tuj, kun ordenoj kaj peruko kaj glavo kaj altbotoj<!--kragstövlar-->.

– Ha? A! Ja! la marŝalo' diris kaj prenis princo Pipi kaj princino Fifi, un en ĉiu mano.

Kaj do li kuris kun ili, kiom rapide kiel li povis suferi, tra la arbaro kaj al la kastelo, kie la reĝo sidis kaj atendis ilin kun <!--regnofasko-->regnovipo en la maldekstra mano kaj granda <!--reĝa pomo ~~regnoorbo, pli ĝuste~~-->regnoglobo en la dekstra. Li estis multe koleriĝita/kolera, pro tio ke ili forestis tiom longe.

– Na! la marŝalo diris, trankvilige.

Kaj la reĝo tuj komprenis, ke la marŝalo celis, ke ili estis ensorĉigita de Pomperipossa, kaj ke ili ne povis kontroli tion/ne kulpis pri tio. Tiam la reĝo iĝis kortuŝitan, metis la <!--reĝan faskon-->regnovipon en la postkropo' de la marŝalo kiel (rare)/malkutime eminenta premio, sed donis al princo Pipi kaj princino Fifi po' <!--duono' de la reĝa pomo-->hemisferon de la regnoglobo.¹

----
¹ Origine ”regnofasko” kaj ”duono de la regnopomo”. La regnofasko implicite por vippuno de la infanoj (feliĉe nun kontraŭlega, same ĉiu korpopuno de infanoj) – ŝanĝita al regnovipo por eviti konfuzigo kun historia uzo de fasko-hakilo kiel simbolo de regpovo. _Riksäpple_ – ”Regnopomo” – estas la nomo de regnoglobo en la sveda, kaj la aŭtoro ludas kun la atendo/ekspekto de la legantoj, tra la posta trajtado de la regnopomo kiel kutima pomo, dividita inter la infanoj.

{Använder ’långa nos’ om Pomperipossa på ett ställe.}

https://growsverige.se/2024/05/08/kol-olja-och-gas-ar-varre-pa-alla-satt



Ĉu oni povas diri, ke lerni la akuzativon estas iom kiel trejni muskolon, kiu oni ne sciis ke oni havas?

~~Komunismanoj~~Komunistoj ofte volis revulucion – ofta vidpunktio estis ke revolucio necesas, mi ne scias multe pri la ideoj de nuntempaj komunismoj. Mi havis iom da EPIPHANY antaŭ nelonge ĉi-nokte. Kiel mi pensis pri sufiĉe ĵusa aŭ lastatempa protestmovado – cetere iom aplikeblas al alia lastatempa konflikto ankaŭ – ankaŭ aplikeblas al problemoj kun povoj en la kapitalisma/j sistemo/j – ili devas agi drastike, pro tio ke la 'povuloj' ne forlasos/as sian povon alie (aŭ pli ĝuste, la unua penso estis ke la protestantoj devas agi drastike, pro esti trajtata kiel grava en la MEDIAS). Tamen, estis du-era EPIPHANY, kaj tio estis nur la unua, malpli grava ero.

La dua, grava ero estis ke jes, povuloj ne forlasas sian povon (Ĉu tio estas deva afero? Alia temo.) vole, do kiam tia afero estas bezonata, oni ankaŭ bezonas drastan ŝanĝon. Sed tio tamen ofte okazis ne tra (homara) revolucio, sed povas okazi ofte tra aliaj manieroj.

Mi ĉefe pensis pri ŝanĝoj en teknikaĵoj, ekonomio kaj la materia mondo – marksistaj aferoj, ĉu ne? – sed ankaŭ kongruas kun ŝanĝoj pere de sociaj movadoj – tamen mi supozas ke tio ofte okazas pro materiaj ŝanĝoj, ankaŭ.

<!--Esperanto – not (at all) as easy as you might think! could be used as marketing/advertisement angle, really.-->


Isa Whitney – baruda de 'teoloji docter' Elias Whitney morti alanementa, jef-alimisti a SCHOOL komun alte de St. George – abe abito de fumi afum. Ta AQUIRE tis abito – mi ja' puri' fikir tot mi janasamat – ja ru estudisti'/sikisti': ta dulu basa al'boma de De Quincey de tae fantas' i sueni, i Isa ENDEVOUR/testot (puri) dala DROP fuju an tae tabako (puri') sedan' aradat sujo tafir sama. Siro' ren ater anik puri ta, ta tanji tot BE muda mas'EASIER AQUIRE abito karab je REMOVE ito, i duara tem jera (fuju) anik ta puri isti SLAVE pur HORRIBLE/TERRIBLE/AWFUL dambi i OBJECT ETC.

IW, BROTHER of/to nun desese UNIVERSITY_DOCTOR of deolojia EW, CHIEF guru' a SCHOOL a St. George, have dai ada of SMOKE apin.   da AQUIRE da (hir) ada – mi did krede ke mi ruhu-tene/UNDERSTAND – ALREADY da' tem' a/of da BE studer: da did haf lete

Han förvärvade sig denna vana, har jag trott mig förstå, redan som student; han hade läst De Quinceys beskrivning på sina drömmar och fantasier och tog sig för att hälla opiedroppar på sin tobak i avsikt att framkalla samma verkan. Liksom mången annan före honom fann han, att det är lättare skaffa sig en ovana än att bli den kvitt, och under flere år var han en slav under den hemska lasten och ett föremål för släktingars och vänners fasa och medlidande. Jag kan, när jag vill, se honom för mig, där han med gulblekt, uppsvällt ansikte, sänkta ögonlock och små, nålfina pupiller satt hopkrupen i sin länstol — en fullständig ruin av en god och ädel man.


<!--is an unmarked noun by default in singular? or is it whichever of singular or plural?

I see that roots with different syntactical roles seems to have different endings. Is there a system to it? Some verbs seems to end in -t, others in -a – adjectives often in -ij, some nouns in -a, adverbs in -a?-->

-at, -t, -te – adjective/noun → verb
-ij, -j, -ji – noun/verb → adj
-(e)menta – adj → adv

teni – hold, tenij – heavenly?

pur – for (conj.)

barbarar-eraro. erara barbararo. erara barbarero.



The words _this, that_ and MORE? comes before the word they refer to in Jitasama: this house and that dog is TRANSLATE, just like in English. These words, that sort of points out a specific individual or EXEMPLAR of something, is called determiners.

_Determiners are/counts as a type of pronouns._

Adjectives are words that answer **how something** (a thing, a phenomenon, an idea or abstraction) **is.** In ”a green house”, ”a big dog”, and ”happy bird”, green, big and happy are adjectives. Can you figure out which ones of the following words are adjectives?

”angry carpenter”
”blue fish”
”difficult equation”
<!--angry, blue, difficult-->

In Jitasama, adjectives comes after the word they modify: **EXAMPLES FROM ABOVE**.

THE WORDS FROM THE SECOND EXAMPLE, AS AN EXCERSIZE.


”intensely green house”, ”intensely” describes the adjective green, and should be an adverb.

|Just like adjectives, adverbs come(s) ~~before~~after the thing they modify in Jitasama:

Determiners as _this_ and _that,_ as we have seen, comes before the word they modify in Jitasama, while adjectives and adverbs comes after. Some languages put all these classes of words before the words they modify, while others put them all after (as far as I know).

Jitasama places itself in the middle there, by having some before, and the rest after, making it less biased to only one type. This also makes the 'phrases' before and after the described words more balanced and less heavy.|

_Adjectives and adverbs are both called **modifiers** – words that describe qualities of/in other words._ ~~As we have seen, ~~all~~ modifiers in Jitasama comes after the words they describe, but determiners' comes before.~~
Determiners are quite similar to adjectives, at least, but one should keep in mind that they are put in front of the word they describe. I guess they don't count as modifiers, but I haven't looked it up.



### Useful words
**mi tu ta  
nus bos un**

**mie tue tae  
nuse buse une**

**ito** – it  
**tot** – which, who etc

**isti** – to be  
**i** – and  
**dan** – but  
**a** – at, to  
**in** – in

**na** – no, not
**ai** – yes
**puri** – past tense marker

**al'** – noun – always? formed from verb
**betar** – better
**je** – than
**ru** – as
**mas** – more

### kegan
tu uaki in bed ko uba sapato i besti  
badi ren lej a tua tebi, kia ren TUSAN isti ta?  
bir lata i gelas in apartijia HELA  
solen raite SÅ al'jange toto' isti kanabil  
sajementa' estan' i soso tue kitu/al'maliki fuju  
na janasamat kia jona tu isti i kia jona tu polas ito  
karabementa'  
(mi jana)  
dan mae kitu apendamenta'  
agar nus kaiji a kaijij

ia, nus rola pij filma solo poko din (fuju)  
tua boi turek tu, moj bi tot puri isti samamenta bonementa/aona bon/betar  
bos na HA DET ajabementa bejementa  
i alan tu puri aja badi ren tot kan tua mi sai  
i alan tu isti aki dan tua karda (fuju) isti dur'  
tu solo uolan pao, dan tu musa tanji tua fono (mi jana)  
i jinda betar, pur sabab alan ito isto lodo jun




## Rura Aŭstralio proprigas/akceptas elektroaŭtojn
## En rura Aŭstralio, elektroautoj akceptiĝas

**En lando kun 8 % da elektroaŭtojn, la rura popolo malkovris ke maltrankvilo pri veturodistanco<!--atingopovo/a.distanco/veturd.--> ne devas esti granda, kaj ke elektroaŭtojn havas substance pli malaltajn kostojn**

Elektroaŭti en rura Aŭstralio kutime postulis planado kaj zorga loĝistiko, sed nun pluraj loĝantoj diras ke ili ne plu maltrankvilas pri veturodistanco.

	”Estas timo en la mensoj de homoj ke ĝi povas malŝarĝiHMM iam ajn kaj vi ne trovos ŝarĝilejojn. Ne estas tiel.” [diris mininĝiniero Samial Hasnat en Mackkay al _ABC News_](https://www.abc.net.au/news/2024-06-10/regional-rural-queensland-electric-vehicle-uptake/103914408).

Laŭ novaj datumoj kolektitaj de aŭstralia organizo por elektroaŭtoj, homoj en la kamparo estas aĉetantoj en preskaŭ 20 procentoj de aĉetoj de tiaj veturiloj, kompare al grandurbaj homoj, kiuj estas aĉetantoj en 40 procentoj de tiuj.

Ke elektroaŭtoj oferas multajn kvalitojn kompare al benzino kaj dizelo estas<!--aŭ ŝajnas--> evidenta el reagoj de homoj.

Ili postulas multe malpli funkcitenon, tempo kiu oni povas uzi por aliaj aferoj.

	”Post labori/sperto de/pri mian malnovan Landcruiser, konstante riparante aferojn, simple/nur estas ĝuo havi elektroaŭton, kun tre, tre malgranda risko pri aferoj rompi en ili~~n~~/ĝi~~n~~”, li diris.

	”Funkciteno nur/simple estas nenio, esence plenigi la vitroviŝan likvon.” ([ABC News Au.](https://www.abc.net.au/news/2024-06-10/regional-rural-queensland-electric-vehicle-uptake/103914408))

La kostoj malpli altas, precipe se vi mem produktas elektron.

”Se mi inkludas la kosto de la sunelektra generilo<!--elektrejo, produktilo--> kaj kiom mi anticipias produkti dum ĝia tuta ebla funkciociklo, kostas ĉirkaŭ 70 centoj por ŝarĝi ĉi tiun aŭton de tute malplena al plena per la sungenerilo' ni havas ĉi tie.”

Unu malavantaĝo estas la manko de ŝarĝejoj, io kiu rapide pliboniĝas sed (nun/ankoraŭ) signifas pli longajn vojaĝojn.

	”Mi devas iri pli longa/n vojo/n ol mi farus kun eksplodmotoro, do ĝi estas paro de cent kilometro pli longa.”
	
	Sed antaŭe li diris ke la vojo al Brisbane kostus lin 1&nbsp;200 dolarojn, do la ĝenaĵo ŝparas por li monon, kaj veturo al la ĉefurbo de Queensland nun kostas lin nur 24 dolarojn ŝarĝi. ([ABC News Au.](https://www.abc.net.au/news/2024-06-10/regional-rural-queensland-electric-vehicle-uptake/103914408))

## Nova sistemo por endoma stokado de sunvarmo

**Sveda Norconsult ellaboris<!--evoluigis--> energiokonservan sistemon kiu stoka svarmon el sunenergio dum pli longa tempo.**

ASES estas la nomo de la sistemo por varmostokado kiu Norconsult ellaboris. Ĝi stokas sunvarmon tra sunkaptiloj dum sumero en grundtavolo. Poste, oni elprenas/eltiras la varmon pere de varmopumpilo dum vintro. La sistemo varmigas kaj domojn kaj akvon.

”La sistemo estas bazita de sunkaptiloj konektitaj al akvocikl(il)o/akvomaŝo kiu etendiĝasORD suben al isolita tavolo de ŝtonfaruno, en la grundo sub aŭ apud la domo. Kiam la suno brilas, la akvo en la ciklilo varmiĝas kaj la ŝtonfaruna tavolo progrese iĝas pli varma.” (Norconsult)

La ilo povas malaltigi mezkvantan<!--averaĝan--> energiouzo de 122 kilovathoroj<!--ŭat--> per/pro? kvadratmetro kaj jaro|po 122 kWh de kvadratmetro kaj jaro?|al sub 25 kilovathoroj PER kvadratmetro kaj jaro.

”Dum jaro, ASES donas po sep partoj varmo de parto elektro.

–Tio do signifas ke mi de unu kilovathoro elektro akiras sep kilowathoroj varmo. Geoterma sistemo/geotermejo donas, por komparo, kvar kvarHURDÅ kilovathoroj varmo, Marcus Rydbo diras.

–Per ASES oni povas stoki energion dum someraj duonjaroj<!--orig. best sing.--> pro poste uzi ĝin kiam oni bezonas ĝin, dum vintro. La eblo stoki sunenergio tiom longe estas tute unika. La energiouzo iĝas malpli alta ol en pasivodomo, kaj oni ankaŭ ne devas adapti la arkitekta planoUTFORMNINGEN.” (Norconsult)



Ĝi stokas sunvarmon dum sumero – does an unmarked noun _have_ to be interpreted as indefinate, or would this work?

<!--Lecionoj de mondlingvo "Esperanto": la unua lernolibro por lerneja gejunularo
https://sezonoj.ru/2016/12/zamenhof-11 okänt av LLZ, lät intressant, nere
https://esperanto-forum.org/index.php?topic=1167.0
https://archiwum.allegro.pl/oferta/la-mondlingvo-tichawski-esperanto-katowice-1929-i4662011891.html
https://soundcloud.com/esperanto-asocio/tabuaj-vortoj-en-mondlingvo

https://eventaservo.org/e/Subteni
https://eo.wikisource.org/wiki/Ne_ankora%C5%AD_ekzistas_mondlingvo


## Talspråk kontra skriven standardsvenska

Talspråk är inte sämre än skriftspråk – tvärtom är det mindre konstgjort, och många gånger har det djupare kulturella rötter – men för att inte talspråket ska skyllas för avvikelser i skriftspråket, kan det vara bra att hålla koll på saker som är vanliga i talspråk, men som hur som haver|av endera anledningen inte har blivit skriftspråksnorm, och därför kan störa vissa läsare (saker som anses fel i skriftspråk). (Detta föranleddes av att jag funderade på skillnaden mellan mot och emot, om sådan finnes.)

* medans – skrivs _medan_ utan s. Jag vet inte varför man valde denna form, kanske för att den var kortare. (Man kan tycka att båda formerna skulle vara tillåtna, och jag har ibland använt formen medans av den anledningen ändå.)

https://frageladan.isof.se/visasvar.py?svar=37545


eventa servo, BEMI → https://bike-berlin-copenhagen.com/en/home
-->