## Spokil

Spokil seems to be fairly forgotten and looks interesting. I was going to say that it is interesting that it's published in 1887, but enwp says that the claim is that he started working on it in 1887, and gives the impression that it actually was in 1890.

The words seems fairly nice, and the website (there is like one, not currently online but archived) says it's a highly euphonic language, or something like that.

nature.com says that _pimo_ and _pino_ are opposites, and _peme_ and _pene_ too. That's not great, of course.

An obvious quick fix would be to replace one of them with something less ambigous, say _mum_ instead of _m,_ or even VmVm, where V is the vowel from before the _m_ (in the original Vm) (a quick fix or hack). If it is the case that opposites often are distinguished with _m_ vs. _n._

Here's the book: https://archive.org/details/spokillangueint00nicogoog

Another thing I noted early was that dimunitive is -inne, which is a bit of a.. false friend for Swedes and Scandinavians, say, because -inna and similar forms is a feminine suffix, for some (older) job titles – _lärarinna_ ('teacheress'), hm, I guess that might even be the only job title with that name, oh, _värdinna_ of different kinds (hostess, and there are professions such as _skolvärdinna_), and författarinna, "authress" (yes, they are generally dated, maybe not so much lärarinna) – but there are also other types of titles like _grevinna_ (noblewoman title), prostinna (wife of a prost, a clergy title), etc. But that's no biggie, of course, there will always be some false friends and things like that.

The letters of Spokil are presented like this, if I get the scan right:

  aeiou

  bgdvjzrm
  pktfhsln

This order of presentation impresses me a bit, with voiced/unvoiced and pairs of similar consonants.

Though, I hope it's not all utilized for contrasting pairs.

h is apparently pronounced as /S/, and j as ĵ, I guess (homepage).

**-a** – noun  
**-e** – infinitive verb  
**-i** – prepositions and conjunctions  
**-o** – adjective  
**-u** – adverb

if adjectives and adverbs are distinguishable by syntax too, it's rather clever to have similar sounds for those, I think. if we think that -u and -o is a bit similar, but maybe I had Swedish <o> in mind.

The plural is marked with -s, usually only put on the noun article (can also be put on the noun, afaigi).

Articles:
indef. – **ne, nes**
def. – **le, les**

## Day 2

I will start making an overview below. There, started.

## Day 3

Sadly, Spokil seems to have quite a bit of "bad redundancy" in related or opposite concepts – for instance, words beginning with m are almost opposites of words beginnig with n. Some things like that might be fixable, _n-_ could be substituted with _ne,_ for example. Or something like that, freestanding _ne_ is indefinate article (see above).

Or I'm not sure the above is right, it's according to Esperanto (Esperantic?) Wikipedia, but now I'm looking in the book, section _G. Indéfinis._ Transscribed the leading text to se if it shed some light:

> Il n'y a pas lieu, en Spokil, de faire une classe á part de ces mots des grammaires qui ne diffèrent pas des autres mots du lexique, en dehors des suivants, catégorisés, en Spokil, en series parallelles, au moyen des contrastiques m, n.

But oh well, I don't know.

It's also a bit tricky to read a French grammar (in French, that is), when I don't speak French.

## Overview

All according to my understanding. Spokil is a quite agglutinating language (yay! at least as it seems to be done here), with both a priori and a posteriori parts, it seems.

### Letters and pronunciation

Spokil has 21 letters:

  aeiou

  bgdvjzrm
  pktfhsln

**h** and **j** is pronounced like Esperanto ŝ (!) and ĵ.

### POS markers

**-a** – noun  
**-e** – infinitive verb  
**-i** – prepositions and conjunctions  
**-o** – adjective  
**-u** – adverb

### Articles and plural

Interestingly, Spokil has articles botrh for definate and indefinate, singular and plural. The plural marker **-s** can be attached to nouns, but are more commonly attached only to the articles.

* indefinate (s., pl.): ''ne, nes''
* definate (s., pl.): ''le, les''

Is it possible to mix up these? Yes, definately. Maybe it's possible to leave the indefinate just unmarked (or the other way around), and ditto with the indefinate plural, possibly (generally, so that one can use _ne_ and _nes_ too when needed).
