swe i kwebose tuli e oPute, sya [a] oPute i sibu heyswa kwedoy, sun botegeba-byasogya swehey'. polu taw (i swa) e doy. hapa i lako e tay doy sogya pakagibo [crown], sya [a] pasa i hamu e [appear like] degwa.

sya oPute i botilusi: ”kya mohu moybay i tay tey hay e haytako [baskets], moybay i swa e guse kwetasay?”
bemyu bya(kin) i pulisimi [whistle] boy' swi kikaw-luta. moybay i (lase) seni [swing] e haytako teli kwegyu – toy pene dwi lase tapi moybay e deki.

dyu san oPute toy byakin [by his side] i sami byo doy kamu dego mite pana-kikaw' [palmtrees!] kya bili [swayed] boy keho.
lon moyswe i bani e doy kisa, pene lon/sopi oPute i heyswa e buse [pale] boy deha, kile teso [even given the fact that] moyswe i polulon gwimohu [politely] bamu.

dyu kwekumye (lon), [a] moyswe i buli boy' kiwe tihopla [lush] kikawdin sya botegeba-(bya)sogya i solegyu: ”gwibuli/bulibam~i~u boy hoybay sogyadego/sogyadego hoy bay!”
bemy~o~u oPute i pokebey 'badeludi/doy~toso~ludi e: ”moy bote pata i gapi boy kikaw, [a] bay i mye kwepolulon loy e mey sungoy!”

”gwi [a] oPute, kyu i (gabi)kweteydalu [stupid], kay a kyu i kwe mapi teso a swe e geba! bemyu may~'/pinsibu/pindin~ a moybay i GULLIGHET-gwa hoy bay!”
swe i tosoludi e: ”i buli e hoyboy peke GULL-byamikya!” – sye gye oPute a peke botegeba-byamikya i tagi i kwepay e pay toga [stand, and bow]!

sya a botegeba-sogya i solegyu [smile]: ”(~ONI~man) ~suke'~ i [suke] (hey)loy doke kwelaw e~teso~ hoy kya daw a pin i tey swa. may, (a) oPute, a kya i dya kweday – i swa lase, a hoybay bote mikya [be fast, my blue children]!”
Och blåbärskungen belåtet ler: »man säkert på utanskriften ser, hvems pojkar det här kan vara. Nu, Putte, ska’ du dina blåbär få, er raska nu, mina gossar blå, ni bruka ju vara snara!»

sya may a moygoy i tay dwi lase sopi: a moyswe i payplu i syubili i piki, sya dyu kwekumye a hay~dun~tako i swa tay/plabey pla.
moyswe i doyludi e ”i buli i sesey e moybay”, e ”kyu i dyu loy e tay moy tamye goy”. sya a oPute i swa sibu hey'dobu'.

sya a oPute i heytewi' tubyo byo doy sugyo. tubyo e kikawtela [bark] sya a kehotiho [sails] e kikawtiho (doke o Acer). benu, kwepulolon a swe i ~heytamye~ mye heytamye hoy san dalu.
sansibu/sandipu a swe i bitye bomi e bay geba: ”bemyu kyadin a swe i dyu seki lake-geba byo pindin? pindin e kwe kupe satobey, kay kwe?” [here is not sunny enough, is it not?]

”bemyu i meti, a oPute. kyu i dyu loy. may a moybay i temu' (e) lakegeba-mawmya, a swe i dyu suke day e lakegeba uKOLLA kyu! kyu i subi dole lade kwekumye sibu, a ~bemyu~moybay i dyu basi e pana e maba, i syu dita (kulu)sato'.”