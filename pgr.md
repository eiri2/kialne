## Plu Glosa Re

I just realised why – maybe – English use camel casing in titles. Because of the articles – it might feel a bit weird, I'm thinking, if only the first word in a title gets capitalized, if that word is often an article. That's less common in say Swedish, which also doesn't use camel casing (as a norm, some people do of course (and yes, it's ugly)). I had that experience just now with Glosa.

But anyway. There should be a place for stuff in Glosa too, (in case the constant logouts from Gitlab would get resolved, at least) (bitter remark) (sorry about that), of course. Here it is.

Even better, actually, would be if I had like a kitchen zink file, where I could generally put stuff, without spreading it out before a more mature stage when a separate file is more warranted. But we'll see. Often I half-and-half use some files for that, files that I happen to work on right than. Maybe this file could turn into that just in general.

So far no Glosa. We'll see when I next time 1) check Gitlab 2) is logged in, or 3) take the time to log in.

## Hedo de piske
Zhuangzi e Huizi pa ambula epi u ponti supra u fluvi Hao, tem u pe mo nota: ”Vide qo-modo' plu piski<!--minnows--> sagita' inter/meso plu petro! Tali/U-la es u hedo de piski!”

”Tu no es u piski”, Huizi pa dice, ”ex qo tu posi pote logi; (plu) piski es hedo?<!--order? alt. u hedo de piski.-->”

”E tu no es mi”, Zhuangzi pa dice, ”qo-mode tu pote logi; mi no logi qi sti piski hedo/qi plu   piski es hedo?”

”Postula mi, (qi) no es tu, no pote logi qi tu logi”, Huizi reakti, ”qe no seqe ex u-la; tu, (qi) no es piske, no pote logi qi face piske hedo?”

”Lase' na versi a lo qi' na pa proto. Tem tu pa dice ’_ex qo_ tu posi pote logi; plu piske es hedo?’, id pa monstra<!--free addition based on first translation and maybe other I saw somewhere-->; tu jam logi; mi pa logi. <!--E-->Mi pa logi id ex supra u Hao.”

* https://paper-republic.org/pers/lucas-klein/anarchist-anthropology-happy-fish-and-translation
* https://pages.ucsd.edu/~dkjordan/chin/LaoJuang/JoyOfFishe.html

## U papilio-sonia

Mo kron, Zhuang Zhou pa sonia; an pa es papilio, u papilio qi tremulo a-ci e a-la<!--nyord-->, hedo de auto e akti iso an volu<!--place-->. An no ski si an pa es Zhuang Zhou qi pa sonia; an pa es u papilio, alo u papilio qi sonia an es Zhuang Zhou. Inter Zhuang Zhou e u papilio nece es uno klari'/difere<!--distinction-->. U-ci es ge-nomi u muta de plu re.

<!--Manmino: It tahi', Z.Z. mong-le; ta <butterfly> (toli=bird) cey-le', <butterfly>-->

* https://www2.hawaii.edu/~freeman/courses/phil494/10.%20Zhuangzi%27s%20Butterfly%20Dream.pdf
* "etymology of meng", also interesting

**General thoughts:** _It looks to me as if "volu dona auxi" and "volu gene auxi" is swapped/switched around in the dictionary (#error). First I thought I didn't understand the English phrase "want help" correctly (but both seem to be kinda flipped)._

## What I've learned about Manmino

**SOV** word order – not as difficult as it sounds, I think it's key to simply start phrasing things in your native/other familiar language in this order while translating, if excersizing by translation, to get into it. Like, it's half impossible otherwise ("'I'.. – what's the verb? – 'read' – now let's get back to the object.." vs. "'I it.. read.' – 'I read it.'").

Negation is done by the prefix **but-** added to the verb (S O but-V). iirc, this might be the only prefix in the language. There are other suffixes, though.

Tense is done with suffixes (on the verb): **-le** is past tense, **-kalu** future tense. **a co but-ken-le** – I didn't see it.

The adpositions (known as prepositions if they come before the noun) are simply suffixes, that are put on the end of the nouns.

Most, if not all of this, thanks to the excellent course by Trosel. (I put it into a sent slideshow (or rather, some, in plural), used as flash cards.)

## What I've learned about Lidepla

### This is rather notes on grammar

that tries to be clearer and briefer.

#### Stress
* On **last vowel before consonant or y**, or in (C)CVV the **next to last vowel**, except:
  * **-en, -us, -um, -er** endings and **-ik-** and **-ul-** suffixes are unstressed, except compounds with the suffix **-fula**
  * u is not stressed in au or eu
  * words with doubled and regular vowels, where the doubled ones mark stress (stress is quantitative, so afaiu a long vowel is a stressed vowel) (words with one pair of doubled vowels and no other vowels are not considered stressed)

* Compounding, including with suffixes such as plural **-es/-s**, adverbial **-em**, and noun **-ing**, doesn't affect the stress, except:
  * **-isi, -ifi, -inka, -ina, -ista, -(t)ive, -ale, -are** and **-(i)taa**
  * suffixes beginning with a consonant ”may recieve a secondary stress” (some of them, most of them, or all of them? mandatory or not?), including **lik, shil, tul,** and **bile** (~-like?, ~inlined?, tool?, -ble?)

This is not fully regular, so you don't need to keep it in mind unless you see a clear use for it (maybe the last one is a bit more useful):
* Nouns often end in **-a**
* Verbs often end in **-i**
* Adjectives often end in **-e**
* Adverbs related to **-e** adjectives end in **-em**

#### Tenses
* **he** – have (as tense marker), completed action
* **-te** – general past tense (including completed action)
* **zai** – continous aspect, -ing – non-mandatory

#### Useful words
* **mah** – make (causative particle)
* **bu** – not

## Felisitaa de fishes

Zhuangzi e Huizi zai promeni-te on ponta sobre riva Hao', al' un-ney de li observi-te: ”Vidi komo fish fliti inter rokas! Tal es felisitaa de fishes.”

”Yu bu zai es fish” Huizi shwo, ”fon kwo yu jan ke fish es felise?”

”E yu bu zai es me” Zhuangzi shwo, ”komo yu mog jan ke me bu jan kwo mah* fishes felisitaa?”

”Si me, bu zai es yu, bu mog jan kwo yu jan” Huizi jawabi, ”ob (it)' bu sewki de toy same fakta, ke yu, bu zai es fishe, bu mog jan kwo mah fishes felise?”

”Nu returni ba a wo nu starti.<!--originale' confusing in tiddlydict/dict--> Wen yu shwo ’fon kwo yu jan ke fish es felise’, yu diki ke yu jan-te ke me jan-te. E me jan-te it fon sobra (riva) Hao.”

**General thoughts and notes:** Glosa has several inter, Lidepla has one.

I have lately thought that I should, and also done it, focus more on auxlangs I just feel like fiddling with, rather than those that cerebrally seems good theoretically. Sometimes it coincides, of course, and I might not really have exclusively focused on theory, at least not often, and I suppose originally I might have focused on what feels interesting/good, more unknowingly, and that I've iterated between the two. Maybe I'll iterate again – maybe the best would be the combination of the two ;) – and maybe I'll rephrase this some day, because I don't think I managed to express it quite well, but at any rate, I just found it genously/good, and wanted to express the geniality of it. Reasons, I'm not quite clear on right now, but I'm doing it as a hobby and for fun, and also, what I subjectively like might be what others subjectively like – not sure if these are the actual most important reasons.

Anyway. I was going to say that Lidepla, feeling-wise, is a bit of a hotchpotch (?), a mix that doesn't quite feel smooth or coherent all the time (a bit collage-like, in a negative feeling-way) – that was the feeling right now, that is – and I've always felt like the documentation and everything generally (not all of it) is a mess (also just formatting wise) (and there is no simple, clear and brief introduction – ”this thing is generally done like this. Except this, and except this, but not in these cases.”).

Unrethorically now, I mentioned the negatives first, but at the same time I have to say I find Lidepla pretty and nice looking (and sounding, probably). So all-in-all, I don't know where I land feeling-wise on it.

It seems Lidepla maybe has a feminine suffix but not a male one? Hm: I don't quite like that, but haven't checked it up. Update: no, it does have both a male suffix and female suffix (-o and -ina) – the male one is just shorter. Oh well, that's alright I guess.

Seems like tense marking generally is mandatory. I'm spoiled by Mundeze. 🙂

Bu is a nice word that I know since lon, but at least in case someone else reads this, it sure belongs in useful words.<!--
Zhuangzi and Huizi were strolling on a bridge over the River Hao, when the former observed, “See how the minnows dart between the rocks! Such is the happiness of fishes.”
“You not being a fish,” said Huizi, “how can you possibly know what makes fish happy?”
“And you not being I,” said Zhuangzi, “how can you know that I don’t know what makes fish happy?”
“If I, not being you, cannot know what you know,” replied Huizi, “does it not follow from that very fact that you, not being a fish, cannot know what makes fish happy?”
“Let us go back,” said Zhuangzi, “to your original question. You asked me how I knew what makes fish happy. The very fact you asked shows that you knew I knew—as I did know, from my own feelings on this bridge.”
‘Whence do you know that the fish are happy?’ And the final line: ‘Let’s go back to where we started. When you said “Whence do you know that the fish are happy?”, you asked me the question already knowing that knowing that I knew. I knew it from up above the Hao.’ 
-->