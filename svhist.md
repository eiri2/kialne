# Sveriges historia

Historia är egentligen forskningen kring dokument och liknande – historiska källor – och vad man kan dra för slutsatser från dem. Tiden innan de äldsta historiska källorna kallas förhistoria eller forntid (och tiden från vilken det finns legender och nedtecknade mer eller mindre
otillförlitliga berättelser protohistoria). De äldsta svenska källorna som finns kvar är från omkring 900-tal (runstenar) till 1100-tal (handskrifter), och med dem börjar historisk tid i Sverige.

Denna artikel går även igenom förhistorisk tid.

## Äldre stenålder
## Yngre stenålder
## Bronsåldern
## Järnåldern
### Vikingatid
Historiska epoker har inte ett och samma startdatum överallt, och inte alla når hela världen. Man kan säga att det fortfarande idag finns grupper som fortfarande inte inträtt i Yngre stenåldern ännu (de lever fortfarande renodlat som jägar-samlare och har inte blivit bofasta), och de som fortfarande lever i former som under Yngre stenåldern (som enkla jordbrukare, utan metallproduktion – eller nyttjar kanske även de primitivaste jordbrukarna idag i djunglerna metallföremål? Svårt att säga.).
## Medeltid
Medeltiden räknas i Sverige från vikingatidens slut 1066, fram till att Gustav Vasa avsätter Kristian II, och därmed slutgiltigt upphäver Kalmarunionen år 1521 – med andra ord, Sverige accepterar efter detta ingen mer unionskung (och jag vet inte om Danmark-Norge längre ville försöka få med Sverige på tåget). Med Gustav Vasa kommer också reformationen, då Sverige går över från katolicism till protestantism, med målsättningen att göra kronan – kungamakten – starkare och få in mer pengar till kronan, och göra maktpositionen säkrare.

### 1200-tal
* Någon gång vid denna tid kan Sverige ha börjat erövra Finland, och efter en tids expansion är Östlandet¿, som man brukade kalla landsdelen förr i tiden, del av Sverige i många hundra år framöver. Legenderna talar om korståg mot Finland, som man inte tror har ägt rum i den formen på riktigt. Sveriges östra grannland blir Ryssland och dess föregångare.

* Arkeologiska lämningar på Öland indikerar att gårdar vid denna tid övergivs, och en hypotes är att de flyttat till Estlands skärgård som del av en svensk politik att försöka kolonisera områden österut – jämför införlivandet av dagens Finland ovan. Dessa kolonisatörer blir estlandssvenskarna, och Estland blir mycket riktigt _senare_ under en period svenskt – estlandssvenskarna, som fortsätter att tala svenska på de stora estländska öarna fram tills de flesta tvingas fly eller flytta under Stalintiden (1920-tal–1950-tal). Under Katarina den Stora på 1700-talet förflyttas en grupp till Ukraina, till det som idag heter Gammelsvenskby. Idag finns få om ens några estlandssvenskar kvar i Estland. Många migrerade runt tiden för andra världskriget till Sverige.

* Birger jarl, som egentligen tillhör en familj som har stått på folkungarnas sida, krossar folkungarna och inbördeskriget i Sverige, och enar Sverige med järnhand. Till och med de medeltida källorna förfasas över hans grymma och svekfulla handling när han lovar sina fiender amnesti och fri lejd¿, och sedan låter avrätta dem. Detta i en tid när det var av yttersta vikt att kunna visa att man höll sitt ord, då ryktet var ett viktigt medel för att kunna leva och verka i samhället som det fungerade då – man var så att säga tvungen att kunna lita på varandra.

### 1300-tal
* Kalmarunionen grundas av drottning Margareta 1389.

## Nya tiden
### Upplysningen
* 1793: en grupp adelsmän i komplott mördar Gustav III.
### Industrialismen eller moderniteten
Modern kommer av latinets modus, som betyder förändring eller föränderlighet, något i den stilen, och moderniteten är den epok som kännetecknas av förändring – förändring och självreflexivitet blir kännetecknande som epokens särdrag, i och med att sociala-samhälleliga mekanismer och idéer uppkommer som resulterar i ett fruktsamt men också obönhörligt förändrande.

* tidigt 1800-tal: Sverige förlorar halva sitt territorie i krig, till Ryssland, och området blir nu Storfurstendömet Finland under ryske tsaren (1809?). Landet, åtminstone de samhällsklasser som har möjlighet att bry sig, sägs ofta ha varit i chock. Inte mycket finns nu kvar av makten och stoltheten som man får tänka sig att stormaktstiden i någon mån innebar.

* 1810-tal: senaste kriget i Sverige, när Danmark går in med en norsk här under Napoleonkrigen (1812?). Kriget blir inte långvarigt, och invasionen avstyrs snart. Sverige är ett av de länder i världen som har haft fred längst, nu en bra bit över 200 år.

* 1906: unionen mellan Sverige och Norge upphör efter en folkomröstning i Norge (?). Oscar den II dör inte långt därefter, och Gustav V blir kung.

#### Mellankrigstiden
#### Efterkrigstiden och kalla kriget
