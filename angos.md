## Angos

Angos is a nice auxlang, a simple and neutral language, that I have looked into some times, but it's been a while and right now I'm a bit rusty. Let's explore it together!

For some basics first, Angos has part-of-speech (POS) ~~markers~~ word endings (like Esperanto), so words ending in **o** are nouns, **a** verbs, **i** adjectives, and **u** adverbs (unlike in Esperanto). Most (all?) 'function words' end in **e.**

Plural is marked with **le.** Let's see if it's mandatory. It's not, good ('if number can be understood through context')! Should we understand that qoute as it being a little bit mandatory – mandatory to hint every time something is not plural? It's a bit different from completely 'a-numeral' words if so, and I think it's better if the roots are note seen as singular-by-default. So far we are free to interpret it as this not being the case, I'm thinking. [edit: I'd say this is now confirmed, page, uh, Section 3 → Nouns, in the grammar.]

Also, as far as I remember, roots are taken to a large degree from 'medium-sized' and minority languages, so for instance Native American languages, but also Japanese. Phonoaesthetics (?) or the (look and) sound of it, at least, is a big thing in Angos, so I guess a factor in word choice has maybe been what looks nice, or fits the phonology. I think all-in-all it ~~creates~~ makes Angos vocab nearly a priori-level neutral, from my perspective and in my experience, but ymmv, I guess.

THere are two odd things in Angos. The first one is included in the name of the language itself, and it's the **s** ending.¹ **s** distinguishes something man-made (artefact) from something natural, and fills a role of reducing the number of needed roots. I only remembered it being used with nouns, and that might be the most common thing, but apparently it can be applied to all word classes (save function words, afaiu). THe word **angos** thus means simply man-made/artificial language, and **ango** is a natural language. **leiso** is shelter, **leisos** is 'house, building, bunker', webo is web, network, webos internet, and an interesting one (in the puzzling/not-neccessarily-good way) I didn't know about was **olo** – thing vs. **olos** – tool. Like, strictly speaking, a thing is of course not a natural tool, and a tool is not just any man made thing, but I think this might not be a problem at all really, an ‘ugly’ (?) hack that might be very functional. The usage of **s** might be sometimes a bit 'poetic' and not strictly locigal, then.

The other odd thing is how you say you have something in Angos – expressing ownership. This is just a bit quirky, but maybe there are many models for it? To say ‘I have a cat’, you ~~simply~~ say ‘At/to me, there is a cat’, or alternatively I see now ‘There is a cat to me’. Not difficult at all, but a bit quirky? Fine with me, though.

The third odd thing about Angos – I did say three, didn't I? – is that (honestly I forgot). Oh, yes. Angos is what you could call noun-centered. Every root (except function words, right?) is a noun in it's basic meaning (judging from what I've heard about Ido development and also Esperanto, not knowing the fundamental 'nature' of a root can cause a mess), then adjectives and verbs and so on are derived from this. I think I might have read in the old grammar about an epiphany about nouns being core as words, that using nouns you could express most things, that the creator had during recurring daily lattes with a friend.

That might be all peculiarities (the vocabulary basis could be counted as a number four) that you need to know about Angos, if you are familiar with generalities of simple auxlangs (or auxlangs generally). Let's move on to a general, basic overview.

Oh, I have to say I've personally found Angos easier to learn and study, vocabulary wise (and also in relation to some syntax stuff, that's a bit hard to ficure out in Kah, in parts due to the documentation, in parts due to unintuitiveness, for my part)

Do note, though, that **Angos vocabulary unfortunately is in an unfinished state,** and it's not rare at all to encounter words that are missing (well, you get what I mean, to want to speak about a concept or need a word that there is no Angos translation for yet). In some cases this can be solved with creativity, and verbs being so to speak "any action related to the noun root that makes sense in context" also helps with that, but you would need to fill in blanks sometimes, for instance <ĉi tiel> or <like this>, using another language (Esperanto nouns at least fit fairly well, Japanese might be another option – or ofc otherwise English – but it's neat with something more neutral, and yes, I'd say that Esperanto and Japanese both are more neutral than English actually – in the case of the latter, a very small percentage of the world population will be advantaged.)

----
¹ effectively a 6th ending besides o, a, i, u and e, unless you count all combinations separately

## Basics
* Word order is SVO.
  * modifiers come before the modified word (head final, right?) – adverbs goes before the verbs, adjectives before the nouns.
* Nouns end in o, verbs in a, adjectives in i, adverbs in u – function words ('particles'?) in e, and man-made things take the s ending after that.
* Words are stressed on the next to last syllable (right? With POS markers forming syllables, on their own if after vowel, with consonant if immedeatly after consonant): _pa-NI-o, is-TI-nu._

## Reading the grammar
"the meaning of a kod-kalimo (verb) depends on the context of the noun root used" – another oddity! I have seen this before.

(Also names can take all the main POS endings.)

Plus points in my book for the word **kwio** (which I'll interpret a bit freely).

It goes without saying that there is no verb conjugation. Future tense is **ke,** past tense is **me,** imperative is just the verb without subject (optionally with _nae_ before it). The adverb **inu** before the verb shows reflexivity.

It's funny how tae is a particle and not just a verb. Flows well, I think, though.

----
hus talu ie
