**Mundeze** is a _very_ regular constructed language, designed and aimed (aiming) to be an auxiliary language, utilizing word creation with available roots a lot, onomatopoeia, and having POS markers.

It was a while ago now that I last used it, so I'm a bit rusty.

Imperative (or some similar grammatical function) is made by emphasizing/intonating the last vowel of the verb, which is an i – _peli_ – to speak, _pelí_ – speak!

Plural is non-mandatory – you don't need to mark it, and often that is clear from context – but if you want, well, first of all you can use some number word such as _many, two, some_ etc, but you can also use the optional plural suffix **-y.** Apart from the POS markers, this is the only suffix in which tells something grammatical-ish (at least) (iirc – there is this compounding, so what counts as a suffix or not imo becomes a matter of subjective definition, I guess, but plural and POS markers are all one-letter ones).

There is no definiteness marker in Mundeze, yay! None whatsoever (iirc). No thinking about whether to add a la or something like that – sure, with a germanic native language (with definiteness suffix, mainly), it's generally intiutive when it feels like it makes sense (but some usage is idiomatic and differs between languages), but it's still liberating to not have to sprinkle your texts (and, uh, speech) with definiteness markers, I mean not only in getting a (clean and neat) text without that repetition, but mainly just that it feels so swift.

### Day 2

It has been a while since I wrote the above, so I'm even more rusty, and I also don't know how many days I wrote the text in, but let's just call this day 2, anyhow.

I'm thinking some translations and texts would definitely be good. That way I would also de-rust my knowledge a bit. 🙂

### Day 3

Mundeze is quite intricate in it's way of utilizing grammatical structure to convey detailed meanings (did that phrase make sense?) – or maybe, it makes detailed usage of grammatical structures. This is a strength, but it could clearly be a thing that might make the language less beneficial for someone for whom an adjective is a completely alien concept.

For instance, iirc genitive "of" can be (or is?) expressed with the adjective ending _a_ used as a particle. This sort of makes sense to me – speaking as an auxlang nerd – in the vein, I suppose, that pronoun+adjective ending in an auxlang makes sense for genitive – the me-bed → my bed. If I'm right about the usage, it means that the _a_ marks that what follows is in adjectival relation to the previously mentioned thing or agent, giving us things like a John-ish pie → Johns pie (this all became fully clear to me only now as I wrote about it). That also makes sense, thinking about it (and also made sense-ish before deeper reflection too).

But for someone who barely understand what an adjective is, to say "to say 'of' in the sense that someone owns something, you put the adjective ending before the owner" or things like that – that could turn out confusing, and takes some more extensive explaining, at least, I'm thinking.

––––

Wow, "so, then, thus, therefore" is the same word as "because, for", but turned the other way around (sa, as)!

## Overview

* very simple and regular
* words often formed by combining more basic roots, and sometimes onomatopoeia
* POS word endings:
  * **e** – noun (**ey** optional plural)
  * **i** – verb (**í** optional? imperative)
  * **a** – adjective
  * **o** – adverb
* word order is always **SV** – either SVO or OSV.
  * head final (which means that nouns and verbs come before adjectives and adverbs).
  * words or part of a sentence can be ommitted if the meaning is clear from context.
* no conjugation or nonsense like that ;). tense and aspect are expressed with markers.

### common and useful words
* **ha** – yes
* **ne** – no, not
* **ya** – some, any
* **tia** – that
* **titia** – this
* **nyami** – eat
* **vrume** – car
* **os** – at, when, during, in [time]
* **nio** – also (compare **ni** – and)

### verb markers
tense and aspect are expressed using the following markers that are put before the verb (there are also longer forms of the tense markers):

* **preo** – past marker
* **nuo** – present marker
* **poso** – future marker<br /><br />
* **jo** – completed action marker
* **so** – ongoing action marker
* **vo** – planned action marker

### conjunctions
* **ni** – and
* **u** – or (**u X u Y** – either X or)
* **may** – but, whereas, while
* **sa** – so, then, thus, so that, therefore
* **as** – because, for
* **nio** – also (conjunction or not?)
* **nio ne** – neither, nor (**nio ne X, nio ne Y** – neither X, nor Y)

### pronouns
1)  **me** (I, me) – **noy** (we, us)
2)  **tu** (you) – **voy** (you)
3)  **lo** (he, she, him, her, it) – **ley** (they, them)

indefinite personal pronoun: 
**ane** – one, they, you.
possesive pronouns:
**mea, tua, loa, noya, voya** and **loya.**


### Translations and texts
#### nevateye' a blua djowelkane<!--carbuncle, ej dyamante-->

me ringi dore<!--(dindone)--> a? _Sherlock Holmes_ os bia die posos natale, navoli<!--kon navole a--> speri nas' lo lobe a este<!--compliments o t season-->. lo lejesi sop 'sofa' in purpura/vyola longice, kon 'pipe-rack' in loa nastuke laten loa reste, ni multome a' plega' 'newspapers', oydo<!--gumentceseblo--> deekso lodjia [FORTS].

He was lounging upon the sofa in a purple dressing-gown, a pipe-rack within his reach upon the right, and a pile of crumpled morning papers, evidently newly studied, near at hand. Beside the couch was a wooden chair, and on the angle of the back hung a very seedy and disreputable hard felt hat, much the worse for wear, and cracked in several places. A lens and a forceps lying upon the seat of the chair suggested that the hat had been suspended in this manner for the purpose of examination. 
