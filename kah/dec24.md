### <!--Pinja-->Valu fanka la meo

**Tauku wi, kil kwi kwaranu eno baska tongono eno rutum uferu 'okwaza na yunyo, mao.**  
<!--Ago long, mouse PAST gather in_order_to decide measure in_order_to overcome enemy share-d of they, cat.-->

**'Uwi ka awau, uwi ka aye – esto zebas kil sita mora ai ka en yu le ajepau en yu je ke' mondoi doi.**  
<!--Some say this, some say that – but at last mouse young get_up [?] and say that he have proposal that he think would [?] solve problem.-->

**”Linyo yo (ke) jelu”, yu ka ”en izen tiku na wanyo weron ashy siskom/mweno ai kompu en uferu za (eno faunu wanyo). Tolo, tonto (wanyo) tengi choza tancho kakwa na 'afaunu na yu, (tontoye wanyo) tengi churu gejole chu yu. Echu, wa jepau en jale fanka sin, ai valu ya za nongo la kenke na meo. Shau', wanyo denu/ke jo hata' yu faunu/de/CONTINOUS_la, ai (ke) tengi nuze gejole tabo yu fafau.”**  
<!--”You all (would) agree”, he say ”that danger chief of us consist_of manner cunning/subtle and deceitful [sly and treacherous] that enemy use (in_order_to approach us). Now, if (we) can receive signal any [some signal] of approach of her, (then we) can escape easily from her. Therefore, I propose that aquire bell small, and attach it using ribbon at neck of cat. Like_this, we will/would know when [?] she approach/come/CONT_at, and (would) can retreat easily while she be_close.”-->

**Ajepau wau choza achechese sisanto', deche kil naita mora ai ka: ”Aye yo nen, esto hayu valu fanka la meo?”. Kil yun noyom' ai yuyung kaza.**
<!--Proposal this receive applause in_general, until mouse old get up and say: ”This all good, but who attach bell at cat?”. Mice look each_other and nobody spoke.-->


jekil – kil

**Tai kil naita ka: ”Wivang be jepau amumong teneng.”**  
<!--Then mouse old say: ”be_easy TOP propose remedies [measures, solutions?] impossible.”-->


put sth on sth/smby
wikuta→kwikuta – kolla
nokanyo = council
plural?
share-d
would, could → in many situations in simpler languages, you do not use this grammatical form – often only when it's something explicitly conditional or imagined

o- derives a stative verb (ominza – stolen, omomi – cured, okai – to be closed) – often -ed forms in english, I'm thinking

stilistiken i "wa jepau en", sedan utan pronomen.

## A quick Kah course

Is Kah optimal/an optimal oligosynthetic a priori language? Well, I'm not really sure, but it is also possible~~ – both that it is not, and that it is~~.^(unclear derivation and related words being different are two critiques I've found/I have so far, but that's not for this text – the creator has been abscent from the project for several years, so they cannot be asked)

Anyway, if you are curious of Kah, you can learn the basics/the basics of it's vocabulary here!

### I, you, he and she

Let's look at our first three Kah words:

* **wa** – **li** – **yu**

You might have guessed already that these three words are the singular pronouns meaning I, you, and he/she/they (used about individual people and animals), respectively – all referring to living beings.

To these, there is actually also a fourth one: **ya** – it – used about things and abstractions (inanimate).

Note that the beings-word ends in **u** and the thing word ends in **a**. These letters are often used as prefixes, to form other beings- and thing-words , as you will see later.

### And but

Our next two words are:

* **ai** – and
* **esto** – but

Another similar word is **eom** – meaning or.

Now you know how to say ’me and you’, ’you and she’, and ’he or me’!

### Not just no

Let's continue with **nong** and **eo**. Nong means no and not, and eo means yes.

Now you can say ’yes, me and you’, and ’you but not she’.

### Some more

If I tell you that **wanyo** means we, you can probably already tell what **linyo** and **yunyo** means? That's right, that's the plural pronouns ’you all’ ~(plural you)~ and ’they’.

There's no object forms in Kah, so ’I see him’ and ’she sees me’ is just **wa wayun yu** and **yu wayun wa**.

### la, chu, de

Another very frequent word in Kah is **la**. It means ’be at, at’, and just like the English word ’at’ (’at’ is rather odd to me, really), it seems to be used both about time and space. And not only that, but it has a wide meaning, seemingly being used as a ”general preposition”, for things like ’in, by, at, on’.

Other common ”space words” are **chu** and **de**. Chu means from (_achu_ means source), and the ~very~ frequent word de means both ’come, arrive at’ and ’go’. This might take a bit of getting used to, but is of course not complicated.

### bo and his friend
Speaking of la, a more explicit way to say ’in’ is **bo**, which only means ’in’. A related useful (but less frequent) word is **tabo** (time+in), meaning ’while, during’.

och så ka, jo, je, weyun? (10 units of Kah words, kanske?) (Kah crasch course.)

wau, ye – awau, aye – uwau, uye.

tengi.
kaza, kanske?

kwi, denu. (andra saken bör lära sig, kanske?) (eller senare.)

the dictionary says **kwi** means ’already’, but don't be fooled by that – or well, it's true, but one should know that this is the standard past tense marker in Kah.

tense marking is however not mandatory in Kah, and is only necessary when it's needed for clarification/when the context doesn't ~tell~show the tense.FORMULERA-OM

yu – ha – hayu

och så continous, finns nog?

Inga artiklar, inget plural. This, that, plural, many, numerals etc can be used when clarification is needed/when it's needed for clarity.

N A, eller head-modifier. (och så vad omvänt betyder.)

Stacking verbs? att kan bara sätta dem en efter en på sätt som känns naturligt sedan. Se exempel/Exempel i ordlista ofta användbara.

Som-konstruktioner.

Pro-drop, jag använder sällan.

----
amaika.

to be close/near? fafau, kanske.

tonen som unbearable british aristocrats, lite störande, i sammanhanget iaf.

hur formar man adverb? ordföljd verkar inte vara super-strikt, _kan_ markera toppflyttat ord med be? om det behövs, tror jag det stod.



determine, eller vilket ord det var i studie.
