## A possible excursion into Lidepla

**Lingwa de Planeta** (Lidepla) is an auxlang with a rather nice sound. Finished in 2011/2012, or something like that, it's an early worldlang (at least in the wave of worldlangs, starting in the 2000s), that gained some traction (other mentionworthy early ones are Sasxsek, Neo Patwa and of course Pandunia – which at least early on was a succession of a few different projects with the same name). It also has reggae music. And cute childrens stories videos.

And messy documentation. It's been spread over several webpages, in various documents, in non-optimal formatting (messy text formatting, sorry). There's one great interactive presentation, though (in the form of presentation media), and someone [space for credit if wanted] has actually made a website with a good-formatted presentation of the grammar.

The grammar itself I've often (i.e. the times that I've looked into it) found a bit messy too. It has redundant features (two way of expressing genitive pronouns, it seems, for instance), and is not super systematical – there's a lot of "adjectives are often formed like this, but not always" – meaning that generally one has to learn or use it like one would a simple/simplified natural language. That has often been an obstacle to me in my aquaintance with the language, with me expecting more systematicity, but that might be part on me. And, the language is said to be Novial-inspired, btw – another interesting auxlang, one of the classics.

The language mixes among other sources Chinese and English roots (and Russian, Arabic etc, iirc – just emphasizing these to explain my impression of the language) into a quite nice looking/sounding blend.

### Day 2
So, I got the impulse when (before) first posting this file to explore Lidepla a bit. I guess I'll keep notes up here, and add grammar ~~notes~~ facts in it's own section – a summary/an overview – below.

Oh, now I remember, I thought after writing the first part of this file, that I should also mention that it's got the very nice publication _Jiva,_ with a nice design, published during some years. Even the name itself (meaning life), is nice.

ti – ni in the grammar is slightly complicated to understand (the construction itself, how to phrase things, the word order of that construction), and the verb tense-aspect system is a bit complex, because it has two options for expressing ~~many (all?)~~ [one] of the tenses/aspects. Then there are rather precise tense/aspect distinctions too, which feels a bit Novialish (not specifically it's tense/aspect system, but general preciseness) (and Novial is often mentioned as an inspiration/basis for Lidepla).

How to structure that in the overview? Maybe not include it all?

Okay, it's actually only past tense that has two options. The passive participle/past active participle marker rings a Novial bell too.

### Grammar
* **SVO** word order (or subject – predicate – object).
  * **den** marks an object not in it's usual place (comes before the object)
  * **da** marks a subject not in it's usual place (before the subject)
    * somewhat special cases with kom and kem (refer to the grammar page)<!--: * **kom den yu** – as X Y you * **kom da yu** – as you Y X-->
* questions:
  * interrogative particles, use either of:
    * **ob** at the start of senence/clause,
    * **ku** at the end of it.
  * repeat the verb:
    * **(subject) verb-bu-verb (object)** – (subject) verb-or not-verb (object)?
  * **bu ver?** – isn't it?
  * **kwo, kwel, wo** (etc?) question words
    * kwo and kwel can be put at start of sentences

* multiple negations does not cancel each other:
  * **me bu vidi nixa** – I don't see anything [lit. nothing]

#### Verb tense and aspect
Preceeding particles:
* Tense:
  * <!--distant-->remote past – past – immediate past – continous – immedeate future – future
  * **gwo – he – yus – zai – sal – ve**

* Other:
  * conditional (would) – passive (of becoming – becomes) – while [verb]
  * **wud – gei – al**

Affixes:
* past tense · present active participle · verbal adverb · passive participle/past active participle
* **-te · -she · -yen · -ney**

* (Some verbs are combinations of two words with hyphen between them, and many of the verbs end in -i.)
