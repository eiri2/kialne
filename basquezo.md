You've seen the PIE mundezido/esperantido and, uh, other things. Now it's time for the Basque mundez-esperantido!

I haven't updated the PIE/Pangea project for a little while, and one reason is that word collecting is either cumbersome or will give you outdated roots – a bit bothering – and the rest of the reasons is probably just randomness.

So, time for Euska, or something like that. I hope, or, I presumably think that finding okay translations for things shouldn't be as hard. Iirc, Basque grammar is rather analytical/agglutinating and probably ill suited for an esperantumido.
