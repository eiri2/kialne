## Vitaminer och varningar

**Vitaminer, kosttillskott och varningar** är inte bara varningar för en del kosttillskott och vitaminer, utan i första hand ett försök till en översikt över vad jag lärt mig och erfarit kring att läsa om och prova olika preparat. Mina problem är framförallt ångest, sömnproblematik och oro, och det är alltså det jag huvudsakligen varit inriktad på i mitt (försiktiga!) experimenterande.

Jag har också märkt att det finns så många sorglösa, halsbrytande och i vissa fall rent farliga rekommendationer kring vitaminer och kosttillskott. Jag vill därför försöka sprida lite medvetenhet och kunskap om faror med kosttillskott. Vissa ämnen är relativt ofarliga, andra är väldigt, väldigt skadliga och riskfyllda att inta som tillskott. Det säger sig självt att ämnen som påverkar kroppens funktion inte på något magiskt sätt har inbyggda spärrar mot negativ påverkan av kroppen – och i vissa fall rör det sig om väldigt stora skador, potentiellt.

(Det sägs t ex ibland att vattenlösliga vitaminer är ofarligt, man kissar ut överskottet. Men även med sådana går det att få överskott och skador (looking at you, B6), t ex vid högre intag än kroppen kan göra sig av med, intag med för lite vatten eller vid uttorkning (looking at you, B6), eller under oklara omständigheter (B6!).)

**Jag gör inga anspråk på att ha identifierat alla möjliga risker** med kosttillskotten som nämns. Ta alltså inte utebliven information som någon slags garanti om säkerhet.

Jag är **lekman** (dvs ej medicinskt skolad amatör – redan genom att diskutera detta låter det som att jag får någon slags implicit auktoritet – "jag är amatör (underförstått: men ändå vet jag såhär mycket!)" – detta är ej syftet), och skriver (till att börja med) mycket ur minnet, så ta inget jag säger som ett intyg om något, utan läs på och sätt dig in via andra källor om du vill prova något eller ha säkrare information. Detta är ingen medicinsk rådgivning, och har du medicinska problem, vänd dig till sjukvården.

* **Teanin**

* **Inositol**

* **Kelp** innehåller jod, som vi visserligen behöver, men som kan ge sjukdomar och skador i sköldkörteln vid för högt intag.

* **Selen** ska tas i försiktiga doser, överintag är farligt. En paranöt innehåller runt 50 ENHET? vilket är omkring RDA. Påstås kunna minska ångest. Jag upplevde första dagen med paranöt en lätt depressiv effekt, kan ha varit från B1, ganska neutral andra. Personer med alzheimers har lägre nivåer av selen (SH, ur minnet).

* **Biotin, B7** hjälper myelineringen av nervcellerna (här har jag faktiskt en gnutta utbildning, från biologidelen i psykologi A) – myelin är ett fettlager som ska täcka nervtrådarna och som gör att den elektriska signalen går snabbare/lättare – ökar ledningsförmågan, helt enkelt, kanske, i nervtråden – men ska låta det vara osagt om det gäller vanliga nervtrådar, eller en mer central del av nervcellen. Bra för nervsystemet. Överviktiga och personer med IBS har låga nivåer (SH, ur minnet).

## 11 september 2023
* Minskat på fluortandkräm. När slutade helt, verkade få ökad ångest efter 1-2v – första tandkrämen gav också antiångesteffekt först pga ngn ingrediens, evt. limonene (kan vara helt fel!), men sedan rebound/"backlash". Siktar nu på fluor en gång om dagen (dvs minsking – ett tag sköljde jag ej ur tandkräm efter tips på TV, billigt alternativ till fluorsköljning, men kanske ej att rekommendera, kanske inte något av dem). Effekt minskning/avbrott:

  * mindre oljig hy (som jag annars har problem med). i början och sporadiskt kombinerat med att lägga mig en kortis på marken (mot min acne, för hyns bakterieflora), som också kan ha effekt – då, och under helt uppehåll från fluor på någon vecka, kändes ryggen/huden på ryggen _torr_ när jag ~~skulle duscha~~ duschade. alltså, som att huden var torr i sig. har jag aldrig varit med om.

  * minskad ångest och förbättrat psykiskt välmående? kanske, det har funnits positiva tendenser på sistone, men också många faktorer (och ffa, se ovan). har ingen direkt slutsats här (men se det om att sluta helt, ovan).

  (Jag trodde att fluoret liksom hjälpte att göra tänderna _renare_ innan jag provade det här. När jag 'läst runt' lite så har jag funnit att så verkar inte vara fallet, fluoret bidrar däremot till att laga små uppkomna hål i emaljen – vilket säkert är fördelaktigt för tandhälsan, kan tänka mig drastiskt minskad gynnsamhet för tillväxt av dåliga bakterier.)

* Sporadiskt provat att lägga mig på gräsmattan med bar rygg efter nylig dusch, för att bättra på bakteriefloran. Detta samtidigt som experiment med uppehåll/minskad mängd fluorintag. Tycker att det verkar ha motverkat/verkat i en minskande riktning/tendens gällande min acne.

* Provat intag av selen via 1 paranöt per dag (SH anger att en sån ligger omkring 60 (mg eller mcg?), vilket är dagsbehovet ojmr, och ganska långt ifrån farligt intag (400 <enhet> eller liknande?) – förstås kan man redan få i sig mycket via annan kost med, och jod via kelp. Paranöten tyckte jag har en negativ effekt på mitt mående/humör, lite "nedslående", och jag har upplevt liknande när jag på sistone tagit zink, paradoxalt nog (ska vara bra mot ångest och depression), men med tiamin upplever jag att jag har en lätt negativ effekt den dag jag tar den, och mår sedan lite bättre, så kanske 1) har det med zinken att göra (mkt zink i paranötter, skulle jag nämna) 2) är den långsiktiga effekten positiv. (Har läst om "dåliga buffrare", vilket jag kanske verkar vara – förstår det som att vid intag av ett visst ämne så startar kroppen att tillverka så mycket av hormon x som behöver sagda ämne som den bara kan, typ, istället för att tillverka lagom. Grovt förenklat och kanske fel – kan också vara en myt. Men skulle kunna förklara mina symptom med negativa reaktioner på många ämnen som anses positiva.) Selenet skulle evt minska ångest (kort sikt: inte märkt ngn klar effekt, lång sikt: kanske), och kunna hjälpa rätta till/återställa skev dygnsrytm. Jag har kommit på att det funkar bra att ta min Mirtazapin på kvällen uppdelad på två doser (för hög dos = ej sömngivande/sämre dylik effekt), och så är det kelp/jod:en nedan, och försöker komma i säng snäppet tidigare och lyckas ibland, men jag somnar snabbare och verkar sova bättre, kanske har selenet hjälpt till med det.

  I alla fall, provade kelp för att det evt. innehöll taurin (som jag får ångest av i ren form, provat 500 mg eller mindre), och visade sig att det innehöll substantiella mängder jod, redovisat på förpackningen, 150 % av dagsbehovet, tänkte att jod är väl inte så illa. Detta 1) verkade ha effekter 2) var alldeles för kraftigt egentligen (men å andra sidan, vore det inte det så kanske jag inte hade märkt att det hade en bra effekt – fast då hade jag ju å andra sidan provat det ett tag först).

  Fick mer energi på relativt lungt sätt, kunde ändå somna, lättare än ofta annars, vaknade till lättare första natten och vaknade upp flera gånger, därmed också tidigare än vanligt. Lättare hantera ångesten, men sedan också intensivare ångest – båda eller kanske iaf det första pga bättre energi. Huden mer fet och oljig initialt, djupare röst (tog dag 1 och dag 4), sedan lite retad och kände mig svullen i halsen efter dos 2 åtm (sköldkörteln kan ha blivit irriterad, alt-healthare säger just av en lustig slump att man ska ta bl a selen vid jodintag, för minska risk för sköldkörtelinflammation – potenta grejer det här, ska understrykas, så man vill inte felhantera – körteln såg bra ut enligt läkare, lite rosa el. dyl. som vid mkt harklande). Alltså, fet hy, bättre energi, positiva och negativa effekter gällande ångest.

  Och fet hy är intressant, se ovan under fluoret. Läst att sköldkörteln lär ska ta upp vissa typer av ämnen (halogener?), där bl a fluor och brom ingår (bromerade flamskyddsmedel? PFAS som jag har haft mkt i dricksvatten ingår kanske inte där), som konkurrerar med jodet, ojmr. Tänker att kanske frisätts fluor från sköldkörteln när jag tar lite jod, därav den oljiga huden (när fluoret stör någon annan process, i sin tur, i kroppen). Vet ej. Men intressant om det hör ihop.

  Får kanske inte i mig så mkt jod med kosten. Ska finnas bl a i skaldjur (och fisk?), och det äter jag sällan (fisk lite oftare).

* Mamma högt blodtryck, för något år sedan eller så tagit inositol en period, därefter blodtryck bättre, och sedan yrsel när tog blodtrycksmedicin och minskade på egen hand dosen (doktorn ville avvakta). I våras-somras (3-7 månader sedan) börjat prova taurin mot ADD/ADHD-liknande problem (fokus, energi (humör?)). Övre bl. tr. nu 12x, 145, 14x och liknande, tagit inositol i 4-5 dagar, yrsel igår, skippat inositol sedan dess, blodtryck nere i 109/70, 5-10 min senare 113/73 (puls 68, 73).

### Faktaletande
https://old.reddit.com/r/Supplements/comments/10aer79/feel_amazing_with_10mg_zinc_feel_depressed_horny – choklad för kopparintag samtidigt. kelerad zink rek. mår bra av 10 mg, dåligt av 20.

https://old.reddit.com/r/depression/comments/byzsz0/zinc

https://old.reddit.com/r/Nootropics/comments/11ggf4s/zinc_causing_depression
> Zinc has action on acetylcholine (increasing), although probably to a minor degree
Not saying that's it, but some folks get the symptoms you're describing from increases in acetylcholine

> If you're worried about copper eat some sesame seeds at least 2 hrs apart from the zinc. They're high in copper.

annan tråd:
> zinc upregulates nmda receptors. And this leads to excitotoxicity, decrease in neurogenesis and depression.

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9280433
