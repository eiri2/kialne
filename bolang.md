## Bolak

I've read a bit about Bolak or _Langue bleu_ etc, and I can say that it is not as bad as I remembered it. For the /tS/, I would substitute the cyrillic letter with say <c> I think. At least when not being able to write cyrillic, for ease of writing.

The worst part so far is that it has a lot of systematical small words, with fine-grained differences, with not so much 'individual' difference between them in some cases.

A fun thing (I don't find it now) is that Bollack claims that o is pronounced as English o (which is usually a diphtong, right?), but I suppose it's pronounced in a way that English o is often not (more like Esperanto o). Maybe it's an oversight of the author or translator, maybe it's a change in English translation, or ideas about pronounciation, since then?

The vowel u fulfills grammatical/systematical roles in [blue], and.. actually, one could get the impression that the translator is not always understanding what they are writing about. Or it's dated terms (_feminine name, plural name, reunion_ – meaning feminine noun, plural noun, merge, and _perfection_ seems to mean perfect tense). How to try to, or.. I'd like to try to summarise the usages simply. cvc words are always nouns, and I use c here instead of the cyrillic letter ~~(obviously it's an abbreviation of "cyrillic letter")~~. So, u is used as a marker for these things, in the following specific positions:

* uCVC(u) (except uCVd and uCVc) – feminine noun
* (u)CVCu – plural [noun] ~~marker~~

* CVCuV – passive verb ~~marker (verb in passive voice)~~
* uCVC – past tense verb ~~marker (in active voice)~~

* uCVCVd – attributive with idea of parity, comparative of parity (what?) in qualifying words (this might mean adjectives)

* uC(C)V(C)(V)c (or uXc) – comparative of parity in adverbs
* CVCuCVC – compound word, "oral dash"

Bolak and Bollack then boldly states that it is _impossible_ to mistake, actually the phrasing is weird there, but to mix up these different usages.

My comment on the parity, like.. that and other things that I don't understand isn't neccessarily on the book, it might be me lacking knowledge of certain terms in English.

Bolak literally has it's own grammar, with 8 parts of speech divided in 2 classes, Shortwords and Longwords. The Shortwords are, afaik, short, but also do not give "any precise notion" (of intended meaning), such as English "of" and "and" (vs. lion, troll, mountaingoat).

Longwords in other words includes at least nouns, adjectives and I guess adverbs too. Shortwords could be called "function words (and interjections)" alternatively, I'm guessing.

The prefixes _fku_ (anti-) and _sku_ (likeness to, -ish) are a bit similar, risking getting mixed up. The future and up-and-coming Bolak revival, or just vival, community should maybe choose another root for the first one, because maybe it's less productive (unless it's close to Esperanto _mal-,_ which was my first intuitive understanding).

There are four markers for non-bolak-y words, going before the words they mark:

* **au** – a proper name
* **eu** – from non-native language for speaker _and_ interlocutor
* **iu** – from native language for interlocutor etc
* **ou** – from native language of speaker

* **io** – subordination, "whatever preposition"?
* **oi** – general conjunction
* **ea** – singular marker?
* **ae** – plural marker?

Not sure what they all really mean yet, but I like the idea of general-type words, if it is as I think it is. Is ea and ae too similar? I think maybe not, and often the information carried by them is not so crucial.

Bolak has many good ideas, but possibly it's not so very stringent. Not sure yet, though.

* **it, bo, or** – and, but (?), or
* **fo, go** – when, then
* **ik, bi, os** – before, during, after
* **in, ib, ot** – in, on, out
* **id, to, tso** – to (direction), to/towards, from

The difference between id and to is not clear for me. Is it possible that some of these short words (there's more in the list in the book) are too similar? Yes, maybe, it's hard to say so far (maybe in general without using it).

## Day 2 (or something like that)

Surely, I think that it's probably so that if I've known enough French to read the original book, some quirks in the English edition would be straightened out (or rather, non-present).

Hmm, apparently the last part of the scan on archive.org are pages in French – the same content as the start of the book in English, I think? I stumbled upon this now.

Maybe one could say that Bollack enthusiastically talks about the systematical parts of the language so much or in such detail, that it's a bit hard to understand. And there seems to be many of them. It's a bit fun when one feels that one can start to make a bit sense of those things. This feels far away, so far, though.

This feels more complicated than Volapük! (And, well, read that litteraly – Volapük is not extremely complicated, but a bit.)

I sort of hope to get the questionmarks straightened out by reading more, but instead I get new information and more questionmarks (and that thing is a Swedish idiom in case it is not used in English, it's not that I'm a poet or something like that).

Finally, something concrete:

* **-i** – ~~infinitive~~ eternal tense
* **-o** – present tense
* **-e** – the past
* **-a** – the future

As I understand it. It's hard to see the difference between e and o in the scan, so I thought it was the other way around first. But I think this is the right one, um, interpretation.

I thought eternal tense was a clever way of saying infinitive, but apparently it's a new whatever-tense tense form.

**-am** is a vague collective marker (my paraphrasing of the text) – lokam, "about eight". How lovely! Also, multiplying collective marker -olt, especially in _nifolt,_ sounds suspiciously similar to/reminds of Swedish _-falt_ (as in _niofalt_), and of course English -fold in _ninefold._

There seems to be no actual vocabulary/dictionary availible on the internet?
