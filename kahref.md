## Kah reference

* **SVO** ~~'svord order'~~, **noun** then **modifier**, topic goes first (ie. emphazised things?), subject pronouns often dropped/skipped, subjects can have several verbs with different objects, **[possession] na [possessor].**

* no inflection, oligosynthetic (and compounds seeem to be able to be lexicalized).

* alphabet: **a b c d e f g h i j k l m n o p r s t u v w y z,** digraphs: **ch ny ng sh** – /tʃ ɲ ŋ ʃ/<!-- (allowed glides: **py kw mw sw** – /p^y k^w m^w s^w/)-->. _c_ is only used in _ch,_ _j y_ is /dʒ j/, respectively.

* stress on penultimate syllable, _unless_ the last is filled by a vowel sequence or ends with consonant – then stress on that syllable. (did I intepret this right?)

----
* four parts of speech: **noun, verb, conjunctions, interjections.**
  * verbs are **stative** or **active,** and both of these can be either intransitive or transitive. a stative verb after a noun functions as an adjective: **nia yam** – car [be red], 'red car', while putting it before makes it more insisting: *yam nia** – [be red] car, 'the car is red'. <!--I think this has to do with the topic thing – and -->It's a bit like the noun is a modifier to the static verb, in '[static verb] [noun]' (_be red car_). <!--osäker på vad jag tänkt fortsätta med här-->also, stative verbs can express things such as [placeholder] and _tizia wezai_ – raise loving – 'raise lovingly'(resultative and adverbial)
  
  * interjections include **eo** – 'yes', **wu** – 'how very', and **ha** – question particle put in the end of yes/no-phrases, etc, and can be inserted anywhere to express emotions.

#### Prefixes
* Verb prefixes (also see Combining words below) – the forms of _jam, mun_ and _tom_ depends on the following consonant:
  * **jam, jan [right?], ja** – to do – transitive verb from a stative verb
  * **mun, mu** – to cause – transitive verb from an active verb
  * **we** – to be stative, in a state – transitive stative verb
  * **o** – [to result as?] – stative verb, usually involving a resultative aspect
  * **tom, ton [right?], to** – to become – intransitive active verb [how can become be an _active_ verb? different meaning of active?]

* Noun prefixes:
  * **u** – animate object
  * **a** – inanimate object
  * **i** – a state (not geographical)

* Some conjunctions are formed from other words using the prefix **e.**

----
* stringing verbs is a core feature of Kah. 'subj verb verb verb (object)' can mean?/always means? 'subj verb in order to verb in order to verb object'. **wa nunai de ka li** – I travel come talk to you – _I traveled to come to talk to you._

* emphazis can be done using two different markers. one way is to put the emphasized word or phrase to the beginning of the sentence followed by the marker **be** that then also tells us word order is different. the other way is to just put the marker **lo** after the emphasized word or phrase. as far as I understand, that is all that differs between them.
  * **be** shows that what comes before is emphasized content, not in it's standard position.
  * **lo** tells us that what preceeds that word is emphasized, in regular word order.

## Combining words
To fit with the phonotactial rules of Kah, phonemes sometimes change when they are put together in a compound. In the following, **C** is the relevant consonant, **V** is a reduplication of the last preceeding vowel, while **W** is a reduplication of the first following vowel.

* **m, n, ng** before
  * **f, v, h, l, r → CV** (lam + vun  = lamavun)
    * **unless** in the verb prefixes **jam, mun** and **tom** → **ja, mu, to** (VC → V)
  * otherwise 'made homorganic' with the following consonant
    * **p → m** (ng included?)
    * **k, s → n** (ditto)
    * [b c d g j m n t w y z?]

* **s** before
  * anything **except p t k w y** → **sV**
  * **y** → **sh**
  * [ptk unchanged, but what with w?]

* **l** before anything **except p t k** → **lV**

* **i** followed by
  * **y** or **i** → just a **single y** (i+y = y, i+i = y)
    * **except** if followed by **ya, yo, ia** and **io** [not sure the latter two exist] → just a **single i** (i+ya = io, i+yo = io) (did I interpret this right?)
  * **V** → **y**

* **u** followed by **w** → just **one/a single w**

Last-vowel duplication thus occurs after **m, n, ng** before f, v, h, l, r – **s** before anything except p, t, k, w, y – **l** before anything except p, t, k.

* opposites are formed with **-ng** if the word ends in vowel, **-Vng** if it ends in a consonant
 * one syllable roots are **reduplicated** before adding ng – je → jejeng [there's no one syllable roots ending in C, I presume?]

~~Prefixes used to form verbs change in slightly different ways. (See above.)~~

### Shorter description
One could say that the basic general rule/patterns are that

* **m, n, ng, s, l** before many consonants duplicates the **preceeding vowel** (V¹C turns into V¹CV¹)
* **i** before most vowels turns into **y**
* **u** before **w** gets **removed** (turns into just a single _w_ – u+w → w)
* all of the above **excepts** the following
    * _jam, mun_ and _tom,_ that turns into _ja, mu_ and _to_
    * _m, n, ng_ before any of [b c d g j k m n p s t w y z] → made homorganic with that consonant
    * _s_ before
      * _p, t, k_ → s (unchanged)
      * _y_ → _sh_
      * _w_ → [??]
    * _i_ before _i_ or _y,_ which turns into just a single _y_ (i+i and i+y → y), except
      * when followed by _ya, yo, ia, io_ [do these latter two exist?], then the _ii_ and _iy_ turns into just a single _i_ (i+ya and i+ia → ia, i+yo and i+io → io) [right?]

And that should be all the rules for phonological changes.
