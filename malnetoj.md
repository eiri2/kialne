## Some esperantido
I had the idea of joking about an esperantido with more international roots (i.e. with more English roots) the other day, like:

mi sias la hawso, etc

in my head this morning this spiraled into something different and more elaborate:

ek sie de haws

ek yu hi, xi, it, dej?
wi yu?/ir dej

'ek' is too cool to not include.

I figured maybe verbs more often end in a vowel, so they end in -e, also from da no de (is, fa?), and then nouns end in any consonant. One could maybe argue for the other way around too.

'yu' has the benefit of not having a natural separate object form. while ek is Nordic, 'ir' is as a compensation/balancing closer to german (but also old nordic 'I' and object form 'er').

though, I know Afrikaans and Low Saxon has very good pronouns too.

## Introverteco

La difino, pmp, de introverteco kiu mi lernis estas tiu trajto de homoj kiu koncistas de plej facile ol malintrovertuloj stimiliĝas. Do, ili preferas aktivitoj kiu ofte ne sufiĉe stimulas malintrovertajn homojn, kaj trovas aktivoj kiu sufiĉe stimulas extrovertuloj trostimula/superstimula (kiu ofte estas taksa afero). Simple fakte temas, laŭ kio mi lernis, pri kiom facile oniaj neŭronoj stimuligas. Se ili malfacile stimuligas, vi estas ekstrovertulo, se ili facile stimuligas, vi estas introvertulo. (Kaj la kutima difino laŭmemore kredeble pli temas pri kiaj okupoj vi ŝatas – introvertuloj ŝatas aferoj kiel sole aŭ duope trankvili studi aŭ analizi aferojn, eble diskuti, legi, havi mem-tempon, ktp.)

povas esti malpli facile renkontiĝi amikojn kiel introvertulo, ĉar 1) la  plejmulto de homoj havas ĉefa ekstroverta trajto 2) (la societo estas adaptigita pli al ekstrovertuloj, kaj tiu trajto estas iom pli valorata kaj) ne ekzistas tiom multe de aktivitetoj por introvertuloj por renkontiĝi, eble? libroronroj ne havas la saman ŝtatuson kiel iri al festivaloj aŭ sociumi en festo.

mi cetere ne multe legas librojn, mi estas iom tro ĥaosa por fari tion ofte, ekde longe, minimume (kaj eble mia medio estas iom tro ĥaosa ankaŭ).

nu, mi ekpensis alian (ne konfliktan) eblan difinon: trajto de ĝui pensado aŭ pensi pri aferojn (aŭ analisi). (Kiu oni ankaŭ ne trofaru se temas pri zorgj aŭ ankcioj, ĉar tio laŭ ~ACT~ MCT estas *la* kozo de plurmultaj mensaj malsanojn – la plejmulto de ili, eĉ).

Kaj mi ankaŭ ekpensis simbolo (UTF-8-kompatibla/emotikona) por introverteco: taso de teo. Sentas kiel la ideo de ~taso~teo-trinkado bone simbolas introvertajn trajtojn (komparu al la orda, trankvila kaj malrapida japana teo-ceremonio, ktp), kaj trankvilon.

Mi tute ne trinkas multe da teo, fakte mi evitas vera teo ĉar la kofeino trostimulas (!) min, kaj mi ne ŝatas tro varmegaj trinkaĵoj kaj la atendo por ili malvarmigi ĝis trinkeblaj temperaturoj, sed mi iom ŝatas la ideo de teo, kaj mi ŝatas variajn herbo-teojn, eĉ se mi ne multe ofte trinkas ilin. Sed la simbolo tamen nur sentas taŭga. ☕️

kon-, var det väl, (verko), revo

lapennas misslyckade rapport, iel malsukcesa raporto?, eble formiĝas/markigas pinto la esperanta movado, en la historio de esperanto, poste kio oni ne havis klarajn celojn plu.

I think I've found a pinnacle in the history of Esperanto. When you read stuff about Esperanto, at least in my case, you find a lot about the early years and/or the golden age/some sort of a golden age (and those periods start at the earliest in the late 1800s, up until the 1940s to maybe 1960s/1970s, something like that – it's vague, and the latter date about the end of the information.. actually the info I find generally goes to the 30s/40s, and the rest is extrapolating, influenced by the find), and during that time there was seemingly rather much activity (at least in Sweden and some other places), and later the interest and activity seems to have vaned, so that Esperanto was rather forgotten and unmodern in the 1980s–2000s, at the very least (2010s, one can certainly say too, and I guess noones said that that has actually stopped ~just~ yet).

In the 1960s, UEA president Lapenna presented a proposal for the UN regarding use of Esperanto, which was categorically denied because organisations couldn't do that type of proposals, which UEA and Lapenna was seemingly unaware of (all according to Wikipedia). After that, UEA was according to Libera Folio discouraged and pleaded not to continue that project by a Swedish esperantist and politician<!--IB-->. Why? I don't know really.

But you do get sort of an impression that there ends a sort of a forward-striwing in the Esperanto movement, and then follows possibly a goal-lessness and being (a bit) lost (and so on). Raŭmismo also comes after that, which seem logical. It all seems a bit modernism turned into a target-less and confused postmodernism instead (not trying to super-critizise anything, just my impression), as in other parts of society/societies, movements and the world, at different rates and in different times.

(And what to do now? Goals are probably good, generally. What goal(s) should Esperanto have, if it can?)

And I think one should market Esperanto more as a fun hobby, a fun language to learn (which increases general language ability – I suspect languages generally do that, maybe very regular ones do it more, too), a symbolic act or a symbolic value, (and) parttaking in a (egalitarian, at least in a sense) small/minor global community.

(If one now should do 'marketing' in any shape or form. Maybe even other auxlangs should preferrably be marketed instead?)

## Ĉina reformulo mortis

Ĉina reforma aŭ progres(em)a komunistpartiana eks-politikisto mortis en la aĝo de 90 jaroj. Mi akiris impreson ke li estis sufiĉe bona kaj bonintenca, kaj iel kontraŭis la masakro je la <torg> de la Ĉiela paco. Poste tio, li estis arestita kaj imprisonita.

Bou aŭ simila. citaĵo.

## Situacio en Myanmaro malboniĝas
La armeo de Mjanmaro/Birmo, kiu antaŭe dum longan periodon sole kaj diktatore estris la landon, ne akceptis la lastaj elektoresulto, en kiu la armea partio malgajnis multe da influo, kaj arestis amasoj de aliaj elektituloj. Ekde tiam estas altigita opreso en la lando, kaj bataloj inter la armeo kaj la kontraŭ-armeaj armeetoj aŭ geriloj kiu homoj nun aligas. Inter tiuj estas armea grupo de la ”ombra registaro” de Mjanmar.

* Sveriges radio, Nyheter i korthet

## Malpli da fumo en mondaj kuirejoj

## Rinocerkornoj kaj svedaj alkoj etiĝis
Radio de Svedio raportas ke la kornoj de rinoceroj iĝis malpli granda, kaŭze de evulucia premo kaŭzita de ĉasado dum la 20-a jarcento.

Kaj oni ankaŭ raportas ke svedaj alkoj malgrandiĝis dum la lastaj jarcentoj. La arbor-enterprenoj kaj arbor-propruloj iĝis pli influa en la decidoj de ĉas-rajtoj. La alkoj manĝas de junajn arbojn, kaj do tiuj agistoj influis la decidojn al pli da ĉaso. Tamen, tio ne estas la plena/nura kaŭzo.

Ankaŭ pli ~varjmaj,~ sekaj someroj kaj pli fruaj printempoj influas la alkojn. La sekajn somerojn malgrandigas la disponeblo de manĝaĵoj por la alkoj (plantojn kaj foliojn kaj branĉetojn de arbojn kaj arbetoj/arbedoj). Ankaŭ la alkopatrinoj naskas iliajn idojn kiam la plejmulto de fruaj printempaj foliaĵoj en la printempo, tamen tiu periodo fruigis kaj la naskoj ne ĝenerale, do ankaŭ la alkopatrinoj havas malpli ~da~de nutro por la idoj.

Ĉirkaŭ 20 jaroj anktaŭe, ~mezumgranda~ alkido aŭ alkiĉido (mi ne certas) havis la mezuman pezon de 70 kg, kaj nun ĝi pezas 60 kg.

* https://sverigesradio.se/artikel/farre-och-mindre-algar-i-arets-jakt-klimatforandringar-en-forklaring

## La sveda kompreno de la svedia grandeco

## Bojkotu Ĉinio
Ĉinio estas diktaturo kiu malrespektas homojn kaj homaj rajtoj (ekzemploj inkludas la tibetanoj kaj ties kulturo, la uiguroj kaj ties kulturo, multaj aliaj individuoj, parolmallibereco, cerbostopado, amasa gvatado, mortpunoj, ktp). Samtempe en la okcidento aŭ la resto de la mundo, ni aĉetas aferojn produktita en Ĉinio, riĉigantan kaj helpantan tiun staton. Mi pensas ke ni devas eviti tiun.
